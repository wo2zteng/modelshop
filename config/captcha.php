<?php

return [
    'client_url' => '//captcha.luosimao.com/static/js/api.js',
    'client_site_key' => '9cc5ce64ba7cd463860273604e6f0f68',

    'verify_url' => 'https://captcha.luosimao.com/api/site_verify',
    'verify_api_key' => '1a48ca5364bb1b143f68d451d077963a',

    'enable' => env('ENABLE_CAPTCHA', false),
];
