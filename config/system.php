<?php

return [

    'init_user_name' => env('INIT_USER_NAME', 'admin'),

    'init_user_nick_name' => env('INIT_USER_NICK_NAME', 'admin'),

    'init_user_mobile' => env('INIT_USER_MOBILE', '00000000000'),

    'init_user_password' => env('INIT_USER_PASSWORD', 'ADM_123'),

    'logo_image' => env('LOGO_IMAGE', '/images/logo.png'),

    'default_platform_org' => '格林兄弟',

    'default_admin_role_name' => '超级管理员'
];
