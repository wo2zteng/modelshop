<?php

return [
    'current' => [
        'enable' => false,
        'header_color' => '#dd7722',
        'sidebar_background_color' => '#4477bb',
        'sidebar_sub_menu_color' => '#dd6622',
        'sidebar_menu_item_color' => '#ff7733',
        'sidebar_menu_item_fore_color' => '#f2f2f2',
        'sidebar_menu_item_selected_color' => '#883399',
    ]
];
