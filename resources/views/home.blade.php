@extends('layouts.app')

@inject('profileHelper', 'App\Lib\Auth\UserProfileHelper')

@section('content')
@php
    $userGroups = $profileHelper->userPermissions();

    $moduleHelper = new \App\Lib\Core\ModuleHelper($profileHelper);

    $httpMethod = \Route::getCurrentRequest()->getMethod();
    $routeUri = '/'.\Route::current()->uri();
    $menuAndModuleResults = $moduleHelper->setActiveModuleFlag($httpMethod, $routeUri);

    $menuAndModules = $menuAndModuleResults['menus'];
    $defaultMenu = $menuAndModuleResults['defaultMenu'];
@endphp
<div class="layui-layout layui-layout-admin">
<div class="layui-header header header-demo" {!! config('theme.current')['enable']?('style="background-color:'.config('theme.current')['header_color'].'"'):'' !!}>
    @include('layouts.partial._header', ['isSuperUser'=>!$profileHelper->user()->isSuper(), 'userGroups'=>$userGroups, 'modules'=>$menuAndModules])
</div>
<div class="layui-side layui-bg-black" {!! config('theme.current')['enable']?('style="background-color:'.config('theme.current')['sidebar_background_color'].'"'):'' !!}>
    @include('layouts.partial._siderbar_menu', ['menuAndModules'=>$menuAndModules])
</div>
<div class="layui-body">
    <div style="margin:0;" class="layui-tab layui-tab-card" lay-filter="navitab" lay-allowclose="true">
        <ul class="layui-tab-title">

        </ul>
        <div class="navi-tab-content layui-tab-content">
        </div>
    </div>
</div>
<div class="layui-footer">
<!-- 底部固定区域 -->
</div>

</div>
<script src="/vendor/layui/layui.js" charset="utf-8"></script>
<script src="/vendor/webuploader/require.js"></script>
<!--<script type="text/javascript" src="/vendor/webuploader/upload.js"></script>-->
<script>
layui.config({
    base: '/js/layui/'
}).extend({
    jfTable: 'jfTable',
    naviTabUtil: 'naviTabUtil',
    validator: 'validator',
    popLayerUtil: 'popLayerUtil',
    addressUtil: 'addressUtil',
    uploadUtil: 'uploadUtil',
    dateRangeUtil: 'dateRangeUtil',
    selectBox: 'selectBox',
    reditorUtil: 'reditorUtil',
    multifileuploader: 'multifileuploader',
    largefileuploader: 'largefileuploader'
});

layui.use(['form', 'element', 'layer', 'util', 'jquery', 'naviTabUtil'], function(){
    var form = layui.form();
    var util = layui.util;
    var layer = layui.layer;
    var element = layui.element();
    var $ = layui.jquery;
    var naviTabUtil = layui.naviTabUtil;

    layui.define(['form', 'element', 'layer', 'util', 'jquery'], function(exports){
        var $ = layui.jquery;
        var naviTabUtil = layui.naviTabUtil;

        var obj = {
            changeLeftNaviByModule: function (moduleId) {
                $.get('/changeNaviModule/'+ moduleId, {}, function(str){
                    $(str).appendTo($('.layui-side').html(""));
                    var element = layui.element();
                    element.init();
                    element.on('nav(leftnav)', function (elem) {
                        var url = $(elem).children('a').attr('data-url');
                        var id = $(elem).children('a').attr('data-id');
                        var moduleId = $(elem).children('a').attr('data-module-id');
                        var title = $(elem).children('a').children('cite').text();
                        naviTabUtil.activateNaviTab(url, id, title, moduleId);
                    });
                });
            }
        };
        exports('topNaviUtils', obj);
    });

    form.render();

    form.on('select(user-groups)', function(data){
        console.log(data.elem);
        console.log(data.value);
        console.log(data.othis);
    });

    naviTabUtil.disableFirstTabClose();

    @if (isset($defaultMenu))
    var url = '{{$defaultMenu['route']}}';
    var id = '{{$defaultMenu['id']}}';
    var title = '{{$defaultMenu['name']}}';
    var moduleId = '{{$defaultMenu['module_id']}}';
    naviTabUtil.activateNaviTab(url, id, title, moduleId);
    @endif

    //监听左侧导航点击
    element.on('nav(leftnav)', function (elem) {
        var url = $(elem).children('a').attr('data-url');
        var id = $(elem).children('a').attr('data-id');
        var moduleId = $(elem).children('a').attr('data-module-id');
        var title = $(elem).children('a').children('cite').text();

        naviTabUtil.activateNaviTab(url, id, title, moduleId);
    });

});

function logout(){
    $ = layui.jquery;
    $('.logout-form').submit();
}

</script>
@endsection
