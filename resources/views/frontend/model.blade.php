<!DOCTYPE html>
<html>
    @include('frontend.part.head', ['title' => '享服-服饰3D模型，应有尽有', 
        'descriptions' => '享服是最全球最大的3D服装模型共享平台，平台包含了数以万计的建模师作品，可以为您快速搭建成衣模型，3D效果图', 
        'keywords' => '享服,3D模型,服装,衣服,建模,素材,3dMax'])
<body>

    @include('frontend.part.header', ['topnavis' => [
        ['anchor' => '/', 'section' => '首页'],
        ['anchor' => '/', 'section' => '模型详情'],
        ['anchor' => '/', 'section' => '更多模型']
    ]])

    <div class="p-content">
        <div class="breadcrumb">
            <ol class="am-breadcrumb">
                <li><a href="/">首页</a></li>
                @foreach ($model_cates as $cate)
                <li><a href="#">{{$cate->name}}</a></li>
                @endforeach
                <li><a href="#">{{$model_class->name}}</a></li>
                <li class="am-active">{{$model->name}}</li>
            </ol>
        </div>
        <div class="info-content">
            <div class="model-content">
                <div class="model-top">
                    <div class="title">{{$model->name}}</div>
                    <div class="feedback">
                        <div class="visit">{{ empty($model->visit_count) ? 0:$model->visit_count }}</div>
                        <div class="download">{{ empty($model->download_count)?0:$model->download_count }}</div>
                    </div>

                    <div class="publish-date">
                        发布日期：{{date('Y-m-d', strtotime($model->upload_date))}}
                    </div>
                </div>
                <div class="thumb-c">
                    <img src="{{$thumb}}">
                </div>

                <div class="detail-info">
                <div style="
    margin-left: 10px;
    width: 740px;
    height: 120px;">
                    <div class="am-slider am-slider-default am-slider-carousel" data-am-flexslider="{itemWidth: 150, itemMargin: 5, slideshow: false}">
                        <ul class="am-slides" id="imageboard">
                            <li><img src="{{$thumb}}" /></li>
                            @foreach ($broadCastImages as $image)
                            <li><img src="{{$image}}" /></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                    <ul class="dis-main">
                        <li>
                            <div class="item-title">标题：</div>
                            <div class="item-content">{{$model->name}}</div>
                        </li>
                        <li>
                            <div class="item-title">分组：</div>
                            <div class="item-content">
                            @foreach ($model_cates as $cate)
                            {{$cate->name}} >
                            @endforeach
                            {{$model_class->name}}
                            </div>
                        </li>
                        @foreach ($props as $prop)
                        <li>
                            <div class="item-title">{{$prop['key']}}：</div>
                            <div class="item-content">{{$prop['value']}}</div>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>

    @include('frontend.part.footer')
</body>
@include('frontend.part.scripts')

<script>
$('#imageboard').find('li').on('click', function(){
    var image = $(this).find('img:eq(0)');

    $('.thumb-c').find('img:eq(0)').attr('src', image.attr('src'));
});
</script>
</html>