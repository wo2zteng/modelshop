<!DOCTYPE html>
<html>
    @include('frontend.part.head', ['title' => '享服-服饰3D模型，应有尽有', 
        'descriptions' => '享服是最全球最大的3D服装模型共享平台，平台包含了数以万计的建模师作品，可以为您快速搭建成衣模型，3D效果图', 
        'keywords' => '享服,3D模型,服装,衣服,建模,素材,3dMax'])
<body>

    @include('frontend.part.header', ['topnavis' => [
        ['anchor' => '/', 'section' => '首页'],
        ['anchor' => '/', 'section' => '模型详情'],
        ['anchor' => '/', 'section' => '更多模型']
    ]])

    <div class="p-content">
        <div class="breadcrumb">
            <ol class="am-breadcrumb">
                <li><a href="/">首页</a></li>
                @foreach ($treeCates as $cate)
                <li><a href="#">{{$cate->name}}</a></li>
                @endforeach
            </ol>
        </div>
        <div class="info-content">
            <div class="query-box">
                <div class="query-info">
                    <ul class="query-item">
                        <li>
                            <div class="item-label">模型分类：</div>
                            <div class="item-content">
                                @if (count($modelClasses) > 0)
                                <ul class="query-field-container" model-container>
                                    @foreach ($modelClasses as $modelClass)
                                    <li class="field-item"><div class="box-item" model-item model-id="{{$modelClass->id}}">{{$modelClass->name}}</div></li>
                                    @endforeach
                                </ul>
                                @else
                                <ul class="query-field-container" cate-container index="0">
                                    @foreach ($categories as $cate)
                                    <li class="field-item"><div class="box-item" cate-item cate-id="{{$cate->id}}">{{$cate->name}}</div></li>
                                    @endforeach
                                </ul>
                                @endif
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="query-detail">
                <div class="detail-box">
                    <div class="caption">
                        所有模型
                    </div>
                    <div class="model-item">
                        <ul class="content-cont">
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    <div>

    @include('frontend.part.footer')
</body>
@include('frontend.part.scripts')

<script>
    $('.box-item[cate-item]').on('click', cateClick);

    function cateClick(){
        var self = this;
        $.get('/getcates/'+ $(self).attr('cate-id'), {}, function(content){
            $('.query-field-container[model-container]').each(function(){
                $(this).remove();
            });

            $('.query-item').children().each(function(i) {
                if (i > 0) {
                    $(this).remove();
                }
            });

            var cUl = $(self).parent().parent();

            cUl.find('div[cate-item]').each(function() {
                $(this).attr('class', 'box-item');
            });

            $(self).attr('class', 'sel-box-item');

            var index = parseInt(cUl.attr('index'));
            index++;
            var ulox = $('.query-field-container[index='+index +']');

            while (ulox != null && ulox != undefined && ulox.length > 0) {
                ulox.each(function() {
                    $(this).remove();
                });
                index++;
                ulox = $('.query-field-container[index='+index +']');
            }

            if (content.length > 0) {
                queryResult(0);
                var startIndex = parseInt(cUl.attr('index')) + 1;
                var ulhtml = '<ul class="query-field-container" cate-container index="' + startIndex + '"></ul>';
                var ul = $(ulhtml).insertAfter(cUl);
                for (var i=0; i<content.length; i++) {
                    ul.append('<li class="field-item"><div class="box-item" cate-item cate-id="' + content[i].id + '">' + content[i].name + '</div></li>'); 
                }

                ul.find('div[cate-item]').on('click', cateClick);
            } else {
                $.get('/getclasses/'+ $(self).attr('cate-id'), {}, function(content){
                    if (content.length > 0) {
                        var ulhtml = '<ul class="query-field-container" model-container></ul>';
                        var ul = $(ulhtml).insertAfter(cUl);
                        for (var i=0; i<content.length; i++) {
                            ul.append('<li class="field-item"><div class="box-item" model-item model-id="' + content[i].id + '">' + content[i].name + '</div></li>'); 
                        }

                        ul.find('div[model-item]').on('click', resetClassProps);

                        queryResult(0);
                    }
                });
            }

            
        });
    }

    function resetClassProps() {
        var self = this;
        $('.query-item').children().each(function(i) {
            if (i > 0) {
                $(this).remove();
            }
        });

        var cUl = $(self).parent().parent();
        cUl.find('div[model-item]').each(function() {
            $(this).attr('class', 'box-item');
        });

        $(self).attr('class', 'sel-box-item');

        $.get('/getclassprops/'+ $(self).attr('model-id'), {}, function(content){
            for (var i=0; i<content.length; i++) {
                var li = $('<li>');
                li.append('<div class="item-label">' + content[i].name + '：</div><div class="item-content"><ul class="query-field-container" props-container></ul></div>');

                var liul = li.find('ul[props-container]');
                var props = content[i].value.split(',');
                for (var j=0; j<props.length; j++) {
                    var fields = props[j].split(':');
                    liul.append('<li class="field-item"><div class="box-item" props-item props-id="' + fields[0] + '">' + fields[1] + '</div></li>'); 

                    liul.find('div[props-item]').each(function() {
                        $(this).on('click', function(){
                            liul.find('div[props-item]').each(function() {
                                $(this).attr('class', 'box-item');
                            });
                            $(this).attr('class', 'sel-box-item');

                            queryResult(0);
                        });
                    });
                }

                $('.query-item').append(li);

                queryResult(0);
            }
        });
    }

    function queryResult(append) {
        var modelCates = '';
        var postdata = {};
        var pIndex = 0;

        if (append == 0) {
            $('.content-cont').attr('pindex', 0);
        } else {
            if ($('.content-cont').attr('pindex') != undefined) {
                pIndex = parseInt($('.content-cont').attr('pindex'));
            } else {
                $('.content-cont').attr('pindex', 0);
            }
        }
        

        var l = $('.query-field-container[cate-container]').length;
        
        if (l > 0) {
            $('.query-field-container[cate-container]').each(function(i) {
                if (i == l - 1) {
                    var x = $(this).find('div[class=\'sel-box-item\']:eq(0)');

                    if (x != undefined && x != null) {
                        modelCates = 'categoryId=' + x.attr('cate-id');
                        postdata.categoryId = x.attr('cate-id');
                    }
                }
            });
        }

        if (modelCates == '') {
            postdata.categoryId = {{$cateId}};
        }

        var modelclass = '';
        
        var lc = $('.query-field-container[model-container]').length;
        if (lc > 0) {
            $('.query-field-container[model-container]').each(function(i) {
                if (i == lc - 1) {
                    var x = $(this).find('div[class=\'sel-box-item\']:eq(0)');

                    if (x != undefined && x != null) {
                        if (x.attr('model-id') != undefined) {
                            modelclass = 'classId=' + x.attr('model-id');
                            postdata.classId = x.attr('model-id');
                        }
                    }
                }
            });
        }

        var vals = '';

        $('.query-field-container[props-container]').each(function() {
            var x = $('.query-field-container[props-container]').find('div[class=\'sel-box-item\']:eq(0)');

            if (x != undefined && x != null) {
                if (x.attr('props-id') != undefined) {
                    vals += x.text() + ',';
                }
            }
        });

        if (vals.length != '') {
            postdata.props = vals.substring(0, vals.length - 1);
            vals = 'props=' + vals.substring(0, vals.length - 1);
        }

        var dataStr = 'pageNumber=' + pIndex + '&pageSize=8';
        if (modelCates != '') {
            dataStr += '&' + modelCates;
        }

        if (modelclass != '') {
            dataStr += '&' + modelclass;
        }

        if (vals != 'props=') {
            dataStr += '&' + vals;
        }

        postdata.pageNumber = 0;
        postdata.pageSize = 8;

        $.ajax({
            contentType: "application/json",
            type: 'post',
            url: '/getmodels/query',
            data: JSON.stringify(postdata),
            success: function (outResult) {
                if (append == 0) {
                    $('.content-cont').children().each(function() {
                        $(this).remove();
                    });
                }

                $('.hasmore').each(function() {
                    $(this).remove();
                });

                for (var i=0; i<outResult.list.length; i++) {
                    var item = outResult.list[i];

                    var li = $('<li class="item"></il>');
                    var itembox = $('<div class="item-box"></div>');
                    var detail = '<div class="container-box"><img src="' + item.getFullImageThumb + '">';
                    detail += '<div class="mask-layer hide">' + item.description + '</div><a class="am-btn am-btn-warning am-round am-btn-sm download hide">下载模型</a><a class="am-btn am-btn-primary am-round am-btn-sm collect hide">收藏模型</a></div>';
                    detail += '<div class="info-box">';
                    detail += '<a class="title">' + item.name + '</a>';
                    detail += '<div class="price">价格：' + item.price + '元</div>';
                    detail += '<a class="am-btn am-btn-warning am-round am-btn-sm download-box">下载模型</a></div>';
                    
                    itembox.append($(detail));
                    li.append(itembox);

                    $('.content-cont').append(li);
                }

                $('.container-box').on('mouseover',function(){
                    $(this).find('.mask-layer').removeClass('hide');
                    $(this).find('.mask-layer').addClass('active');
                    $(this).find('.download').removeClass('hide');
                    $(this).find('.download').addClass('active');
                    $(this).find('.collect').removeClass('hide');
                    $(this).find('.collect').addClass('active');
                });

                $('.container-box').on('mouseout',function(){
                    $(this).find('.mask-layer').removeClass('active');
                    $(this).find('.mask-layer').addClass('hide');
                    $(this).find('.download').removeClass('active');
                    $(this).find('.download').addClass('hide');
                    $(this).find('.collect').removeClass('active');
                    $(this).find('.collect').addClass('hide');
                });

                if (outResult.hasMore) {
                    $('.content-cont').append($('<div class="hasmore">点击查看更多</div>'));

                    $('.hasmore').on('click', function() {
                        var pindex = $('.content-cont').attr('pindex');
                        if (pindex == undefined) {
                            pindex = 0;
                        } else {
                            pindex = parseInt(pindex) + 1;
                        }
                        $('.content-cont').attr('pindex', pindex);
                        queryResult(1);
                    });
                }
            },
            error: function (error) {
                
            }
        });
    }
    
    queryResult(0);

</script>
</html>