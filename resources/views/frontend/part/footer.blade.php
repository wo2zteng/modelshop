<footer class="footer-box">
    <div class="footer-x">
        <div class="channels">
            <ul>
                <li><div class="info-text with-sep">关于享服</div></li>
                <li><div class="info-text with-sep">联系我们</div></li>
                <li><div class="info-text with-sep">帮助中心 </div></li>
                <li><div class="info-text">反馈建议 </div></li>
            </ul>
        </div>

        <div class="copyright">
            <div>Copyright  2017 www.x3dhome.com All right reserved. &nbsp;&nbsp;&nbsp;&nbsp;粤ICP备17117694号</div>
        </div>

        <div class="sns">
            <ul>
                <li><a href="##" class="am-icon-btn am-primary am-icon-qq"></a></li>
                <li><a href="##" class="am-icon-btn am-success am-icon-weixin"></a></li>
                <li><a href="##" class="am-icon-btn am-danger am-icon-weibo"></a></li>
            </ul>
        </div>
    </div>
</footer>