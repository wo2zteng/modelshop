@inject('modelCategoryService', 'App\Services\ModelCategoryService')

@php 
    $cates = $modelCategoryService->getCategories([
        'parentId' => '0'
    ], false);
@endphp
<nav class="scrollspy-nav am-g" data-am-scrollspynav="{offsetTop: 120}" data-am-sticky>
    <div class="top-header am-g">
        <ul class="navi am-u-sm-6">
            @foreach ($topnavis as $navi)
            <li><a href="{{$navi['anchor']}}">{{$navi['section']}}</a></li>
            @endforeach
        </ul>
        <div class="right-bar am-u-sm-4">
            <div class="am-input-group">
                <span class="am-input-group-label"><i class="am-icon-search am-icon-fw"></i></span>
                <input type="text" class="am-form-field" placeholder="输入关键字搜索您想要的模型">
            </div>
        </div>
        <div class="user-info am-u-sm-2">
            <button class="am-btn am-btn-default am-btn-xs">登录</button>
            <button class="am-btn am-btn-primary  ult am-btn-xs">注册</button>
        </div>
    </div>
    <div class="index-container">
        <div class="index-header am-g">
            <div class="am-u-sm-3"><img src="/res/images/logo.png" class="logo"></div>
            <div class="am-u-sm-9">
                <div class="md-class">
                    <ul class="navi-info">
                        <li class="navi-item"><a style="color:#888;" href="/model/query/0.htm">全部模型</a></li>
                        @foreach ($cates as $cate)
                        <li class="navi-item"><a style="color:#888;" href="/model/query/{{$cate['id']}}.htm">{{$cate['name']}}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</nav>