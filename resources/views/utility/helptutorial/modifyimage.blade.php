<div class="layui-form">
    <div class="layui-layer-outerbox" style="margin-bottom:80px">
        <div class="layui-form-item">
            <label class="layui-form-label"> 新增素材</label>
            <div class="layui-input-block layui-block-middle">
                <div class="layui-big-upload-box">
                    <input type="hidden" name="{{makeElUniqueName('tutorial_id')}}" value="{{ $tutorialId }}">
                    <img id="{{makeElUniqueName('img_upload_new')}}" src="/images/no-pic-back.png">
                    <input type="hidden" name="{{makeElUniqueName('img_file_id')}}" value="">
                    <div class="site-demo-upbar">
                        <input type="file" name="{{makeElUniqueName('new_img')}}" class="layui-upload-file" id="{{makeElUniqueName('new_img')}}">
                    </div>
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
                <legend>已有素材图片</legend>
            </fieldset>
            <ul class="flow-default" id="{{makeElUniqueName('imageDetails')}}">
            </ul>
        </div>
    </div>
</div>

<script>
    layui.use(['form', 'validator', 'uploadUtil'], function(){
        var form = layui.form();
        var $ = layui.jquery;
        var layer = layui.layer;
        var popLayerUtil = layui.popLayerUtil;
        var uploadUtil = layui.uploadUtil;

        form.render();

        uploadUtil.doUpload({
            success: function(fileId, filePath, fileKey) {
                if (fileKey == '{{makeElUniqueName('new_img')}}') {
                    $('#{{makeElUniqueName('img_upload_new')}}').attr('src', filePath);
                    $('input[name=\'{{makeElUniqueName('img_file_id')}}\']').val(fileId);
                    layui.tutorialImageFuncs.addTutorialImage();
                }
            }
        });

        layui.define(function(exports){
            var obj = {
                toQuery: function() {
                    $('#{{makeElUniqueName('imageDetails')}}').html('');
                    var index = layer.load(1);
                    $.ajax({
                        contentType: "application/json",
                        type: 'get',
                        url: '/backstage/api/help-tutorial/edit-detail/query/' + $('input[name=\'{{makeElUniqueName('tutorial_id')}}\']').val(),
                        data: {},
                        success: function (datas) {
                            layer.close(index);
                            for (var i=0; i<datas.length; i++) {
                                var li = $('<li>').css('display', 'inline-block')
                                    .append('<div style="width:300px;height:260px;margin-left:20px;margin-right:20px;margin-top:10px;border:solid 10px #ccc;position:relative"><img style="position:absolute;left:0px;top:0px;width:300px;height:260px" src="' + datas[i].getFullImagePath + '"><button class="layui-btn layui-btn-small layui-btn-danger" style="position:absolute;left:250px;top:5px" onclick="layui.tutorialImageFuncs.deleteTutorialImage(\'' + datas[i].id + '\')"><i class="layui-icon">&#xe640;</i></button></div>');
                                $('#{{makeElUniqueName('imageDetails')}}').append(li);
                            }
                        },
                        error: function (error) {
                            layer.close(index);
                            layui.validator.processValidateError(error);
                        }
                    });
                },
                addTutorialImage: function() {
                    var index = layer.load(1);
                    $.ajax({
                        contentType: "application/json",
                        type: 'post',
                        url: '/backstage/api/help-tutorial/edit-detail/add',
                        data: JSON.stringify({
                            tutorial_id: $('input[name=\'{{makeElUniqueName('tutorial_id')}}\']').val(),
                            image_file_id: $('input[name=\'{{makeElUniqueName('img_file_id')}}\']').val()
                        }),
                        success: function (datas) {
                            layer.close(index);
                            layui.tutorialImageFuncs.toQuery();
                        },
                        error: function (error) {
                            layer.close(index);
                            layui.validator.processValidateError(error);
                        }
                    });
                },
                deleteTutorialImage: function(tutorialImageId) {
                    layer.confirm('确定删除该素材图片？', {
                        btn: ['确定','放弃'],
                        icon: 3
                    }, function(){
                        var index = layer.load(1);
                        $.ajax({
                            contentType: "application/json",
                            type: 'get',
                            url: '/backstage/api/help-tutorial/edit-detail/delete/' + tutorialImageId,
                            data: {},
                            success: function (datas) {
                                layer.close(index);
                                layui.tutorialImageFuncs.toQuery();
                            },
                            error: function (error) {
                                layer.close(index);
                                layui.validator.processValidateError(error);
                            }
                        });
                    }, function(){
                    });
                }
            };
            exports('tutorialImageFuncs', obj);
        });

        layui.tutorialImageFuncs.toQuery();
    });
</script>
