<div class="layui-default-tree-left">
    <ul id="{{makeElUniqueName('left-navi')}}"></ul>
</div>
<div class="layui-default-tree-navi">
    <div class="layui-field-box">
        <div class="layui-form-item" style="margin:0;margin-top:15px;">
            <div class="layui-inline">
                <label class="layui-form-label"></label>
                <div class="layui-input-inline layui-short-input">
                    <input type="hidden" name="{{makeElUniqueName('parent_id')}}">
                </div>
                <label class="layui-form-label" style="width:200px">帮助主题</label>
                <div class="layui-input-inline layui-short-input">
                    <input type="text" placeholder="帮助主题" name="{{makeElUniqueName('title_name')}}" autocomplete="off" class="layui-input">
                </div>
                <div class="layui-input-inline" style="width:auto">
                    <button class="layui-btn" lay-filter="{{makeElUniqueName('search_tutorial')}}"><i class="layui-icon">&#xe615;</i> 搜索</button>&nbsp;
                    <button class="layui-btn layui-btn-normal" lay-filter="{{makeElUniqueName('add_tutorial')}}"><i class="layui-icon">&#xe654; </i> 新增</button>
                </div>
            </div>
        </div>
    </div>
    <div id="{{makeElUniqueName('tbTutorial')}}"></div>
</div>
<script>
layui.use(['jfTable', 'tree'], function(){
    var layer = layui.layer;
    var $ = layui.jquery;
    var jfTable = layui.jfTable;

    layui.define(function(exports){
        var obj = {
            doEdit:function(tutorialId) {
                $.get('/backstage/help-tutorial/edit/'+ tutorialId, {}, function(str){
                    var popLayerUtil = layui.popLayerUtil;
                    popLayerUtil.doPopUp({
                        index: layer.open({
                            id: '{{makeElUniqueName('editHelpTutorial')}}',
                            title: '修改主题条目',
                            type: 1,
                            content: str,
                            area: ['800px', '500px']
                        }),
                        onClose: function() {
                            layui.helpTutorialFuncs.refreshNaviTree();
                        }
                    });
                });
            },
            doEditDetail:function(tutorialId) {
                $.get('/backstage/help-tutorial/edit-detail/'+ tutorialId, {}, function(str){
                    var popLayerUtil = layui.popLayerUtil;
                    popLayerUtil.doPopUp({
                        index: layer.open({
                            id: '{{makeElUniqueName('editHelpTutorialDetail')}}',
                            title: '修改素材图片',
                            type: 1,
                            content: str,
                            area: ['800px', '500px']
                        }),
                        onClose: function() {
                            layui.helpTutorialFuncs.refreshNaviTree();
                        }
                    });
                });
            },
            doDelete:function(tutorialId) {
                layer.confirm('确定删除该条目？', {
                    btn: ['确定','放弃'],
                    icon: 3
                }, function(){
                    var index = layer.load(1);
                    $.ajax({
                        contentType: "application/json",
                        type: 'post',
                        url: '/backstage/api/help-tutorial/delete',
                        data: JSON.stringify({
                            id: tutorialId
                        }),
                        success: function (outResult) {
                            layer.close(index);
                            if (outResult.Success) {
                                layer.msg(outResult.Message, { icon: 6 });
                                layui.helpTutorialFuncs.refreshNaviTree();
                            } else {
                                layer.msg(outResult.Message, { icon: 5 });
                            }
                        },
                        error: function (error) {
                            layer.close(index);
                            layui.validator.processValidateError(error);
                        }
                    });
                }, function(){
                });
            },
            refreshTableGrid: function() {
                $('input[name=\'{{makeElUniqueName('title_name')}}\']').val('');
                $("#{{makeElUniqueName('tbTutorial')}}").jfTable("reload");
            },
            initNaviTree: function() {
                this.doInitNaviTree(layui.helpTutorialFuncs.initTableGrid);
            },
            refreshNaviTree: function() {
                $('#{{makeElUniqueName('left-navi')}}').find('li').each(function() {
                    $(this).remove();
                });
                this.doInitNaviTree(layui.helpTutorialFuncs.refreshTableGrid);
            },
            doInitNaviTree: function(callback) {
                var index = layer.load(1);
                $.ajax({
                    contentType: "application/json",
                    type: 'get',
                    url: '/backstage/api/help-tutorial/gettree',
                    data: JSON.stringify({}),
                    success: function (outResult) {
                        layer.close(index);
                        var data = [{
                            id: 0,
                            name: '服装应用素材库',
                            spread: true,
                            children: outResult
                        }];
                        $('input[name=\'{{makeElUniqueName('parent_id')}}\']').val(data[0].id);
                        layui.tree({
                            elem: '#{{makeElUniqueName('left-navi')}}',
                            skin: 'shihuang',
                            nodes: data,
                            click: function(node) {
                                $('input[name=\'{{makeElUniqueName('parent_id')}}\']').val(node.id);

                                layui.helpTutorialFuncs.refreshTableGrid();
                            }
                        });
                        callback.call(this);
                    },
                    error: function (error) {
                        layer.close(index);
                        layui.validator.processValidateError(error);
                    }
                });
            },
            initTableGrid: function() {
                $("#{{makeElUniqueName('tbTutorial')}}").jfTable({
                    url: '/backstage/api/help-tutorial/query',
                    pageSize:5,
                    page: true,
                    skip: true,
                    first:'首页',
                    last:'尾页',
                    columns: [{
                        text:'操作',
                        name: 'id',
                        width: 310,
                        align: 'center',
                        formatter: function(value, dataItem, index) {
                            var html = '<a class="layui-btn layui-btn-small layui-btn-normal" onclick="layui.helpTutorialFuncs.doEdit(' + value + ')"><i class="layui-icon">&#xe642;</i> 编辑</a>';
                            html += '&nbsp;&nbsp;<a class="layui-btn layui-btn-small layui-btn-danger" onclick="layui.helpTutorialFuncs.doDelete(' + value + ')"><i class="layui-icon">&#xe640;</i> 删除</a>';
                            html += '&nbsp;&nbsp;<a class="layui-btn layui-btn-small layui-btn-success" onclick="layui.helpTutorialFuncs.doEditDetail(' + value + ')"><i class="layui-icon">&#xe620;</i> 修改素材图片</a>';

                            return html;
                        }
                    },{
                        text:'素材主题',
                        name: 'title',
                        width: 200,
                        align: 'center',
                    },{
                        text:'同级排序号',
                        name: 'sort_order',
                        width: 80,
                        align: 'center',
                    },{
                        text:'完整主题路径',
                        name: 'fullPath',
                        width: 400,
                        align: 'left',
                    }],
                    method: 'get',
                    queryParam: {
                        name:$('input[name=\'{{makeElUniqueName('title_name')}}\']').val(),
                        parentId:$('input[name=\'{{makeElUniqueName('parent_id')}}\']').val(),
                    },
                    toolbarClass: 'layui-btn-small',
                    onBeforeLoad: function (param) {
                        return $.extend(param, {
                            name:$('input[name=\'{{makeElUniqueName('title_name')}}\']').val(),
                            parentId:$('input[name=\'{{makeElUniqueName('parent_id')}}\']').val(),
                        });
                    },
                    onLoadSuccess: function (data) {
                        return data;
                    },
                    dataFilter:function (data) {
                        return data;
                    }
                });
            }
        };
        exports('helpTutorialFuncs', obj);
    });

    layui.helpTutorialFuncs.initNaviTree();

    $('.layui-btn[lay-filter=\'{{makeElUniqueName('search_tutorial')}}\']').on('click', function(){
        $("#{{makeElUniqueName('tbTutorial')}}").jfTable("reload");
    });

    $('.layui-btn[lay-filter=\'{{makeElUniqueName('add_tutorial')}}\']').on('click', function(){
        var parentId = $('input[name=\'{{makeElUniqueName('parent_id')}}\']').val();
        if (parentId != '' && parentId != undefined) {
            $.get('/backstage/help-tutorial/create/' + parentId, {}, function(str){
                var popLayerUtil = layui.popLayerUtil;
                popLayerUtil.doPopUp({
                    index: layer.open({
                        id: '{{makeElUniqueName('createTutorial')}}',
                        title: '新增素材主题条目',
                        type: 1,
                        content: str,
                        area: ['800px', '500px']
                    }),
                    onClose: function() {
                        layui.helpTutorialFuncs.refreshNaviTree();
                    }
                });
            });
        } else {
            layer.msg('还没有选择父级分类', { icon: 5 });
        }

    });
});
</script>
