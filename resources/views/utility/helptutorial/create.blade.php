<div class="layui-form">
    <div class="layui-layer-outerbox" style="margin-bottom:80px">
        <div class="layui-form-item">
            <label class="layui-form-label"><span style="color:red">*</span> 素材主题</label>
            <div class="layui-input-block">
                <input type="hidden" name="{{makeElUniqueName('helptutorial_id')}}" value="{{ isset($tutorial)?$tutorial->id:'' }}">
                <input type="text" name="{{makeElUniqueName('tutorial_title')}}" value="{{ isset($tutorial)?$tutorial->title:'' }}" required lay-verify="required" placeholder="标识该分类的显式名称，必填（*）" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label"><span style="color:red">*</span> 上级主题</label>
            <div class="layui-input-block">
                <select name="{{makeElUniqueName('parent_tutorial')}}" lay-search required lay-verify="required">
                    <option value='0' {{ isset($tutorial)?(empty($tutorial->parent_id)?'selected':''):'' }}>根级主题</option>
                    @foreach ($possiableParents as $possiableParent)
                        @if (isset($tutorial))
                            <option value='{{$possiableParent->id}}' {{ $tutorial->parent_id==$possiableParent->id?'selected':'' }}>{{ $possiableParent->fullPath() }}</option>
                        @else
                            <option value='{{$possiableParent->id}}' {{ $possiableParent->id==$parentId?'selected':'' }}>{{ $possiableParent->fullPath() }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label"><span style="color:red">*</span> 内容详情</label>
            <div class="layui-input-block">
                <textarea class="layui-textarea" id="{{makeElUniqueName('description')}}" name="{{makeElUniqueName('description')}}" style="display: none">
                    @if (isset($tutorial))
                        {!! $tutorial->content !!}
                    @endif
                </textarea>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label"><span style="color:red">*</span> 排序号</label>
            <div class="layui-input-block">
                <input type="text" name="{{makeElUniqueName('sort_order')}}" value="{{ isset($tutorial)?$tutorial->sort_order:'' }}" required lay-verify="required|number" placeholder="同级的排序号" autocomplete="off" class="layui-input">
            </div>
        </div>
        @if (!isset($tutorial))
        <div class="layui-form-item">
            <label class="layui-form-label">素材图片</label>
            <div class="layui-input-block">
                @include('componet._multi_uploader')
            </div>
        </div>
        @endif
    </div>
    <div class="layui-dialog-bar">
        <button class="layui-btn" lay-submit lay-filter="{{makeElUniqueName('tutorial_save')}}"><i class="layui-icon">&#xe605;</i> 保存</button>&nbsp;
        <button class="layui-btn layui-btn-primary" onclick="layer.close(layui.popLayerUtil.index);"><i class="layui-icon">&#x1006;</i>  关闭</button>
    </div>
</div>

<script>
layui.use(['form', 'validator', 'reditorUtil', 'multifileuploader'], function(){
    var form = layui.form();
    var $ = layui.jquery;
    var popLayerUtil = layui.popLayerUtil;
    var reditorUtil = layui.reditorUtil;
    var multifileuploader = layui.multifileuploader;

    @if (!isset($tutorial))
    multifileuploader.init({
        uploader: 'uploader',
        paste: 'uploader',
        filePicker: 'filePicker',
        addButton: 'filePicker2',
        successfiles: 'successfiles',
        dnd: 'dndArea',
        guidField: 'guidField'
    });
    @endif
    //按layui的要求，如果要生成表单select/checkbox这些，必须先调用form.render()方法
    form.render();

    var descIndex = reditorUtil.doInitEditor({elemId:'{{makeElUniqueName('description')}}'});

    //所有ajax请求的api，都必须放到/backstage/api下面，避免被CSRF拦截
    form.on('submit({{makeElUniqueName('tutorial_save')}})', function(data){
        var index = layer.load(1);
        var url = '/backstage/api/help-tutorial/savenew';
        var postParam = {
            title: data.field['{{makeElUniqueName('tutorial_title')}}'],
            parent_id: data.field['{{makeElUniqueName('parent_tutorial')}}'],
            sort_order: data.field['{{makeElUniqueName('sort_order')}}'],
            @if (!isset($tutorial))
            sub_images: $('#successfiles').val(),
            guidField: $('#guidField').val(),
            @endif
            content: reditorUtil.getContent(descIndex),
        };
        var categoryId = data.field['{{makeElUniqueName('helptutorial_id')}}'];
        if (categoryId != '') {
            url = '/backstage/api/help-tutorial/update';
            postParam.id = categoryId;
        }
        $.ajax({
            contentType: "application/json",
            type: 'post',
            url: url,
            data: JSON.stringify(postParam),
            success: function (outResult) {
                layer.close(index);
                if (outResult.Success) {
                    layer.msg(outResult.Message, { icon: 6 });
                    layer.close(popLayerUtil.index);
                    popLayerUtil.onClose();
                } else {
                    layer.msg(outResult.Message, { icon: 5 });
                }
            },
            error: function (error) {
                layer.close(index);
                layui.validator.processValidateError(error);
            }
        });
        return false;
    });
});
</script>
