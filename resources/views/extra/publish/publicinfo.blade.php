@extends('layouts.app')

@section('content')
<div class="layui-body" style="left:20px">
    <div class="layui-form layui-layer-outerbox">
        <div class="layui-default-tree-left">
            <ul id="left-navi"></ul>
        </div>
        <div class="layui-default-tree-navi">
            <input type="hidden" name="parent_id">
            <blockquote class="layui-elem-quote" style="color:green;font-weight:bold;font-size:20px;padding-left:30px" id="title">&nbsp;</blockquote>
            <div style="width:90%;margin-left:20px" id="content"></div>
            <fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
                <legend>对应素材图片</legend>
            </fieldset>

            <ul class="flow-default" id="imageDetails">
            </ul>
        </div>
    </div>
</div>

<script src="/vendor/layui/layui.js" charset="utf-8"></script>

<script>
layui.use(['tree', 'layer', 'jquery', 'flow'], function(){
    var layer = layui.layer;
    var $ = layui.jquery;
    var flow = layui.flow;

    layui.define(function(exports){
        var obj = {
            initNaviTree: function() {
                this.doInitNaviTree();
            },
            doInitNaviTree: function() {
                var index = layer.load(1);
                $.ajax({
                    contentType: "application/json",
                    type: 'get',
                    url: '/publish/tutorial/gettree',
                    data: JSON.stringify({}),
                    success: function (outResult) {
                        layer.close(index);
                        var data = [{
                            id: 0,
                            name: '应用素材库',
                            spread: true,
                            children: outResult
                        }];
                        $('input[name=\'parent_id\']').val(data[0].id);
                        layui.tree({
                            elem: '#left-navi',
                            skin: 'shihuang',
                            nodes: data,
                            click: function(node) {
                                $('input[name=\'parent_id\']').val(node.id);

                                layui.commonPublishFuncs.doQueryTutorialDetail(node.name);
                            }
                        });

                    },
                    error: function (error) {
                        layer.close(index);
                        layui.validator.processValidateError(error);
                    }
                });
            },
            doQueryTutorialDetail: function(title) {
                var index = layer.load(1);
                $.ajax({
                    contentType: "application/json",
                    type: 'get',
                    url: '/publish/tutorial/' + $('input[name=\'parent_id\']').val(),
                    data: JSON.stringify({}),
                    success: function (outResult) {
                        layer.close(index);

                        $('#title').html(title);
                        $('#content').html(outResult.content);

                        $('#imageDetails').html('');

                        flow.load({
                            elem: '#imageDetails',
                            scrollElem: '#imageDetails',
                            done: function(page, next){
                                layui.commonPublishFuncs.doGetDetailImages(page, 10, function(datas) {
                                    var data = datas.list;
                                    var lis = [];

                                    for (var i=0; i<data.length; i++) {
                                        lis.push('<li style="display:inline-block"><div style="width:300px;height:225px;margin-left:20px;margin-right:20px;margin-top:10px;border:solid 10px #ccc" onclick="layui.commonPublishFuncs.doShowBigImage(\'' + data[i].getFullImagePath + '\')"><img src="' + data[i].getFullImagePath + '" width="300" height="225"></div></li>')
                                    }

                                    next(lis.join(''), datas.hasMore);
                                });
                            }
                        });
                    },
                    error: function (error) {
                        layer.close(index);
                        layui.validator.processValidateError(error);
                    }
                });
            },
            doGetDetailImages: function(pageNumber, pageSize, callback) {
                $.ajax({
                    contentType: "application/json",
                    type: 'get',
                    url: '/publish/tutorial/getdetail/getimages',
                    data: {
                        tutorial_id: $('input[name=\'parent_id\']').val(),
                        pageNumber: pageNumber,
                        pageSize: pageSize
                    },
                    success: function (outResult) {
                        callback.call(this, outResult);
                    },
                    error: function (error) {
                        layer.close(index);
                        layui.validator.processValidateError(error);
                    }
                });
            },
            doShowBigImage: function(imagePath) {
                layer.open({
                    id: '{{makeElUniqueName('showBigImage')}}',
                    title: '显示大图',
                    type: 1,
                    content: "<div style='width:780px;height:580px;border:solid 10px #ddd'><img style='width:780px;height:580px' src='" + imagePath + "'></div>",
                    area: ['800px', '645px']
                })
            }
        };
        exports('commonPublishFuncs', obj);
    });



    layui.commonPublishFuncs.initNaviTree();

});
</script>
@endsection
