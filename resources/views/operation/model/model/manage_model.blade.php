<div class="layui-default-tree-left">
    <ul id="{{makeElUniqueName('left-navi')}}"></ul>
</div>
<div class="layui-default-tree-navi">
    <div class="layui-field-box layui-form">
        <div class="layui-form-item" style="margin:0;margin-top:15px;">
            <div class="layui-inline">
                <label class="layui-form-label">品类</label>
                <div class="layui-input-inline layui-short-input">
                    <input type="hidden" name="{{makeElUniqueName('category_id')}}">
                    <select name="{{makeElUniqueName('classes')}}" lay-search>
                        <option value="0">(===请选择品类===)</option>
                    </select>
                </div>
                <label class="layui-form-label" style="width:100px">名称</label>
                <div class="layui-input-inline layui-short-input">
                    <input type="text" placeholder="模型名称" name="{{makeElUniqueName('model_name')}}" autocomplete="off" class="layui-input">
                </div>
                <label class="layui-form-label" style="width:100px">所有人</label>
                <div class="layui-input-inline layui-short-input">
                    <input type="text" placeholder="模型所有人" name="{{makeElUniqueName('owner_name')}}" autocomplete="off" class="layui-input">
                </div>
                <div class="layui-input-inline" style="width:auto">
                    <button class="layui-btn" lay-filter="{{makeElUniqueName('search_model')}}"><i class="layui-icon">&#xe615;</i> 搜索</button>
                </div>
            </div>
        </div>
    </div>
    <div id="{{makeElUniqueName('tbModel')}}"></div>
</div>
<script>
layui.use(['jfTable', 'tree', 'form'], function(){
    var layer = layui.layer;
    var $ = layui.jquery;
    var jfTable = layui.jfTable;

    var form = layui.form();

    form.render();

    layui.define(function(exports){
        var obj = {
            doSetting:function(modelId) {
                $.get('/backstage/model-oper/settings/'+ modelId, {}, function(str){
                    var popLayerUtil = layui.popLayerUtil;
                    popLayerUtil.doPopUp({
                        index: layer.open({
                            id: '{{makeElUniqueName('editCategory')}}',
                            title: '修改模型分类',
                            type: 1,
                            content: str,
                            area: ['600px', '700px']
                        }),
                        onClose: function() {
                            layui.modelIndexFuncs.refreshNaviTree();
                        }
                    });
                });
            },
            releaseClass: function() {
                var categoryId = $('input[name=\'{{makeElUniqueName('category_id')}}\']').val();

                var index = layer.load(1);
                $.ajax({
                    contentType: "application/json",
                    type: 'get',
                    url: '/backstage/api/model_category_class/getclass/' + categoryId,
                    success: function (outResult) {
                        layer.close(index);

                        var classSelect = $("select[name='{{makeElUniqueName('classes')}}']");
                        classSelect.find('option').each(function(){
                            $(this).remove();
                        });

                        classSelect.append($('<option>').attr('value', 0).text('(===请选择品类===)'));

                        for (var i=0; i<outResult.length; i++) {
                            classSelect.append($('<option>').attr('value', outResult[i].id).text(outResult[i].getFullPath));
                        }

                        form.render();
                    }
                });
            },
            refreshTableGrid: function() {
                $('input[name=\'{{makeElUniqueName('category_name')}}\']').val('');
                $("#{{makeElUniqueName('tbModel')}}").jfTable("reload");
            },
            initNaviTree: function() {
                this.doInitNaviTree(layui.modelIndexFuncs.initTableGrid);
            },
            refreshNaviTree: function() {
                $('#{{makeElUniqueName('left-navi')}}').find('li').each(function() {
                    $(this).remove();
                });
                this.doInitNaviTree(layui.modelIndexFuncs.refreshTableGrid);
            },
            doInitNaviTree: function(callback) {
                var index = layer.load(1);
                $.ajax({
                    contentType: "application/json",
                    type: 'get',
                    url: '/backstage/api/model-model-category/gettree',
                    data: JSON.stringify({}),
                    success: function (outResult) {
                        layer.close(index);
                        var data = [{
                            id: 0,
                            name: '所有分类',
                            spread: true,
                            children: outResult
                        }];
                        $('input[name=\'{{makeElUniqueName('category_id')}}\']').val(data[0].id);
                        layui.modelIndexFuncs.releaseClass();
                        layui.tree({
                            elem: '#{{makeElUniqueName('left-navi')}}',
                            skin: 'shihuang',
                            nodes: data,
                            click: function(node) {
                                $('input[name=\'{{makeElUniqueName('category_id')}}\']').val(node.id);
                                layui.modelIndexFuncs.releaseClass();
                                layui.modelIndexFuncs.refreshTableGrid();
                            }
                        });
                        callback.call(this);
                    },
                    error: function (error) {
                        layer.close(index);
                        layui.validator.processValidateError(error);
                    }
                });
            },
            initTableGrid: function() {
                $("#{{makeElUniqueName('tbModel')}}").jfTable({
                    url: '/backstage/api/model-oper/query',
                    pageSize:5,
                    page: true,
                    skip: true,
                    first:'首页',
                    last:'尾页',
                    columns: [{
                        text:'操作',
                        name: 'id',
                        width: 150,
                        align: 'center',
                        formatter: function(value, dataItem, index) {
                            var html = '<a class="layui-btn layui-btn-small layui-btn-normal" onclick="layui.modelIndexFuncs.doSetting(' + value + ')"><i class="layui-icon">&#xe642;</i> 设置首页</a>';
                            return html;
                        }
                    },{
                        text:'模型所有人',
                        name: 'getOwnerIconPath',
                        width: 100,
                        align: 'center',
                        formatter:function(value, dataItem, index) {
                            return '<img src=\'' + value + '\' width=70 height=60></img>';
                        }
                    },{
                        text:'模型名称',
                        name: 'name',
                        width: 100,
                        align: 'center',
                    },{
                        text:'模型缩略图',
                        name: 'getFullImage',
                        width: 80,
                        align: 'center',
                        formatter:function(value, dataItem, index) {
                            return '<img src=\'' + value + '\' width=70 height=60></img>';
                        }
                    },{
                        text:'模型描述',
                        name: 'description',
                        width: 200,
                        align: 'center',
                    },{
                        text:'作者',
                        name: 'author.nick_name',
                        width: 100,
                        align: 'center',
                    },{
                        text:'价格',
                        name: 'price',
                        width: 80,
                        align: 'center',
                    }],
                    method: 'get',
                    queryParam: {
                        name:$('input[name=\'{{makeElUniqueName('model_name')}}\']').val(),
                        classId:$('select[name=\'{{makeElUniqueName('classes')}}\']').val(),
                        categoryId:$('input[name=\'{{makeElUniqueName('category_id')}}\']').val(),
                        owner_name:$('input[name=\'{{makeElUniqueName('owner_name')}}\']').val(),
                    },
                    toolbarClass: 'layui-btn-small',
                    onBeforeLoad: function (param) {
                        return $.extend(param, {
                            name:$('input[name=\'{{makeElUniqueName('model_name')}}\']').val(),
                            classId:$('select[name=\'{{makeElUniqueName('classes')}}\']').val(),
                            categoryId:$('input[name=\'{{makeElUniqueName('category_id')}}\']').val(),
                            owner_name:$('input[name=\'{{makeElUniqueName('owner_name')}}\']').val(),
                        });
                    },
                    onLoadSuccess: function (data) {
                        return data;
                    },
                    dataFilter:function (data) {
                        return data;
                    }
                });
            }
        };
        exports('modelIndexFuncs', obj);
    });

    layui.modelIndexFuncs.initNaviTree();

    $('.layui-btn[lay-filter=\'{{makeElUniqueName('search_model')}}\']').on('click', function(){
        $("#{{makeElUniqueName('tbModel')}}").jfTable("reload");
    });

    $('.layui-btn[lay-filter=\'{{makeElUniqueName('add_model')}}\']').on('click', function(){
        $.get('/backstage/model/create', {}, function(str){
            var popLayerUtil = layui.popLayerUtil;
            popLayerUtil.doPopUp({
                index: layer.open({
                    id: '{{makeElUniqueName('createCategory')}}',
                    title: '新增模型分类',
                    type: 1,
                    content: str,
                    area: ['800px', '600px']
                }),
                onClose: function() {
                    layui.modelIndexFuncs.refreshNaviTree();
                }
            });
        });
    });
});
</script>
