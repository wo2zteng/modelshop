<div class="layui-form">
    <div class="layui-layer-outerbox" style="margin-bottom:80px">
        <div class="layui-form-item">
            <label class="layui-form-label">模型名称</label>
            <div class="layui-input-block">
                <input type="hidden" name="{{makeElUniqueName('model_id')}}" value="{{ isset($model)?$model->id:'' }}">
                <input value="{{ isset($model)?$model->name:'' }}" readonly class="layui-input" style="border-width:0px">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">缩略图</label>
            <div class="layui-input-block">
            <div class="layui-big-upload-box" style="height:160px;width:200px">
                    <img id="{{makeElUniqueName('img_thumb_new')}}" style="height:160px;width:200px" src="{{ $model->getFullImage() }}">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label"><span style="color:red">*</span> 设为最新</label>
            <div class="layui-input-block">
                <select name="{{makeElUniqueName('is_new')}}" lay-search required lay-verify="required">
                    @foreach (\App\BaseDictionary::$YES_NO_MAP as $item)
                        @if (isset($model))
                            <option value='{{$item['key']}}' {{ $model->is_new==$item['key']?'selected':'' }}>{{$item['text']}}</option>
                        @else
                            <option value='{{$item['key']}}'>{{$item['text']}}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label"><span style="color:red">*</span> 新品排序</label>
            <div class="layui-input-block">
                <input type="text" name="{{makeElUniqueName('is_new_order')}}" value="{{ isset($model)?$model->is_new_order:'' }}" required lay-verify="required|number" placeholder="首页最新上架模型的显示顺序" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label"><span style="color:red">*</span> 设为精品</label>
            <div class="layui-input-block">
                <select name="{{makeElUniqueName('is_recommand')}}" lay-search required lay-verify="required">
                    @foreach (\App\BaseDictionary::$YES_NO_MAP as $item)
                        @if (isset($model))
                            <option value='{{$item['key']}}' {{ $model->is_recommand==$item['key']?'selected':'' }}>{{$item['text']}}</option>
                        @else
                            <option value='{{$item['key']}}'>{{$item['text']}}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label"><span style="color:red">*</span> 精品排序</label>
            <div class="layui-input-block">
                <input type="text" name="{{makeElUniqueName('is_recommand_order')}}" value="{{ isset($model)?$model->is_recommand_order:'' }}" required lay-verify="required|number" placeholder="首页精品推荐模型的显示顺序" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label"><span style="color:red">*</span> 设为素材</label>
            <div class="layui-input-block">
                <select name="{{makeElUniqueName('is_recommand_materials')}}" lay-search required lay-verify="required">
                    @foreach (\App\BaseDictionary::$YES_NO_MAP as $item)
                        @if (isset($model))
                            <option value='{{$item['key']}}' {{ $model->is_recommand_materials==$item['key']?'selected':'' }}>{{$item['text']}}</option>
                        @else
                            <option value='{{$item['key']}}'>{{$item['text']}}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label"><span style="color:red">*</span> 素材排序</label>
            <div class="layui-input-block">
                <input type="text" name="{{makeElUniqueName('is_recommand_materials_order')}}" value="{{ isset($model)?$model->is_recommand_materials_order:'' }}" required lay-verify="required|number" placeholder="首页精品推荐模型的显示顺序" autocomplete="off" class="layui-input">
            </div>
        </div>
    </div>
    <div class="layui-dialog-bar">
        <button class="layui-btn" lay-submit lay-filter="{{makeElUniqueName('settings_save')}}"><i class="layui-icon">&#xe605;</i> 保存</button>&nbsp;
        <button class="layui-btn layui-btn-primary" onclick="layer.close(layui.popLayerUtil.index);"><i class="layui-icon">&#x1006;</i>  关闭</button>
    </div>
</div>

<script>
layui.use(['form', 'validator'], function(){
    var form = layui.form();
    var $ = layui.jquery;
    var popLayerUtil = layui.popLayerUtil;
    //按layui的要求，如果要生成表单select/checkbox这些，必须先调用form.render()方法
    form.render();

    form.on('submit({{makeElUniqueName('settings_save')}})', function(data){
        var index = layer.load(1);
        var url = '/backstage/api/model-oper/settings/dosave';
        var postParam = {
            id: data.field['{{makeElUniqueName('model_id')}}'],
            is_new: data.field['{{makeElUniqueName('is_new')}}'],
            is_new_order: data.field['{{makeElUniqueName('is_new_order')}}'],
            is_recommand: data.field['{{makeElUniqueName('is_recommand')}}'],
            is_recommand_order: data.field['{{makeElUniqueName('is_recommand_order')}}'],
            is_recommand_materials: data.field['{{makeElUniqueName('is_recommand_materials')}}'],
            is_recommand_materials_order: data.field['{{makeElUniqueName('is_recommand_materials_order')}}'],
        };

        $.ajax({
            contentType: "application/json",
            type: 'post',
            url: url,
            data: JSON.stringify(postParam),
            success: function (outResult) {
                layer.close(index);
                if (outResult.Success) {
                    layer.msg(outResult.Message, { icon: 6 });
                    layer.close(popLayerUtil.index);
                    popLayerUtil.onClose();
                } else {
                    layer.msg(outResult.Message, { icon: 5 });
                }
            },
            error: function (error) {
                layer.close(index);
                layui.validator.processValidateError(error);
            }
        });

        return false;
    });
});
</script>