<div class="layui-form layui-layer-outerbox">
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 分类名称</label>
        <div class="layui-input-block">
            <input type="hidden" name="{{makeElUniqueName('category_id')}}" value="{{ isset($category)?$category->id:'' }}">
            <input type="text" name="{{makeElUniqueName('category_name')}}" value="{{ isset($category)?$category->name:'' }}" required lay-verify="required" placeholder="标识该分类的显式名称，必填（*）" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 上级分类</label>
        <div class="layui-input-block">
            <select name="{{makeElUniqueName('parent_category')}}" lay-search required lay-verify="required">
                <option value='0' {{ isset($category)?(empty($category->parent_id)?'selected':''):'' }}>根级分类</option>
                @foreach ($possiableParents as $possiableParent)
                    @if (isset($category))
                        <option value='{{$possiableParent->id}}' {{ $category->parent_id==$possiableParent->id?'selected':'' }}>{{ $possiableParent->getFullPath() }}</option>
                    @else
                        <option value='{{$possiableParent->id}}' {{ $possiableParent->id==$parentId?'selected':'' }}>{{ $possiableParent->getFullPath() }}</option>
                    @endif
                @endforeach
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 排序号</label>
        <div class="layui-input-block">
            <input type="text" name="{{makeElUniqueName('sort_order')}}" value="{{ isset($category)?$category->sort_order:'' }}" required lay-verify="required|number" placeholder="同级分类的排序号" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item layui-form-center">
            <button class="layui-btn" lay-submit lay-filter="{{makeElUniqueName('category_save')}}">保存</button>
            <button class="layui-btn layui-btn-primary" onclick="layer.close(layui.popLayerUtil.index);">关闭</button>
    </div>
</div>

<script>
layui.use(['form', 'validator'], function(){
    var form = layui.form();
    var $ = layui.jquery;
    var popLayerUtil = layui.popLayerUtil;
    //按layui的要求，如果要生成表单select/checkbox这些，必须先调用form.render()方法
    form.render();

    //所有ajax请求的api，都必须放到/backstage/api下面，避免被CSRF拦截
    form.on('submit({{makeElUniqueName('category_save')}})', function(data){
        var index = layer.load(1);
        var url = '/backstage/api/model-category/savenew';
        var postParam = {
            name: data.field['{{makeElUniqueName('category_name')}}'],
            parent_id: data.field['{{makeElUniqueName('parent_category')}}'],
            sort_order: data.field['{{makeElUniqueName('sort_order')}}'],
        };
        var categoryId = data.field['{{makeElUniqueName('category_id')}}'];
        if (categoryId != '') {
            url = '/backstage/api/model-category/update';
            postParam.id = categoryId;
        }
        $.ajax({
            contentType: "application/json",
            type: 'post',
            url: url,
            data: JSON.stringify(postParam),
            success: function (outResult) {
                layer.close(index);
                if (outResult.Success) {
                    layer.msg(outResult.Message, { icon: 6 });
                    layer.close(popLayerUtil.index);
                    popLayerUtil.onClose();
                } else {
                    layer.msg(outResult.Message, { icon: 5 });
                }
            },
            error: function (error) {
                layer.close(index);
                layui.validator.processValidateError(error);
            }
        });
        return false;
    });
});
</script>
