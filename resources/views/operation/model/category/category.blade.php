<div class="layui-default-tree-left">
    <ul id="{{makeElUniqueName('left-navi')}}"></ul>
</div>
<div class="layui-default-tree-navi">
    <div class="layui-field-box">
        <div class="layui-form-item" style="margin:0;margin-top:15px;">
            <div class="layui-inline">
                <label class="layui-form-label"></label>
                <div class="layui-input-inline layui-short-input">
                    <input type="hidden" name="{{makeElUniqueName('parent_id')}}">
                </div>
                <label class="layui-form-label" style="width:200px">分类名称</label>
                <div class="layui-input-inline layui-short-input">
                    <input type="text" placeholder="分类名称" name="{{makeElUniqueName('category_name')}}" autocomplete="off" class="layui-input">
                </div>
                <div class="layui-input-inline" style="width:auto">
                    <button class="layui-btn" lay-filter="{{makeElUniqueName('search_category')}}"><i class="layui-icon">&#xe615;</i> 搜索</button>&nbsp;
                    <button class="layui-btn layui-btn-normal" lay-filter="{{makeElUniqueName('add_category')}}"><i class="layui-icon">&#xe654; </i> 新增</button>
                </div>
            </div>
        </div>
    </div>
    <div id="{{makeElUniqueName('tbCategory')}}"></div>
</div>
<script>
layui.use(['jfTable', 'tree'], function(){
    var layer = layui.layer;
    var $ = layui.jquery;
    var jfTable = layui.jfTable;

    layui.define(function(exports){
        var obj = {
            doEdit:function(categoryId) {
                $.get('/backstage/model-category/edit/'+ categoryId, {}, function(str){
                    var popLayerUtil = layui.popLayerUtil;
                    popLayerUtil.doPopUp({
                        index: layer.open({
                            id: '{{makeElUniqueName('editCategory')}}',
                            title: '修改模型分类',
                            type: 1,
                            content: str,
                            area: ['500px', '300px']
                        }),
                        onClose: function() {
                            layui.modelCategoryFuncs.refreshNaviTree();
                        }
                    });
                });
            },
            doDelete:function(categoryId) {
                layer.confirm('确定删除该模型分类？', {
                    btn: ['确定','放弃'],
                    icon: 3
                }, function(){
                    var index = layer.load(1);
                    $.ajax({
                        contentType: "application/json",
                        type: 'post',
                        url: '/backstage/api/model-category/delete',
                        data: JSON.stringify({
                            id: categoryId
                        }),
                        success: function (outResult) {
                            layer.close(index);
                            if (outResult.Success) {
                                layer.msg(outResult.Message, { icon: 6 });
                                layui.modelCategoryFuncs.refreshNaviTree();
                            } else {
                                layer.msg(outResult.Message, { icon: 5 });
                            }
                        },
                        error: function (error) {
                            layer.close(index);
                            layui.validator.processValidateError(error);
                        }
                    });
                }, function(){
                });
            },
            refreshTableGrid: function() {
                $('input[name=\'{{makeElUniqueName('category_name')}}\']').val('');
                $("#{{makeElUniqueName('tbCategory')}}").jfTable("reload");
            },
            initNaviTree: function() {
                this.doInitNaviTree(layui.modelCategoryFuncs.initTableGrid);
            },
            refreshNaviTree: function() {
                $('#{{makeElUniqueName('left-navi')}}').find('li').each(function() {
                    $(this).remove();
                });
                this.doInitNaviTree(layui.modelCategoryFuncs.refreshTableGrid);
            },
            doInitNaviTree: function(callback) {
                var index = layer.load(1);
                $.ajax({
                    contentType: "application/json",
                    type: 'get',
                    url: '/backstage/api/model-category/gettree',
                    data: JSON.stringify({}),
                    success: function (outResult) {
                        layer.close(index);
                        var data = [{
                            id: 0,
                            name: '所有分类',
                            spread: true,
                            children: outResult
                        }];
                        $('input[name=\'{{makeElUniqueName('parent_id')}}\']').val(data[0].id);
                        layui.tree({
                            elem: '#{{makeElUniqueName('left-navi')}}',
                            skin: 'shihuang',
                            nodes: data,
                            click: function(node) {
                                $('input[name=\'{{makeElUniqueName('parent_id')}}\']').val(node.id);

                                layui.modelCategoryFuncs.refreshTableGrid();
                            }
                        });
                        callback.call(this);
                    },
                    error: function (error) {
                        layer.close(index);
                        layui.validator.processValidateError(error);
                    }
                });
            },
            initTableGrid: function() {
                $("#{{makeElUniqueName('tbCategory')}}").jfTable({
                    url: '/backstage/api/model-category/query',
                    pageSize:5,
                    page: true,
                    skip: true,
                    first:'首页',
                    last:'尾页',
                    columns: [{
                        text:'操作',
                        name: 'id',
                        width: 250,
                        align: 'center',
                        formatter: function(value, dataItem, index) {
                            var html = '<a class="layui-btn layui-btn-small layui-btn-normal" onclick="layui.modelCategoryFuncs.doEdit(' + value + ')"><i class="layui-icon">&#xe642;</i> 编辑</a>';
                            html += '&nbsp;&nbsp;<a class="layui-btn layui-btn-small layui-btn-danger" onclick="layui.modelCategoryFuncs.doDelete(' + value + ')"><i class="layui-icon">&#xe640;</i> 删除</a>';
                            return html;
                        }
                    },{
                        text:'模型分类名称',
                        name: 'name',
                        width: 200,
                        align: 'center',
                    },{
                        text:'同级排序号',
                        name: 'sort_order',
                        width: 200,
                        align: 'center',
                    }],
                    method: 'get',
                    queryParam: {
                        name:$('input[name=\'{{makeElUniqueName('category_name')}}\']').val(),
                        parentId:$('input[name=\'{{makeElUniqueName('parent_id')}}\']').val(),
                    },
                    toolbarClass: 'layui-btn-small',
                    onBeforeLoad: function (param) {
                        return $.extend(param, {
                            name:$('input[name=\'{{makeElUniqueName('category_name')}}\']').val(),
                            parentId:$('input[name=\'{{makeElUniqueName('parent_id')}}\']').val(),
                        });
                    },
                    onLoadSuccess: function (data) {
                        return data;
                    },
                    dataFilter:function (data) {
                        return data;
                    }
                });
            }
        };
        exports('modelCategoryFuncs', obj);
    });

    layui.modelCategoryFuncs.initNaviTree();

    $('.layui-btn[lay-filter=\'{{makeElUniqueName('search_category')}}\']').on('click', function(){
        $("#{{makeElUniqueName('tbCategory')}}").jfTable("reload");
    });

    $('.layui-btn[lay-filter=\'{{makeElUniqueName('add_category')}}\']').on('click', function(){
        var parentId = $('input[name=\'{{makeElUniqueName('parent_id')}}\']').val();
        if (parentId != '' && parentId != undefined) {
            $.get('/backstage/model-category/create/' + parentId, {}, function(str){
                var popLayerUtil = layui.popLayerUtil;
                popLayerUtil.doPopUp({
                    index: layer.open({
                        id: '{{makeElUniqueName('createCategory')}}',
                        title: '新增模型分类',
                        type: 1,
                        content: str,
                        area: ['500px', '300px']
                    }),
                    onClose: function() {
                        layui.modelCategoryFuncs.refreshNaviTree();
                    }
                });
            });
        } else {
            layer.msg('还没有选择父级分类', { icon: 5 });
        }

    });
});
</script>
