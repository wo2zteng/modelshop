<div class="layui-field-box">
    <div class="layui-form-item layui-form" style="margin:0;margin-top:15px;">
        <div class="layui-inline">
            <label class="layui-form-label">品类代码</label>
            <div class="layui-input-inline layui-short-input">
                <input type="text" placeholder="" name="{{makeElUniqueName('class_code')}}" autocomplete="off" class="layui-input">
            </div>
            <label class="layui-form-label">品类名称</label>
            <div class="layui-input-inline layui-short-input">
                <input type="text" placeholder="" name="{{makeElUniqueName('class_name')}}" autocomplete="off" class="layui-input">
            </div>
            <label class="layui-form-label">分类类目</label>
            <div class="layui-input-inline layui-short-input">
                <select name="{{makeElUniqueName('category')}}" lay-search>
                    <option value="0">(请选择分类类目)</option>
                    @foreach ($categories as $category)
                        <option value='{{$category['id']}}'>{{$category['getFullPath']}}</option>
                    @endforeach
                </select>
            </div>
            <div class="layui-input-inline" style="width:auto">
                <button class="layui-btn" lay-filter="{{makeElUniqueName('search_class')}}"><i class="layui-icon">&#xe615;</i> 搜索</button>&nbsp;
                <button class="layui-btn layui-btn-normal" lay-filter="{{makeElUniqueName('add_class')}}"><i class="layui-icon">&#xe654; </i> 新增</button>
            </div>
        </div>
    </div>
</div>
<div id="{{makeElUniqueName('tbClass')}}"></div>
<script>
layui.use(['jfTable', 'form'], function(){
    var layer = layui.layer;
    var $ = layui.jquery;
    var jfTable = layui.jfTable;

    var form = layui.form();

    form.render();

    layui.define(function(exports){
        var obj = {
            doEdit:function(classId) {
                $.get('/backstage/model-class/edit/'+ classId, {}, function(str){
                    var popLayerUtil = layui.popLayerUtil;
                    popLayerUtil.doPopUp({
                        index: layer.open({
                            id: '{{makeElUniqueName('editModelClass')}}',
                            title: '修改模型品类',
                            type: 1,
                            content: str,
                            area: ['800px', '570px']
                        }),
                        onClose: function() {
                            layui.modelClassFuncs.refreshTableGrid();
                        }
                    });
                });
            },
            doDelete:function(routeId) {
                layer.confirm('确定删除该路由？', {
                    btn: ['确定','放弃'],
                    icon: 3
                }, function(){
                    var index = layer.load(1);
                    $.ajax({
                        contentType: "application/json",
                        type: 'post',
                        url: '/backstage/api/model-class/delete',
                        data: JSON.stringify({
                            id: routeId
                        }),
                        success: function (outResult) {
                            layer.close(index);
                            if (outResult.Success) {
                                layer.msg(outResult.Message, { icon: 6 });
                                layui.modelClassFuncs.refreshTableGrid();
                            } else {
                                layer.msg(outResult.Message, { icon: 5 });
                            }
                        },
                        error: function (error) {
                            layer.close(index);
                            layui.validator.processValidateError(error);
                        }
                    });
                }, function(){
                });
            },
            refreshTableGrid: function() {
                $('input[name=\'{{makeElUniqueName('class_code')}}\']').val('');
                $('input[name=\'{{makeElUniqueName('class_name')}}\']').val('');
                $("#{{makeElUniqueName('tbClass')}}").jfTable("reload");
            }
        };
        exports('modelClassFuncs', obj);
    });

    $("#{{makeElUniqueName('tbClass')}}").jfTable({
        url: '/backstage/api/model-class/query',
        pageSize:5,
        page: true,
        skip: true,
        first:'首页',
        last:'尾页',
        columns: [{
            text:'操作',
            name: 'id',
            width: 200,
            align: 'center',
            formatter: function(value, dataItem, index) {
                var html = '<a class="layui-btn layui-btn-small layui-btn-normal" onclick="layui.modelClassFuncs.doEdit(' + value + ')"><i class="layui-icon">&#xe642;</i> 编辑</a>';
                html += '&nbsp;&nbsp;<a class="layui-btn layui-btn-small layui-btn-danger" onclick="layui.modelClassFuncs.doDelete(' + value + ')"><i class="layui-icon">&#xe640;</i> 删除</a>';
                return html;
            }
        },{
            text:'品类名称',
            name: 'name',
            width: 140,
            align: 'center',
        },{
            text:'品类代码',
            name: 'code',
            width: 140,
            align: 'center',
        },{
            text:'品类路径',
            name: 'getFullPath',
            width: 440,
            align: 'left',
        },{
            text:'排序号',
            name: 'sort_order',
            width: 60,
            align: 'center',
        }],
        method: 'get',
        queryParam: {
            code:$('input[name=\'{{makeElUniqueName('class_code')}}\']').val(),
            name:$('input[name=\'{{makeElUniqueName('class_name')}}\']').val(),
            categoryId:$('select[name=\'{{makeElUniqueName('category')}}\']').val()
        },
        toolbarClass: 'layui-btn-small',
        onBeforeLoad: function (param) {
            return $.extend(param, {
                code:$('input[name=\'{{makeElUniqueName('class_code')}}\']').val(),
                name:$('input[name=\'{{makeElUniqueName('class_name')}}\']').val(),
                categoryId:$('select[name=\'{{makeElUniqueName('category')}}\']').val()
            });
        },
        onLoadSuccess: function (data) {
            return data;
        },
        dataFilter:function (data) {
            return data;
        }
    });

    $('.layui-btn[lay-filter=\'{{makeElUniqueName('search_class')}}\']').on('click', function(){
        $("#{{makeElUniqueName('tbClass')}}").jfTable("reload");
    });

    $('.layui-btn[lay-filter=\'{{makeElUniqueName('add_class')}}\']').on('click', function(){
        $.get('/backstage/api/model-class/create', {}, function(str){
            var popLayerUtil = layui.popLayerUtil;
            popLayerUtil.doPopUp({
                index: layer.open({
                    id: '{{makeElUniqueName('createClass')}}',
                    title: '新建模型品类',
                    type: 1,
                    content: str,
                    area: ['800px', '570px']
                }),
                onClose: function() {
                    layui.modelClassFuncs.refreshTableGrid();
                }
            });
        });
    });
});
</script>
