<div class="layui-form">
    <div class="layui-layer-outerbox" style="margin-bottom:80px">
        <div class="layui-form-item">
            <label class="layui-form-label"><span style="color:red">*</span> 品类名称</label>
            <div class="layui-input-block">
                <input type="hidden" name="{{makeElUniqueName('class_id')}}" value="{{ isset($class)?$class->id:'' }}">
                <input type="text" name="{{makeElUniqueName('class_name')}}" value="{{ isset($class)?$class->name:'' }}" required lay-verify="required" placeholder="标识该品类的显式名称，必填（*）" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label"><span style="color:red">*</span> 品类代码</label>
            <div class="layui-input-block">
                <input type="text" name="{{makeElUniqueName('class_code')}}" value="{{ isset($class)?$class->code:'' }}" required lay-verify="required" placeholder="标识该品类的唯一代码，必填（*）" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label"><span style="color:red">*</span> 所属类目</label>
            <div class="layui-input-block">
                <select name="{{makeElUniqueName('class_category')}}" lay-search required lay-verify="required">
                    <option value="0">(类目)</option>
                    @foreach ($categories as $category)
                        @if (isset($class))
                            <option value='{{$category['id']}}' {{ $class->category_id==$category['id']?'selected':'' }}>{{ $category['getFullPath'] }}</option>
                        @else
                            <option value='{{$category['id']}}'>{{ $category['getFullPath'] }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label"><span style="color:red">*</span> 排序号</label>
            <div class="layui-input-block">
                <input type="text" name="{{makeElUniqueName('sort_order')}}" value="{{ isset($class)?$class->sort_order:'' }}" required lay-verify="required|number" placeholder="同级分类的排序号" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <fieldset class="layui-elem-field" style="margin-top: 20px;margin-bottom:20px">
                <legend class="layui-default-fieldsets-title" style="font-size:14px;font-weight:bold">品类属性列表</legend>

                <div style="padding-left:10px;padding-right:10px">
                    <button class="layui-btn layui-btn-small" style="margin-top:10px" lay-filter="{{makeElUniqueName('add_property')}}"><i class="layui-icon">&#xe654; </i> 新增</button>
                    <table class="layui-table" id="{{makeElUniqueName('tbProperties')}}">
                        <colgroup>
                        <col width="150">
                        <col width="400">
                        <col>
                        </colgroup>
                        <thead>
                            <tr>
                                <th>属性名称</th>
                                <th>属性值</th>
                                <th>操作</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (isset($class))
                                @foreach ($class->properties as $property)
                            <tr>
                                <td><input type="hidden" value="{{$property->id}}"><input type="text" class="layui-input" required placeholder="请输入属性名称" value="{{$property->name}}"></td>
                                <td><input type="text" class="layui-input" required placeholder="请输入属性值：形如([代码1]:[名称1],[代码2]:[名称2])" value="{{$property->value}}"></td>
                                <td><button class="layui-btn layui-btn-danger layui-btn-small" onclick="layui.modelClassOperFuncs.doDelete(this)"><i class="layui-icon">&#xe640; </i> 删除</button></td>
                            </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </fieldset>
        </div>
    </div>
    <div class="layui-dialog-bar">
        <button class="layui-btn" lay-submit lay-filter="{{makeElUniqueName('class_save')}}"><i class="layui-icon">&#xe605;</i> 保存</button>&nbsp;
        <button class="layui-btn layui-btn-primary" onclick="layer.close(layui.popLayerUtil.index);"><i class="layui-icon">&#x1006;</i>  关闭</button>
    </div>
</div>

<script>
layui.use(['form', 'validator'], function(){
    var form = layui.form();
    var $ = layui.jquery;
    var popLayerUtil = layui.popLayerUtil;
    //按layui的要求，如果要生成表单select/checkbox这些，必须先调用form.render()方法
    form.render();

    layui.define(function(exports){
        var obj = {
            doDelete:function(sender) {
                $(sender).parent().parent().remove();
            }
        };
        exports('modelClassOperFuncs', obj);
    });

    $('.layui-btn[lay-filter=\'{{makeElUniqueName('add_property')}}\']').on('click', function(){
        var html = '<tr><td><input type="hidden" value=""><input type="text" class="layui-input" required placeholder="请输入属性名称" value=""></td>';
        html += '<td><input type="text" class="layui-input" required placeholder="请输入属性值：形如([代码1]:[名称1],[代码2]:[名称2])" value=""></td>';
        html += '<td><button class="layui-btn layui-btn-danger layui-btn-small" onclick="layui.modelClassOperFuncs.doDelete(this)"><i class="layui-icon">&#xe640; </i> 删除</button></td></tr>';

        $("#{{makeElUniqueName('tbProperties')}}").find('tbody').append(html);
    });

    form.on('submit({{makeElUniqueName('class_save')}})', function(data){
        var index = layer.load(1);
        var url = '/backstage/api/model-class/savenew';
        var postParam = {
            name: data.field['{{makeElUniqueName('class_name')}}'],
            code: data.field['{{makeElUniqueName('class_code')}}'],
            category_id: data.field['{{makeElUniqueName('class_category')}}'],
            sort_order: data.field['{{makeElUniqueName('sort_order')}}'],
        };
        var classId = data.field['{{makeElUniqueName('class_id')}}'];
        if (classId != '') {
            url = '/backstage/api/model-class/update';
            postParam.id = classId;
        }
        var props = [];
        $("#{{makeElUniqueName('tbProperties')}}").find('tbody').find('tr').each(function(){
            var tRow = $(this);
            props.push({
                id: tRow.find('td:eq(0)').find('input:eq(0)').val(),
                name: tRow.find('td:eq(0)').find('input:eq(1)').val(),
                value: tRow.find('td:eq(1)').find('input:eq(0)').val()
            });

        });
        postParam.props = JSON.stringify(props);
        $.ajax({
            contentType: "application/json",
            type: 'post',
            url: url,
            data: JSON.stringify(postParam),
            success: function (outResult) {
                layer.close(index);
                if (outResult.Success) {
                    layer.msg(outResult.Message, { icon: 6 });
                    layer.close(popLayerUtil.index);
                    popLayerUtil.onClose();
                } else {
                    layer.msg(outResult.Message, { icon: 5 });
                }
            },
            error: function (error) {
                layer.close(index);
                layui.validator.processValidateError(error);
            }
        });
        return false;
    });
});
</script>
