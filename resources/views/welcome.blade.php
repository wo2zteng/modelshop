<!DOCTYPE html>
<html>
    @include('frontend.part.head', ['title' => '享服-服饰3D模型，应有尽有', 
        'descriptions' => '享服是最全球最大的3D服装模型共享平台，平台包含了数以万计的建模师作品，可以为您快速搭建成衣模型，3D效果图', 
        'keywords' => '享服,3D模型,服装,衣服,建模,素材,3dMax'])
<body>

    @include('frontend.part.header', ['topnavis' => [
        ['anchor' => '#index', 'section' => '首页'],
        ['anchor' => '#new', 'section' => '最新上架'],
        ['anchor' => '#competitive', 'section' => '精品模型'],
        ['anchor' => '#material', 'section' => '素材库']
    ]])

    <div class="am-panel am-panel-default" id="index" style="margin-top:0px">
        <div data-am-widget="slider" class="am-slider am-slider-b2" data-am-slider='{&quot;controlNav&quot;:false}' >
            <ul class="am-slides">
                @foreach ($banners as $banner)
                <li>
                    <img src="{{$banner['getFullPicturePath']}}">
                </li>
                @endforeach
            </ul>
        </div>
    </div>

    <div class="am-panel am-panel-default no-bolder" id="new" style="margin-top:0px">
       
        <div class="new-model">
            <div class="section-header">
                 <div class="title">
                    <i class="am-icon-cubes am-primary"> 最新上架</i> / <span class="eng-text">New</span>
                 </div>
                 <div class="more-text">
                    <a class="am-badge am-badge-secondary am-round">更多+</a>
                 </div>
            </div>
            <ul data-am-widget="gallery" class="am-gallery am-avg-sm-2 am-avg-md-3 am-avg-lg-4 am-gallery-bordered" data-am-gallery="{  }" >
                @foreach ($newModels as $model)
                <li>
                    <div class="am-thumbnail">
                        <div class="model-thumb-cont" onclick="window.open('{{URL('/model/'.$model->id.'.htm')}}')">
                            <img src="{{$model->getFullImageThumb()}}" class="model-thumb" alt=""/>
                            <div class="mask-layer hide">{{$model->description}}</div>
                            <a class="am-btn am-btn-warning am-round am-btn-sm download hide">下载模型</a>
                            <a class="am-btn am-btn-primary am-round am-btn-sm collect hide">收藏模型</a>
                        </div>
                        <div class="am-thumbnail-caption caption-content">
                            <a class="title" href="{{URL('/model/'.$model->id.'.htm')}}" target="_blank" title="{{$model->name}}">{{$model->shortTitle()}}</a>
                            <div class="detail">
                                <a href="##" class="author">
                                    <div class="thumb">
                                        <img src="{{$model->getOwnerIconPath()}}" alt="会员">
                                    </div>
                                    <div class="author-name">
                                        {{$model->owner->name}}
                                    </div>
                                </a>
                                <div class="info">
                                    <div class="visit">{{ empty($model->visit_count) ? 0:$model->visit_count }}</div>
                                    <div class="download">{{ empty($model->download_count)?0:$model->download_count }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
    </div>

    <div class="am-panel am-panel-default no-bolder" id="competitive" style="margin-top:0px">
        <div class="competive-model">
            <div class="section-header">
                 <div class="title">
                    <i class="am-icon-cubes am-primary"> 精品模型</i> / <span class="eng-text">Competitive</span>
                 </div>
                 <div class="more-text">
                    <a class="am-badge am-badge-secondary am-round">更多+</a>
                 </div>
            </div>
            <ul data-am-widget="gallery" class="am-gallery am-avg-sm-2 am-avg-md-3 am-avg-lg-4 am-gallery-bordered" data-am-gallery="{  }" >
                @foreach ($recommandModels as $model)
                <li>
                    <div class="am-thumbnail">
                        <div class="model-thumb-cont" onclick="window.open('{{URL('/model/'.$model->id.'.htm')}}')">
                            <img src="{{$model->getFullImageThumb()}}" class="model-thumb" alt=""/>
                            <div class="mask-layer hide">{{$model->description}}</div>
                            <a class="am-btn am-btn-warning am-round am-btn-sm download hide">下载模型</a>
                            <a class="am-btn am-btn-primary am-round am-btn-sm collect hide">收藏模型</a>
                        </div>
                        <div class="am-thumbnail-caption caption-content">
                            <a class="title" href="{{URL('/model/'.$model->id.'.htm')}}" target="_blank" title="{{$model->name}}">{{$model->shortTitle()}}</a>
                            <div class="detail">
                                <a href="##" class="author">
                                    <div class="thumb">
                                        <img src="{{$model->getOwnerIconPath()}}" alt="会员">
                                    </div>
                                    <div class="author-name">
                                        {{$model->owner->name}}
                                    </div>
                                </a>
                                <div class="info">
                                    <div class="visit">{{ empty($model->visit_count) ? 0:$model->visit_count }}</div>
                                    <div class="download">{{ empty($model->download_count)?0:$model->download_count }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
    </div>

    <div class="am-panel am-panel-default no-bolder" id="material">
        <div class="material-model">
            <div class="section-header">
                 <div class="title">
                    <i class="am-icon-cubes am-primary"> 素材库</i> / <span class="eng-text">Materials</span>
                 </div>
                 <div class="more-text">
                    <a class="am-badge am-badge-secondary am-round">更多+</a>
                 </div>
            </div>
            <ul data-am-widget="gallery" class="am-gallery am-avg-sm-2 am-avg-md-3 am-avg-lg-4 am-gallery-bordered" data-am-gallery="{  }" >
            @foreach ($materials as $model)
                <li>
                    <div class="am-thumbnail">
                        <div class="model-thumb-cont" onclick="window.open('{{URL('/model/'.$model->id.'.htm')}}')">
                            <img src="{{$model->getFullImageThumb()}}" class="model-thumb" alt=""/>
                            <div class="mask-layer hide">{{$model->description}}</div>
                            <a class="am-btn am-btn-warning am-round am-btn-sm download hide">下载模型</a>
                            <a class="am-btn am-btn-primary am-round am-btn-sm collect hide">收藏模型</a>
                        </div>
                        <div class="am-thumbnail-caption caption-content">
                            <a class="title" href="{{URL('/model/'.$model->id.'.htm')}}" target="_blank" title="{{$model->name}}">{{$model->shortTitle()}}</a>
                            <div class="detail">
                                <a href="##" class="author">
                                    <div class="thumb">
                                        <img src="{{$model->getOwnerIconPath()}}" alt="会员">
                                    </div>
                                    <div class="author-name">
                                        {{$model->owner->name}}
                                    </div>
                                </a>
                                <div class="info">
                                    <div class="visit">{{ empty($model->visit_count) ? 0:$model->visit_count }}</div>
                                    <div class="download">{{ empty($model->download_count)?0:$model->download_count }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
    </div>

    @include('frontend.part.footer')
    
</body>
@include('frontend.part.scripts')
<script>
    $('.model-thumb-cont').on('mouseover',function(){
        $(this).find('.mask-layer').removeClass('hide');
        $(this).find('.mask-layer').addClass('active');
        $(this).find('.download').removeClass('hide');
        $(this).find('.download').addClass('active');
        $(this).find('.collect').removeClass('hide');
        $(this).find('.collect').addClass('active');
    });

    $('.model-thumb-cont').on('mouseout',function(){
        $(this).find('.mask-layer').removeClass('active');
        $(this).find('.mask-layer').addClass('hide');
        $(this).find('.download').removeClass('active');
        $(this).find('.download').addClass('hide');
        $(this).find('.collect').removeClass('active');
        $(this).find('.collect').addClass('hide');
    });
</script>
</html>
