<div class="layui-default-tree-left">
    <ul id="{{makeElUniqueName('left-navi')}}"></ul>
</div>
<div class="layui-default-tree-navi">
    <div class="layui-field-box">
        <div class="layui-form-item" style="margin:0;margin-top:15px;">
            <div class="layui-inline">
                <label class="layui-form-label"></label>
                <div class="layui-input-inline layui-short-input">
                    <input type="hidden" name="{{makeElUniqueName('parent_group_id')}}">
                </div>
                <label class="layui-form-label" style="width:200px">部门名称</label>
                <div class="layui-input-inline layui-short-input">
                    <input type="text" placeholder="部门名称" name="{{makeElUniqueName('group_name')}}" autocomplete="off" class="layui-input">
                </div>
                <div class="layui-input-inline" style="width:auto">
                    <button class="layui-btn" lay-filter="{{makeElUniqueName('search_group')}}"><i class="layui-icon">&#xe615;</i> 搜索</button>&nbsp;
                    <button class="layui-btn layui-btn-normal" lay-filter="{{makeElUniqueName('add_group')}}"><i class="layui-icon">&#xe654; </i> 新增</button>
                </div>
            </div>
        </div>
    </div>
    <div id="{{makeElUniqueName('tbGroup')}}"></div>
</div>
<script>
layui.use(['jfTable', 'tree'], function(){
    var layer = layui.layer;
    var $ = layui.jquery;
    var jfTable = layui.jfTable;

    layui.define(function(exports){
        var obj = {
            doEdit:function(departmentId) {
                $.get('/backstage/group-department/edit/'+ departmentId, {}, function(str){
                    var popLayerUtil = layui.popLayerUtil;
                    popLayerUtil.doPopUp({
                        index: layer.open({
                            id: '{{makeElUniqueName('editDepartOrg')}}',
                            title: '修改内部部门组织',
                            type: 1,
                            content: str,
                            area: ['800px', '460px']
                        }),
                        onClose: function() {
                            layui.depGroupFuncs.refreshNaviTree();
                        }
                    });
                });
            },
            doDelete:function(departmentId) {
                layer.confirm('确定删除该部门组织？', {
                    btn: ['确定','放弃'],
                    icon: 3
                }, function(){
                    var index = layer.load(1);
                    $.ajax({
                        contentType: "application/json",
                        type: 'post',
                        url: '/backstage/api/group-department/delete',
                        data: JSON.stringify({
                            id: departmentId
                        }),
                        success: function (outResult) {
                            layer.close(index);
                            if (outResult.Success) {
                                layer.msg(outResult.Message, { icon: 6 });
                                layui.depGroupFuncs.refreshNaviTree();
                            } else {
                                layer.msg(outResult.Message, { icon: 5 });
                            }
                        },
                        error: function (error) {
                            layer.close(index);
                            layui.validator.processValidateError(error);
                        }
                    });
                }, function(){
                });
            },
            refreshTableGrid: function() {
                $('input[name=\'{{makeElUniqueName('group_name')}}\']').val('');
                $("#{{makeElUniqueName('tbGroup')}}").jfTable("reload");
            },
            initNaviTree: function() {
                this.doInitNaviTree(layui.depGroupFuncs.initTableGrid);
            },
            refreshNaviTree: function() {
                $('#{{makeElUniqueName('left-navi')}}').find('li').each(function() {
                    $(this).remove();
                });
                this.doInitNaviTree(layui.depGroupFuncs.refreshTableGrid);
            },
            doInitNaviTree: function(callback) {
                var index = layer.load(1);
                $.ajax({
                    contentType: "application/json",
                    type: 'get',
                    url: '/backstage/api/group-department/gettree',
                    success: function (outResult) {
                        layer.close(index);
                        if (outResult.length > 0) {
                            $('input[name=\'{{makeElUniqueName('parent_group_id')}}\']').val(outResult[0].id);
                        }
                        layui.tree({
                            elem: '#{{makeElUniqueName('left-navi')}}',
                            skin: 'shihuang',
                            nodes: outResult,
                            click: function(node) {
                                $('input[name=\'{{makeElUniqueName('parent_group_id')}}\']').val(node.id);
                                layui.depGroupFuncs.refreshTableGrid();
                            }
                        });
                        callback.call(this);
                    },
                    error: function (error) {
                        layer.close(index);
                        layui.validator.processValidateError(error);
                    }
                });
            },
            initTableGrid: function() {
                $("#{{makeElUniqueName('tbGroup')}}").jfTable({
                    url: '/backstage/api/group-department/query',
                    pageSize:5,
                    page: true,
                    skip: true,
                    first:'首页',
                    last:'尾页',
                    columns: [{
                        text:'操作',
                        name: 'id',
                        width: 160,
                        align: 'center',
                        formatter: function(value, dataItem, index) {
                            var html = '<a class="layui-btn layui-btn-small layui-btn-normal" onclick="layui.depGroupFuncs.doEdit(' + value + ')"><i class="layui-icon">&#xe642;</i> 编辑</a>';
                            html += '&nbsp;&nbsp;<a class="layui-btn layui-btn-small layui-btn-danger" onclick="layui.depGroupFuncs.doDelete(' + value + ')"><i class="layui-icon">&#xe640;</i> 删除</a>';
                            return html;
                        }
                    },{
                        text:'下级部门名称',
                        name: 'name',
                        width: 120,
                        align: 'center',
                    },{
                        text:'部门代码',
                        name: 'code',
                        width: 160,
                        align: 'center',
                    },{
                        text:'负责人',
                        name: 'manager.nick_name',
                        width: 80,
                        align: 'center',
                    },{
                        text:'类型',
                        name: 'group_type_text',
                        width: 80,
                        align: 'center',
                        formatter:function(value, dataItem, index) {
                            if (dataItem.group_type == {{\App\UserGroup::$GROUP_TYPE_INTERNAL_ORGANIZATION}}) {
                                return '<span style="color:blue">' + value + '</span>';
                            } else if (dataItem.group_type == {{\App\UserGroup::$GROUP_TYPE_INTERNAL_DEPARTMENT}}) {
                                return '<span style="color:green">' + value + '</span>';
                            }
                            return value;
                        }
                    },{
                        text:'状态',
                        name: 'active_status_text',
                        width: 50,
                        align: 'center',
                        formatter:function(value, dataItem, index) {
                            if (dataItem.active_status == {{\App\UserGroup::$GROUP_STATUS_INACTIVE}}) {
                                return '<span style="color:red">' + value + '</span>';
                            } else if (dataItem.active_status == {{\App\UserGroup::$GROUP_STATUS_ACTIVE}}) {
                                return '<span style="color:blue">' + value + '</span>';
                            } else if (dataItem.active_status == {{\App\UserGroup::$GROUP_STATUS_BANNED}}) {
                                return '<span style="color:gray">' + value + '</span>';
                            }
                            return value;
                        }
                    },{
                        text:'排序号',
                        name: 'sort_order',
                        width: 50,
                        align: 'center'
                    }],
                    method: 'get',
                    queryParam: {
                        name:$('input[name=\'{{makeElUniqueName('group_name')}}\']').val(),
                        parentId:$('input[name=\'{{makeElUniqueName('parent_group_id')}}\']').val(),
                    },
                    toolbarClass: 'layui-btn-small',
                    onBeforeLoad: function (param) {
                        return $.extend(param, {
                            name:$('input[name=\'{{makeElUniqueName('group_name')}}\']').val(),
                            parentId:$('input[name=\'{{makeElUniqueName('parent_group_id')}}\']').val(),
                        });
                    },
                    onLoadSuccess: function (data) {
                        return data;
                    },
                    dataFilter:function (data) {
                        return data;
                    }
                });
            }
        };
        exports('depGroupFuncs', obj);
    });

    layui.depGroupFuncs.initNaviTree();

    $('.layui-btn[lay-filter=\'{{makeElUniqueName('search_group')}}\']').on('click', function(){
        $("#{{makeElUniqueName('tbGroup')}}").jfTable("reload");
    });

    $('.layui-btn[lay-filter=\'{{makeElUniqueName('add_group')}}\']').on('click', function(){
        var parentId = $('input[name=\'{{makeElUniqueName('parent_group_id')}}\']').val();
        if (parentId != '' && parentId != undefined) {
            $.get('/backstage/group-department/create/' + parentId, {}, function(str){
                var popLayerUtil = layui.popLayerUtil;
                popLayerUtil.doPopUp({
                    index: layer.open({
                        id: '{{makeElUniqueName('createDepGroup')}}',
                        title: '新增下级组织',
                        type: 1,
                        content: str,
                        area: ['600px', '360px']
                    }),
                    onClose: function() {
                        layui.depGroupFuncs.refreshNaviTree();
                    }
                });
            });
        } else {
            layer.msg('还没有选择父级部门组织', { icon: 5 });
        }

    });
});
</script>
