<div class="layui-form layui-field-box">
    <div class="layui-form-item" style="margin:0;margin-top:15px;">
        <div class="layui-inline">
            <label class="layui-form-label">伙伴名称</label>
            <div class="layui-input-inline layui-short-input">
                <input type="text" placeholder="名称" name="{{makeElUniqueName('group_name')}}" autocomplete="off" class="layui-input">
            </div>
            <label class="layui-form-label">系统代码号</label>
            <div class="layui-input-inline layui-short-input">
                <input type="text" placeholder="系统生成的代码" name="{{makeElUniqueName('group_code')}}" autocomplete="off" class="layui-input">
            </div>
            <label class="layui-form-label">类型</label>
            <div class="layui-input-inline layui-short-input">
                <select name="{{makeElUniqueName('partner_group_type')}}">
                    <option value="0">(请选择伙伴类型)</option>
                    @foreach ($partnerGroupTypes as $partnerGroupType)
                        <option value='{{$partnerGroupType['id']}}'>{{$partnerGroupType['name']}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="layui-form-item" style="margin:0;">
        <div class="layui-inline">
            <label class="layui-form-label">所在省份</label>
            <div class="layui-input-inline layui-short-input">
                <select name="{{makeElUniqueName('group_province')}}" lay-filter="{{makeElUniqueName('group_province')}}">
                    <option value="0">(请选择省份)</option>
                </select>
            </div>
            <label class="layui-form-label">所在城市</label>
            <div class="layui-input-inline layui-short-input">
                <select name="{{makeElUniqueName('group_city')}}" lay-filter="{{makeElUniqueName('group_city')}}">
                    <option value="0">(请选择城市)</option>
                </select>
            </div>
            <label class="layui-form-label">状态</label>
            <div class="layui-input-inline layui-short-input">
                <select name="{{makeElUniqueName('status')}}">
                    <option value="0">(请选择状态)</option>
                    @foreach (\App\UserGroup::$GROUP_STATUS_MAP as $item)
                        <option value='{{$item['key']}}' {{ $item['key']==\App\UserGroup::$GROUP_STATUS_ACTIVE?'selected':''}}>{{$item['text']}}</option>
                    @endforeach
                </select>
            </div>
            <div class="layui-input-inline" style="width:auto">
                <button class="layui-btn" lay-filter="{{makeElUniqueName('search_group')}}"><i class="layui-icon">&#xe615;</i> 搜索</button>&nbsp;
                <button class="layui-btn layui-btn-normal" lay-filter="{{makeElUniqueName('add_group')}}"><i class="layui-icon">&#xe654; </i> 新增</button>
            </div>
        </div>
    </div>
</div>
<div id="{{makeElUniqueName('tbSimpleUserGroup')}}"></div>
<script>
layui.use(['jfTable', 'form', 'addressUtil'], function(){
    var layer = layui.layer;
    var $ = layui.jquery;
    var jfTable = layui.jfTable;
    var form = layui.form();
    var addressUtil = layui.addressUtil;

    form.render();

    addressUtil.twoLevelAddressSelect({
        provinceSelect: $('select[name=\'{{makeElUniqueName('group_province')}}\']'),
        citySelect: $('select[name=\'{{makeElUniqueName('group_city')}}\']'),
    });

    layui.define(function(exports){
        var obj = {
            doEdit:function(partnerId) {
                $.get('/backstage/group-simple/edit/'+ partnerId, {}, function(str){
                    var popLayerUtil = layui.popLayerUtil;
                    popLayerUtil.doPopUp({
                        index: layer.open({
                            id: '{{makeElUniqueName('editSimpleGroup')}}',
                            title: '修改个人伙伴',
                            type: 1,
                            content: str,
                            area: ['950px', '600px']
                        }),
                        onClose: function() {
                            layui.userGroupsFuncs.refreshTableGrid();
                        }
                    });
                });
            },
            doDelete:function(partnerId) {
                layer.confirm('确定删除该个人伙伴？', {
                    btn: ['确定','放弃'],
                    icon: 3
                }, function(){
                    var index = layer.load(1);
                    $.ajax({
                        contentType: "application/json",
                        type: 'post',
                        url: '/backstage/api/group-simple/delete',
                        data: JSON.stringify({
                            id: partnerId
                        }),
                        success: function (outResult) {
                            layer.close(index);
                            if (outResult.Success) {
                                layer.msg(outResult.Message, { icon: 6 });
                                layui.userGroupsFuncs.refreshTableGrid();
                            } else {
                                layer.msg(outResult.Message, { icon: 5 });
                            }
                        },
                        error: function (error) {
                            layer.close(index);
                            layui.validator.processValidateError(error);
                        }
                    });
                }, function(){
                });
            },
            refreshTableGrid: function() {
                $('input[name=\'{{makeElUniqueName('group_name')}}\']').val('');
                $('input[name=\'{{makeElUniqueName('group_code')}}\']').val('');
                $("#{{makeElUniqueName('tbSimpleUserGroup')}}").jfTable("reload");
            }
        };
        exports('userGroupsFuncs', obj);
    });

    $("#{{makeElUniqueName('tbSimpleUserGroup')}}").jfTable({
        url: '/backstage/api/group-simple/query',
        pageSize:5,
        page: true,
        skip: true,
        first:'首页',
        last:'尾页',
        columns: [{
            text:'操作',
            name: 'id',
            width: 200,
            align: 'center',
            formatter: function(value, dataItem, index) {
                var html = '<a class="layui-btn layui-btn-small layui-btn-normal" onclick="layui.userGroupsFuncs.doEdit(' + value + ')"><i class="layui-icon">&#xe642;</i> 编辑</a>';
                html += '&nbsp;&nbsp;<a class="layui-btn layui-btn-small layui-btn-danger" onclick="layui.userGroupsFuncs.doDelete(' + value + ')"><i class="layui-icon">&#xe640;</i> 删除</a>';
                return html;
            }
        },{
            text:'伙伴名称',
            name: 'name',
            width: 120,
            align: 'center',
        },{
            text:'系统代码',
            name: 'code',
            width: 120,
            align: 'center',
        },{
            text:'图标',
            name: 'getFullIconPath',
            width: 80,
            align: 'center',
            formatter:function(value, dataItem, index) {
                return '<img src=\'' + value + '\' width=40 height=40></img>';
            }
        },{
            text:'伙伴类型',
            name: 'partner_group_type.name',
            width: 160,
            align: 'center',
        },{
            text:'地址',
            name: 'getFullAddress',
            width: 360,
            align: 'center',
        },{
            text:'负责人',
            name: 'manager.nick_name',
            width: 80,
            align: 'center',
        },{
            text:'负责人电话',
            name: 'manager.mobile',
            width: 100,
            align: 'center',
        },{
            text:'状态',
            name: 'active_status_text',
            width: 50,
            align: 'center',
            formatter:function(value, dataItem, index) {
                if (dataItem.active_status == {{\App\UserGroup::$GROUP_STATUS_INACTIVE}}) {
                    return '<span style="color:red">' + value + '</span>';
                } else if (dataItem.active_status == {{\App\UserGroup::$GROUP_STATUS_ACTIVE}}) {
                    return '<span style="color:blue">' + value + '</span>';
                } else if (dataItem.active_status == {{\App\UserGroup::$GROUP_STATUS_BANNED}}) {
                    return '<span style="color:gray">' + value + '</span>';
                }
                return value;
            }
        }],
        method: 'get',
        queryParam: {
            name:$('input[name=\'{{makeElUniqueName('group_name')}}\']').val(),
            code:$('input[name=\'{{makeElUniqueName('group_code')}}\']').val(),
            partnerGroupTypeId:$('select[name=\'{{makeElUniqueName('partner_group_type')}}\']').val(),
            provinceId:$('select[name=\'{{makeElUniqueName('group_province')}}\']').val(),
            cityId:$('select[name=\'{{makeElUniqueName('group_city')}}\']').val(),
            activeStatus:$('select[name=\'{{makeElUniqueName('status')}}\']').val()
        },
        toolbarClass: 'layui-btn-small',
        onBeforeLoad: function (param) {
            return $.extend(param, {
                name:$('input[name=\'{{makeElUniqueName('group_name')}}\']').val(),
                code:$('input[name=\'{{makeElUniqueName('group_code')}}\']').val(),
                partnerGroupTypeId:$('select[name=\'{{makeElUniqueName('partner_group_type')}}\']').val(),
                provinceId:$('select[name=\'{{makeElUniqueName('group_province')}}\']').val(),
                cityId:$('select[name=\'{{makeElUniqueName('group_city')}}\']').val(),
                activeStatus:$('select[name=\'{{makeElUniqueName('status')}}\']').val()
            });
        },
        onLoadSuccess: function (data) {
            return data;
        },
        dataFilter:function (data) {
            return data;
        }
    });

    $('.layui-btn[lay-filter=\'{{makeElUniqueName('search_group')}}\']').on('click', function(){
        $("#{{makeElUniqueName('tbSimpleUserGroup')}}").jfTable("reload");
    });

    $('.layui-btn[lay-filter=\'{{makeElUniqueName('add_group')}}\']').on('click', function(){
        $.get('/backstage/group-simple/create', {}, function(str){
            var popLayerUtil = layui.popLayerUtil;
            popLayerUtil.doPopUp({
                index: layer.open({
                    id: '{{makeElUniqueName('createSimpleGroup')}}',
                    title: '新建个人伙伴',
                    type: 1,
                    content: str,
                    area: ['950px', '600px']
                }),
                onClose: function() {
                    layui.userGroupsFuncs.refreshTableGrid();
                }
            });
        });
    });
});
</script>
