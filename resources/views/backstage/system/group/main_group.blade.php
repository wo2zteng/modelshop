<div class="layui-form layui-layer-outerbox">
    @if (count($errors) > 0)
    <div class="layui-form-item">
        <blockquote class="layui-elem-quote" style="color:red">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </blockquote>
    </div>
    @endif
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 运营者名称</label>
        <div class="layui-input-block">
            <input type="hidden" name="{{makeElUniqueName('group_id')}}" value="{{ isset($currentUserGroup)?$currentUserGroup->id:'' }}">
            <input type="text" name="{{makeElUniqueName('group_name')}}" value="{{ isset($currentUserGroup)?$currentUserGroup->name:'' }}" required lay-verify="required" placeholder="标识该导航的显示名称，必填（*）" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">代码</label>
        <div class="layui-input-block">
            <input type="text" style="color:blue;border-width:0px" readonly name="{{makeElUniqueName('group_code')}}" value="{{ isset($currentUserGroup)?$currentUserGroup->code:'' }}" required lay-verify="required" autocomplete="off" class="layui-input">
        </div>
    </div>
    @if (isset($currentUserGroup))
        @if ($currentUserGroup->isExternalEnterprise())
    <div class="layui-form-item">
        <label class="layui-form-label">营业执照号</label>
        <div class="layui-input-block">
            <input type="text" style="color:blue;border-width:0px" readonly name="{{makeElUniqueName('group_business_code')}}" value="{{ isset($currentUserGroup)?$currentUserGroup->business_code:'' }}" placeholder="合作伙伴的营业执照号，对于企业则为必填（*）" autocomplete="off" class="layui-input">
        </div>
    </div>
        @endif
    @endif
    <div class="layui-form-item">
        <label class="layui-form-label">图标</label>
        <div class="layui-input-block">
            <div class="site-demo-upload">
                <img id="{{makeElUniqueName('icon_upload_img')}}" src="{{ isset($currentUserGroup)?$currentUserGroup->getFullIconPath():'/images/no-pic-back.png' }}">
                <input type="hidden" name="{{makeElUniqueName('group_icon_file_id')}}" value="{{ isset($currentUserGroup)?$currentUserGroup->icon:'' }}">
                <div class="site-demo-upbar">
                    <input type="file" name="{{makeElUniqueName('group_icon')}}" class="layui-upload-file" id="{{makeElUniqueName('group_icon')}}">
                </div>
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 管理员</label>
        <div class="layui-input-block">
            <select name="{{makeElUniqueName('group_manager')}}" lay-search required lay-verify="required">
                <option value="">(请选择管理员)</option>
                @if (isset($managedUsers))
                    @foreach ($managedUsers as $managedUser)
                        @if (isset($currentUserGroup))
                            <option value='{{$managedUser['id']}}' {{ $managedUser['id']==$currentUserGroup->manager_id?'selected':'' }}>{{ $managedUser['nick_name'] }}</option>
                        @else
                            <option value='{{$managedUser['id']}}'>{{ $managedUser['nick_name'] }}</option>
                        @endif
                    @endforeach
                @endif
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 地址</label>
        <div class="layui-input-inline layui-short-input">
            <select name="{{makeElUniqueName('group_province')}}" lay-filter="{{makeElUniqueName('group_province')}}" required lay-verify="{{makeElUniqueName('group_province')}}">
                <option value="">(请选择省份)</option>
            </select>
        </div>
        <div class="layui-input-inline layui-short-input">
            <select name="{{makeElUniqueName('group_city')}}" lay-filter="{{makeElUniqueName('group_city')}}" required lay-verify="{{makeElUniqueName('group_city')}}">
                <option value="">(请选择城市)</option>
            </select>
        </div>
        <div class="layui-input-inline layui-short-input">
            <select name="{{makeElUniqueName('group_district')}}" lay-filter="{{makeElUniqueName('group_district')}}" required lay-verify="{{makeElUniqueName('group_district')}}">
                <option value="">(请选择区县)</option>
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"></label>
        <div class="layui-input-block">
            <input type="text"  name="{{makeElUniqueName('detailed_address')}}" value="{{ isset($currentUserGroup)?$currentUserGroup->detailed_address:'' }}" required lay-verify="required" placeholder="详细地址，必填（*）" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item layui-form-center">
            <button class="layui-btn" lay-submit lay-filter="{{makeElUniqueName('group_save')}}">保存修改</button>
    </div>
</div>
<script>
layui.use(['addressUtil', 'uploadUtil', 'form', 'validator'], function(){
    var form = layui.form();
    var $ = layui.jquery;
    var popLayerUtil = layui.popLayerUtil;
    var addressUtil = layui.addressUtil;
    var uploadUtil = layui.uploadUtil;

    form.render();

    var defaultProvince = {{ isset($currentUserGroup)?(!empty($currentUserGroup->province_id)?$currentUserGroup->province_id:0):0 }};
    var defaultCity = {{ isset($currentUserGroup)?(!empty($currentUserGroup->city_id)?$currentUserGroup->city_id:0):0 }};
    var defaultDistrict = {{ isset($currentUserGroup)?(!empty($currentUserGroup->district_id)?$currentUserGroup->district_id:0):0 }};

    addressUtil.threeLevelAddressSelect({
        provinceSelect: $('select[name=\'{{makeElUniqueName('group_province')}}\']'),
        defaultProvince: defaultProvince,
        citySelect: $('select[name=\'{{makeElUniqueName('group_city')}}\']'),
        defaultCity: defaultCity,
        districtSelect: $('select[name=\'{{makeElUniqueName('group_district')}}\']'),
        defaultDistrict: defaultDistrict
    });

    uploadUtil.doUpload({
        success: function(fileId, filePath, fileKey) {
            if (fileKey == '{{makeElUniqueName('group_icon')}}') {
                $('#{{makeElUniqueName('icon_upload_img')}}').attr('src', filePath);
                $('input[name=\'{{makeElUniqueName('group_icon_file_id')}}\']').val(fileId);
            }
        }
    });

    form.verify({
        {{makeElUniqueName('group_province')}}: function(value, item){
            if (value == '0') {
                return '必须输入省份';
            }
        },
        {{makeElUniqueName('group_city')}}: function(value, item){
            if (value == '0') {
                return '必须输入城市';
            }
        },
        {{makeElUniqueName('group_district')}}: function(value, item){
            if (value == '0') {
                return '必须输入区县';
            }
        },
    });

    form.on('submit({{makeElUniqueName('group_save')}})', function(data){
        var index = layer.load(1);
        var url = '/backstage/api/current-org/save';
        var postParam = {
            name: data.field['{{makeElUniqueName('group_name')}}'],
            icon: data.field['{{makeElUniqueName('group_icon_file_id')}}'],
            manager_id: data.field['{{makeElUniqueName('group_manager')}}'],
            province_id: data.field['{{makeElUniqueName('group_province')}}'],
            city_id: data.field['{{makeElUniqueName('group_city')}}'],
            district_id: data.field['{{makeElUniqueName('group_district')}}'],
            detailed_address: data.field['{{makeElUniqueName('detailed_address')}}'],
        };

        $.ajax({
            contentType: "application/json",
            type: 'post',
            url: url,
            data: JSON.stringify(postParam),
            success: function (outResult) {
                layer.close(index);
                if (outResult.Success) {
                    layer.msg(outResult.Message, { icon: 6 });
                } else {
                    layer.msg(outResult.Message, { icon: 5 });
                }
            },
            error: function (error) {
                layer.close(index);
                layui.validator.processValidateError(error);
            }
        });
        return false;
    });
});
</script>
