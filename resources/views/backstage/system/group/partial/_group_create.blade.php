<div class="layui-form">
    <div class="layui-layer-outerbox" {!! isset($enableFloatToolbar)?($enableFloatToolbar?'style="margin-bottom:80px"':''):'' !!}>
        <div class="layui-form-item">
            <label class="layui-form-label"><span style="color:red">*</span> {{ $groupNameLabel }}名称</label>
            <div class="layui-input-block">
                <input type="hidden" name="{{makeElUniqueName('group_id')}}" value="{{ isset($currentUserGroup)?$currentUserGroup->id:'' }}">
                <input type="text" name="{{makeElUniqueName('group_name')}}" value="{{ isset($currentUserGroup)?$currentUserGroup->name:'' }}" required lay-verify="required" placeholder="{{$groupNameLabel}}的显示名称，必填（*）" autocomplete="off" class="layui-input">
            </div>
        </div>
        @if (isset($currentUserGroup))
        <div class="layui-form-item">
            <label class="layui-form-label">系统代码</label>
            <div class="layui-input-block">
                <input type="text" style="color:blue;border-width:0px" readonly name="{{makeElUniqueName('group_code')}}" value="{{ isset($currentUserGroup)?$currentUserGroup->code:'' }}" required lay-verify="required" autocomplete="off" class="layui-input">
            </div>
        </div>
        @endif
        @if (isTrue($enablePartnerType))
        <div class="layui-form-item">
            <label class="layui-form-label"><span style="color:red">*</span> 类型</label>
            <div class="layui-input-block">
                <select name="{{makeElUniqueName('partner_type')}}" lay-search required lay-verify="required">
                    <option value=''>(请选择类型)</option>
                    @foreach ($partnerGroupTypes as $partnerGroupType)
                        @if (isset($currentUserGroup))
                            <option value='{{$partnerGroupType['id']}}' {{ $partnerGroupType['id']==$currentUserGroup->partner_group_type_id?'selected':'' }}>{{ $partnerGroupType['name'] }}</option>
                        @else
                            <option value='{{$partnerGroupType['id']}}'>{{ $partnerGroupType['name'] }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>
        @endif
        @if (isTrue($enableParentOrg))
        <div class="layui-form-item">
            <label class="layui-form-label"><span style="color:red">*</span> 上级组织</label>
            <div class="layui-input-block">
                <select name="{{makeElUniqueName('parent_group')}}" lay-search required lay-verify="required">
                    @foreach ($possiableParents as $possiableParent)
                        @if (isset($currentUserGroup))
                            <option value='{{$possiableParent->id}}' {{ $currentUserGroup->parent_id==$possiableParent->id?'selected':'' }}>{{ $possiableParent->getGroupPath() }}</option>
                        @else
                            <option value='{{$possiableParent->id}}' {{ $possiableParent->id==$parent->id?'selected':'' }}>{{ $possiableParent->getGroupPath() }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>
        @endif
        @if (isTrue($enableInternalGroupType))
        <div class="layui-form-item">
            <label class="layui-form-label"><span style="color:red">*</span> 部门类型</label>
            <div class="layui-input-block">
                <select name="{{makeElUniqueName('group_type')}}" lay-search required lay-verify="required">
                    @foreach (\App\UserGroup::$GROUP_TYPE_MAP as $item)
                        @if ($item['key'] == \App\UserGroup::$GROUP_TYPE_INTERNAL_ORGANIZATION || $item['key'] == \App\UserGroup::$GROUP_TYPE_INTERNAL_DEPARTMENT)
                            @if (isset($currentUserGroup))
                                <option value='{{$item['key']}}' {{ $currentUserGroup->group_type==$item['key']?'selected':'' }}>{{$item['text']}}</option>
                            @else
                                <option value='{{$item['key']}}'>{{$item['text']}}</option>
                            @endif
                        @endif
                    @endforeach
                </select>
            </div>
        </div>
        @endif
        @if (isset($currentUserGroup))
            @if ($currentUserGroup->isExternalEnterprise())
    <div class="layui-form-item">
        <label class="layui-form-label">营业执照号</label>
        <div class="layui-input-block">
            <input type="text" name="{{makeElUniqueName('group_business_code')}}" value="{{ isset($currentUserGroup)?$currentUserGroup->business_code:'' }}" placeholder="合作伙伴的营业执照号，对于企业则为必填（*）" autocomplete="off" class="layui-input">
        </div>
    </div>
            @endif
        @else
            @if (isTrue($enableBusinessCode))
    <div class="layui-form-item">
        <label class="layui-form-label">营业执照号</label>
        <div class="layui-input-block">
            <input type="text" name="{{makeElUniqueName('group_business_code')}}" value="{{ isset($currentUserGroup)?$currentUserGroup->business_code:'' }}" placeholder="合作伙伴的营业执照号，对于企业则为必填（*）" autocomplete="off" class="layui-input">
        </div>
    </div>
            @endif
        @endif
        @if (isTrue($enableSortOrder))
        <div class="layui-form-item">
            <label class="layui-form-label"><span style="color:red">*</span> 排序号</label>
            <div class="layui-input-block">
                <input type="text" name="{{makeElUniqueName('sort_order')}}" value="{{ isset($currentUserGroup)?$currentUserGroup->sort_order:'' }}" required lay-verify="required|number" placeholder="同级排序号" autocomplete="off" class="layui-input">
            </div>
        </div>
        @endif
        @if (isTrue($enableIcon))
        <div class="layui-form-item">
            <label class="layui-form-label">图标</label>
            <div class="layui-input-block">
                <div class="site-demo-upload">
                    <img id="{{makeElUniqueName('icon_upload_img')}}" src="{{ isset($currentUserGroup)?$currentUserGroup->getFullIconPath():'/images/no-pic-back.png' }}">
                    <input type="hidden" name="{{makeElUniqueName('group_icon_file_id')}}" value="{{ isset($currentUserGroup)?$currentUserGroup->icon:'' }}">
                    <div class="site-demo-upbar">
                        <input type="file" name="{{makeElUniqueName('group_icon')}}" class="layui-upload-file" id="{{makeElUniqueName('group_icon')}}">
                    </div>
                </div>
            </div>
        </div>
        @endif
        @if (isset($currentUserGroup))
            @if (isset($enableDirectCreateManager))
                @if (!$enableDirectCreateManager)
            <div class="layui-form-item">
                <label class="layui-form-label">{!! isTrue($groupManagerMandory)?'<span style="color:red">*</span> ':'' !!}{{ $groupManagerLabel }}</label>
                <div class="layui-input-block">
                    <select name="{{makeElUniqueName('group_manager')}}" lay-search {!! isTrue($groupManagerMandory)?'required lay-verify="required"':'' !!}>
                        <option value="">(请选择{{ $groupManagerLabel }})</option>
                        @if (isset($managedUsers))
                            @foreach ($managedUsers as $managedUser)
                                @if (isset($currentUserGroup))
                                    <option value='{{$managedUser['id']}}' {{ $managedUser['id']==$currentUserGroup->manager_id?'selected':'' }}>{{ $managedUser['nick_name'] }}</option>
                                @else
                                    <option value='{{$managedUser['id']}}'>{{ $managedUser['nick_name'] }}</option>
                                @endif
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
                @endif
            @endif
        @endif
        @if (isset($enableDirectCreateManager))
            @if ($enableDirectCreateManager)
            <div class="layui-form-item">
                <label class="layui-form-label"><span style="color:red">*</span> 管理员手机</label>
                <div class="layui-input-block">
                    <input type="text" name="{{makeElUniqueName('manager_mobile')}}" value="{{ isset($currentUserGroup)?(isset($currentUserGroup->manager_id)?$currentUserGroup->manager->mobile:''):'' }}" placeholder="管理员的手机号，必填，唯一" required lay-verify="required" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">管理员账号</label>
                <div class="layui-input-block">
                    <input type="text" name="{{makeElUniqueName('manager_name')}}" value="{{ isset($currentUserGroup)?(isset($currentUserGroup->manager_id)?$currentUserGroup->manager->name:''):'' }}" placeholder="管理员的系统账号名，只能支持大小写之母以及数字下划线，缺省为用户手机号" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">管理员昵称</label>
                <div class="layui-input-block">
                    <input type="text" name="{{makeElUniqueName('manager_nick_name')}}" value="{{ isset($currentUserGroup)?(isset($currentUserGroup->manager_id)?$currentUserGroup->manager->nick_name:''):'' }}" placeholder="管理员的系统昵称，缺省为用户手机号" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label"><span style="color:red">*</span> 初始密码</label>
                <div class="layui-input-block">
                    <input type="password" name="{{makeElUniqueName('manager_password')}}" value="" placeholder="管理员的初始密码，最长20位，支持大小写字符数字以及特殊符号" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label"><span style="color:red">*</span> 确认密码</label>
                <div class="layui-input-block">
                    <input type="password" name="{{makeElUniqueName('manager_password_again')}}" value="" placeholder="再次确认密码" autocomplete="off" class="layui-input">
                </div>
            </div>
            @endif
        @endif
        @if (isTrue($enableAddress))
        <div class="layui-form-item">
            <label class="layui-form-label"><span style="color:red">*</span> 地址</label>
            <div class="layui-input-inline layui-short-input">
                <select name="{{makeElUniqueName('group_province')}}" lay-filter="{{makeElUniqueName('group_province')}}" required lay-verify="{{makeElUniqueName('group_province')}}">
                    <option value="">(请选择省份)</option>
                </select>
            </div>
            <div class="layui-input-inline layui-short-input">
                <select name="{{makeElUniqueName('group_city')}}" lay-filter="{{makeElUniqueName('group_city')}}" required lay-verify="{{makeElUniqueName('group_city')}}">
                    <option value="">(请选择城市)</option>
                </select>
            </div>
            <div class="layui-input-inline layui-short-input">
                <select name="{{makeElUniqueName('group_district')}}" lay-filter="{{makeElUniqueName('group_district')}}" required lay-verify="{{makeElUniqueName('group_district')}}">
                    <option value="">(请选择区县)</option>
                </select>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label"></label>
            <div class="layui-input-block">
                <input type="text"  name="{{makeElUniqueName('detailed_address')}}" value="{{ isset($currentUserGroup)?$currentUserGroup->detailed_address:'' }}" required lay-verify="required" placeholder="详细地址，必填（*）" autocomplete="off" class="layui-input">
            </div>
        </div>
        @endif
        @if (isset($enableFloatToolbar))
            @if (!isTrue($enableFloatToolbar))
        <div class="layui-form-item layui-form-center">
            <button class="layui-btn" lay-submit lay-filter="{{makeElUniqueName('group_save')}}">保存</button>
            <button class="layui-btn layui-btn-primary" onclick="layer.close(layui.popLayerUtil.index);">关闭</button>
        </div>
            @endif
        @else
        <div class="layui-form-item layui-form-center">
            <button class="layui-btn" lay-submit lay-filter="{{makeElUniqueName('group_save')}}">保存</button>
            <button class="layui-btn layui-btn-primary" onclick="layer.close(layui.popLayerUtil.index);">关闭</button>
        </div>
        @endif
    </div>
    @if (isset($enableFloatToolbar))
        @if (isTrue($enableFloatToolbar))
    <div class="layui-dialog-bar">
        <button class="layui-btn" lay-submit lay-filter="{{makeElUniqueName('group_save')}}"><i class="layui-icon">&#xe605;</i> 保存</button>&nbsp;
        <button class="layui-btn layui-btn-primary" onclick="layer.close(layui.popLayerUtil.index);"><i class="layui-icon">&#x1006;</i>  关闭</button>
    </div>
        @endif
    @endif
</div>
<script>
layui.use(['addressUtil', 'uploadUtil', 'form', 'validator'], function(){
    var form = layui.form();
    var $ = layui.jquery;
    var popLayerUtil = layui.popLayerUtil;
    var addressUtil = layui.addressUtil;
    var uploadUtil = layui.uploadUtil;

    form.render();

    @if (isTrue($enableAddress))
    var defaultProvince = {{ isset($currentUserGroup)?(!empty($currentUserGroup->province_id)?$currentUserGroup->province_id:0):0 }};
    var defaultCity = {{ isset($currentUserGroup)?(!empty($currentUserGroup->city_id)?$currentUserGroup->city_id:0):0 }};
    var defaultDistrict = {{ isset($currentUserGroup)?(!empty($currentUserGroup->district_id)?$currentUserGroup->district_id:0):0 }};

    addressUtil.threeLevelAddressSelect({
        provinceSelect: $('select[name=\'{{makeElUniqueName('group_province')}}\']'),
        defaultProvince: defaultProvince,
        citySelect: $('select[name=\'{{makeElUniqueName('group_city')}}\']'),
        defaultCity: defaultCity,
        districtSelect: $('select[name=\'{{makeElUniqueName('group_district')}}\']'),
        defaultDistrict: defaultDistrict
    });

    form.verify({
        {{makeElUniqueName('group_province')}}: function(value, item){
            if (value == '0') {
                return '必须输入省份';
            }
        },
        {{makeElUniqueName('group_city')}}: function(value, item){
            if (value == '0') {
                return '必须输入城市';
            }
        },
        {{makeElUniqueName('group_district')}}: function(value, item){
            if (value == '0') {
                return '必须输入区县';
            }
        },
    });
    @endif

    @if (isTrue($enableIcon))
    uploadUtil.doUpload({
        success: function(fileId, filePath, fileKey) {
            if (fileKey == '{{makeElUniqueName('group_icon')}}') {
                $('#{{makeElUniqueName('icon_upload_img')}}').attr('src', filePath);
                $('input[name=\'{{makeElUniqueName('group_icon_file_id')}}\']').val(fileId);
            }
        }
    });
    @endif

    form.on('submit({{makeElUniqueName('group_save')}})', function(data){
        var index = layer.load(1);
        var url = '{{$addNewUrl}}';
        var postParam = {
            name: data.field['{{makeElUniqueName('group_name')}}'],

        @if (isset($currentUserGroup))
            @if ($currentUserGroup->isExternalEnterprise())
        business_code: data.field['{{makeElUniqueName('group_business_code')}}'],
            @endif
        @else
            @if (isTrue($enableBusinessCode))
        business_code: data.field['{{makeElUniqueName('group_business_code')}}'],
            @endif
        @endif

        @if (isTrue($enablePartnerType))
            partner_group_type_id: data.field['{{makeElUniqueName('partner_type')}}'],
        @endif

        @if (isTrue($enableParentOrg))
            parent_id: data.field['{{makeElUniqueName('parent_group')}}'],
        @endif

        @if (isTrue($enableInternalGroupType))
            group_type: data.field['{{makeElUniqueName('group_type')}}'],
        @endif

        @if (isTrue($enableIcon))
            icon: data.field['{{makeElUniqueName('group_icon_file_id')}}'],
        @endif

        @if (isTrue($enableSortOrder))
            sort_order: data.field['{{makeElUniqueName('sort_order')}}'],
        @endif

        @if (isset($enableDirectCreateManager))
            @if ($enableDirectCreateManager)
                manager_name: data.field['{{makeElUniqueName('manager_name')}}'],
                manager_mobile: data.field['{{makeElUniqueName('manager_mobile')}}'],
                manager_nickname: data.field['{{makeElUniqueName('manager_nick_name')}}'],
                manager_password: data.field['{{makeElUniqueName('manager_password')}}'],
                manager_password_again: data.field['{{makeElUniqueName('manager_password_again')}}'],
            @endif
        @endif

        @if (isTrue($enableAddress))
            province_id: data.field['{{makeElUniqueName('group_province')}}'],
            city_id: data.field['{{makeElUniqueName('group_city')}}'],
            district_id: data.field['{{makeElUniqueName('group_district')}}'],
            detailed_address: data.field['{{makeElUniqueName('detailed_address')}}'],
        @endif
        };

        var groupId = data.field['{{makeElUniqueName('group_id')}}'];
        if (groupId != '') {
            url = '{{$updateSaveUrl}}';
            postParam.id = groupId;
        }

        @if (isset($currentUserGroup))
            @if (isset($enableDirectCreateManager))
                @if (!$enableDirectCreateManager)
        var managerId = data.field['{{makeElUniqueName('group_manager')}}'];
        if (managerId != '') {
            postParam.manager_id = managerId;
        }
                @endif
            @endif
        @endif

        $.ajax({
            contentType: "application/json",
            type: 'post',
            url: url,
            data: JSON.stringify(postParam),
            success: function (outResult) {
                layer.close(index);
                if (outResult.Success) {
                    layer.msg(outResult.Message, { icon: 6 });
                @if (isTrue($closeAfterSuccess))
                    layer.close(popLayerUtil.index);
                    popLayerUtil.onClose();
                @endif
                } else {
                    layer.msg(outResult.Message, { icon: 5 });
                }
            },
            error: function (error) {
                layer.close(index);
                layui.validator.processValidateError(error);
            }
        });
        return false;
    });
});
</script>
