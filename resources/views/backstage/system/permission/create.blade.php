<div class="layui-form layui-layer-outerbox">
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 权限名称</label>
        <div class="layui-input-block">
            <input type="hidden" name="{{makeElUniqueName('permission_id')}}" value="{{ isset($permission)?$permission->id:'' }}">
            <input type="text" name="{{makeElUniqueName('permission_name')}}" value="{{ isset($permission)?$permission->name:'' }}" required lay-verify="required" placeholder="标识该权限的显示名称，必填（*）" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 父级权限</label>
        <div class="layui-input-block">
            <select name="{{makeElUniqueName('parent_id')}}" lay-search required lay-verify="required">
                @foreach ($parentPermissions as $parentPermission)
                    @if (isset($permission))
                        <option value='{{$parentPermission['id']}}' {{ $permission->parent_id==$parentPermission['id']?'selected':'' }}>{{ (!empty($parentPermission['parent']['name'])?($parentPermission['parent']['name'].'>>'):'').$parentPermission['name'] }}</option>
                    @else
                        <option value='{{$parentPermission['id']}}' {{ $parentPermission['id']==$parent->id?'selected':'' }}>{{ (!empty($parentPermission['parent']['name'])?($parentPermission['parent']['name'].'>>'):'').$parentPermission['name'] }}</option>
                    @endif
                @endforeach
            </select>
        </div>
    </div>
    @if ($parent->permission_type == \App\MenuPermission::$PERMISSION_TYPE_TOP_NAVI ||
         $parent->permission_type == \App\MenuPermission::$PERMISSION_TYPE_MENU_GROUP)
    <div class="layui-form-item">
        <label class="layui-form-label">图标</label>
        <div class="layui-input-block">
            <input type="text" name="{{makeElUniqueName('permission_icon')}}" value="{!! isset($permission)?$permission->icon:'' !!}" placeholder="图标，采用阿里巴巴矢量图标库（iconfont）" autocomplete="off" class="layui-input">
        </div>
    </div>
    @endif
    @if ($parent->permission_type == \App\MenuPermission::$PERMISSION_TYPE_MENU_GROUP ||
         $parent->permission_type == \App\MenuPermission::$PERMISSION_TYPE_MENU)
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 关联路由</label>
        <div class="layui-input-block">
            <select name="{{makeElUniqueName('route_id')}}" lay-search required lay-verify="required">
                @foreach ($routes as $route)
                    @if (isset($permission))
                        <option value='{{$route['id']}}' {{ $permission->route_id==$route['id']?'selected':'' }}>{{$route['name']}}[{{$route['method_text']}}][{{$route['route']}}]</option>
                    @else
                        <option value='{{$route['id']}}'>{{$route['name']}}[{{$route['method_text']}}][{{$route['route']}}]</option>
                    @endif
                @endforeach
            </select>
        </div>
    </div>
    @endif
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 排序号</label>
        <div class="layui-input-block">
            <input type="text" name="{{makeElUniqueName('sort_order')}}" value="{{ isset($permission)?$permission->sort_order:'' }}" required lay-verify="required|number" placeholder="权限的同级排序号" autocomplete="off" class="layui-input">
        </div>
    </div>
    @if ($parent->permission_type == \App\MenuPermission::$PERMISSION_TYPE_MENU_GROUP)
    <div class="layui-form-item">
        <label class="layui-form-label">是否默认</label>
        <div class="layui-input-block">
            @foreach (\App\MenuPermission::$IS_DEFAULT_MAP as $item)
                @if (isset($permission))
                <input type='radio' name='{{makeElUniqueName('is_default')}}' value='{{$item['key']}}' title='{{$item['text']}}' {{ $permission->is_default==$item['key']?'checked':'' }}>
                @else
                    @if ($loop->first)
                    <input type='radio' name='{{makeElUniqueName('is_default')}}' value='{{$item['key']}}' title='{{$item['text']}}' checked>
                    @else
                    <input type='radio' name='{{makeElUniqueName('is_default')}}' value='{{$item['key']}}' title='{{$item['text']}}' >
                    @endif
                @endif
            @endforeach
        </div>
    </div>
    @endif
    <div class="layui-form-item layui-form-center">
        <button class="layui-btn" lay-submit lay-filter="{{makeElUniqueName('permission_save')}}">保存</button>
        <button class="layui-btn layui-btn-primary" onclick="layer.close(layui.popLayerUtil.index);">关闭</button>
    </div>
</div>

<script>
layui.use(['form', 'validator'], function(){
    var form = layui.form();
    var $ = layui.jquery;
    var popLayerUtil = layui.popLayerUtil;

    form.render();

    form.on('submit({{makeElUniqueName('permission_save')}})', function(data){
        var index = layer.load(1);
        var url = '/backstage/api/permission/savenew';
        var postParam = {
            name: data.field['{{makeElUniqueName('permission_name')}}'],
            parent_id: data.field['{{makeElUniqueName('parent_id')}}'],
            sort_order: data.field['{{makeElUniqueName('sort_order')}}'],
        };
        var routeId = data.field['{{makeElUniqueName('route_id')}}'];
        if (routeId != '0' && routeId != null && routeId != undefined) {
            postParam.route_id = routeId;
        }
        var isDefault = data.field['{{makeElUniqueName('is_default')}}'];
        if (isDefault != null && isDefault != undefined) {
            postParam.is_default = isDefault;
        }
        var icon = data.field['{{makeElUniqueName('permission_icon')}}'];
        if (icon != null && icon != undefined) {
            postParam.icon = icon;
        }
        var permissionId = data.field['{{makeElUniqueName('permission_id')}}'];
        if (permissionId != '') {
            url = '/backstage/api/permission/update';
            postParam.permission_type = {{ isset($permission)?$permission->permission_type:'0'}};
            postParam.id = permissionId;
        }
        $.ajax({
            contentType: "application/json",
            type: 'post',
            url: url,
            data: JSON.stringify(postParam),
            success: function (outResult) {
                layer.close(index);
                if (outResult.Success) {
                    layer.msg(outResult.Message, { icon: 6 });
                    layer.close(popLayerUtil.index);
                    popLayerUtil.onClose();
                } else {
                    layer.msg(outResult.Message, { icon: 5 });
                }
            },
            error: function (error) {
                layer.close(index);
                layui.validator.processValidateError(error);
            }
        });
        return false;
    });
});
</script>
