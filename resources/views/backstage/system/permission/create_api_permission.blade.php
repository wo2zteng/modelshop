<div class="layui-form layui-layer-outerbox">
    <div class="layui-form-item">
        <label class="layui-form-label" style="width:100px"><span style="color:red">*</span> API权限名称</label>
        <div class="layui-input-block layui-block-middle">
            <input type="hidden" name="{{makeElUniqueName('api_id')}}" value="{{ isset($permission)?$permission->id:'' }}">
            <input type="text" name="{{makeElUniqueName('api_name')}}" value="{{ isset($permission)?$permission->name:'' }}" required lay-verify="required" placeholder="标识该API权限的显示名称，必填（*）" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label" style="width:100px"><span style="color:red">*</span> 关联路由</label>
        <div class="layui-input-block layui-block-middle">
            <select name="{{makeElUniqueName('route_id')}}" lay-search required lay-verify="routeId">
                <option value='0'>(未选择路由)</option>
                @foreach ($routes as $route)
                    @if (isset($permission))
                        <option value='{{$route['id']}}' {{ $permission->route_id==$route['id']?'selected':'' }}>{{$route['name']}}[{{$route['method_text']}}][{{$route['route']}}]</option>
                    @else
                        <option value='{{$route['id']}}'>{{$route['name']}}[{{$route['method_text']}}][{{$route['route']}}]</option>
                    @endif
                @endforeach
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label" style="width:100px"><span style="color:red">*</span> 排序号</label>
        <div class="layui-input-block layui-block-middle">
            <input type="text" name="{{makeElUniqueName('sort_order')}}" value="{{ isset($permission)?$permission->sort_order:'' }}" required lay-verify="required|number" placeholder="API权限的排序号" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item layui-form-center">
        <button class="layui-btn" lay-submit lay-filter="{{makeElUniqueName('permission_save')}}">保存</button>
        <button class="layui-btn layui-btn-primary" onclick="layer.close(layui.popLayerUtil.index);">关闭</button>
    </div>
</div>

<script>
layui.use(['form', 'validator'], function(){
    var form = layui.form();
    var $ = layui.jquery;
    var popLayerUtil = layui.popLayerUtil;

    form.render();

    form.verify({
        routeId: function (value) {
            if (value == '0') {
                return "必须选择路由"
            }
        },
    });

    form.on('submit({{makeElUniqueName('permission_save')}})', function(data){
        var index = layer.load(1);
        var url = '/backstage/api/api-permission/savenew';
        var postParam = {
            name: data.field['{{makeElUniqueName('api_name')}}'],
            icon: data.field['{{makeElUniqueName('api_icon')}}'],
            sort_order: data.field['{{makeElUniqueName('sort_order')}}'],
        };
        var routeId = data.field['{{makeElUniqueName('route_id')}}'];
        if (routeId != '0' && routeId != null && routeId != undefined) {
            postParam.route_id = routeId;
        }
        var permissionId = data.field['{{makeElUniqueName('api_id')}}'];
        if (permissionId != '') {
            url = '/backstage/api/api-permission/update';
            postParam.id = permissionId;
        }
        $.ajax({
            contentType: "application/json",
            type: 'post',
            url: url,
            data: JSON.stringify(postParam),
            success: function (outResult) {
                layer.close(index);
                if (outResult.Success) {
                    layer.msg(outResult.Message, { icon: 6 });
                    layer.close(popLayerUtil.index);
                    popLayerUtil.onClose();
                } else {
                    layer.msg(outResult.Message, { icon: 5 });
                }
            },
            error: function (error) {
                layer.close(index);
                layui.validator.processValidateError(error);
            }
        });
        return false;
    });
});
</script>
