<div class="layui-default-tree-left">
    <ul id="{{makeElUniqueName('left-navi')}}"></ul>
</div>
<div class="layui-default-tree-navi">
    <div class="layui-field-box">
        <div class="layui-form-item" style="margin:0;margin-top:15px;">
            <div class="layui-inline">
                <label class="layui-form-label"></label>
                <div class="layui-input-inline layui-short-input">
                    <input type="hidden" name="{{makeElUniqueName('parent_perm_id')}}">
                    <input type="hidden" name="{{makeElUniqueName('permission_type')}}" value="{{\App\MenuPermission::$PERMISSION_TYPE_MENU_GROUP}}">
                </div>
                <label class="layui-form-label" style="width:200px">菜单权限名称</label>
                <div class="layui-input-inline layui-short-input">
                    <input type="text" placeholder="菜单权限名称" name="{{makeElUniqueName('permission_name')}}" autocomplete="off" class="layui-input">
                </div>
                <div class="layui-input-inline" style="width:auto">
                    <button class="layui-btn" lay-filter="{{makeElUniqueName('search_perm')}}"><i class="layui-icon">&#xe615;</i> 搜索</button>&nbsp;
                    <button class="layui-btn layui-btn-normal" lay-filter="{{makeElUniqueName('add_perm')}}"><i class="layui-icon">&#xe654; </i> 新增</button>
                </div>
            </div>
        </div>
    </div>
    <div id="{{makeElUniqueName('tbPermission')}}"></div>
</div>
<script>
layui.use(['jfTable', 'tree'], function(){
    var layer = layui.layer;
    var $ = layui.jquery;
    var jfTable = layui.jfTable;

    layui.define(function(exports){
        var obj = {
            doEdit:function(permissionId) {
                var height = 370;
                var title = '修改菜单组权限';
                var permissionType = $('input[name=\'{{makeElUniqueName('permission_type')}}\']').val();
                if (permissionType == '{{\App\MenuPermission::$PERMISSION_TYPE_MENU}}') {
                    height = 470;
                    title = '修改菜单权限';
                } else if (permissionType == '{{\App\MenuPermission::$PERMISSION_TYPE_OPERATION}}') {
                    height = 370;
                    title = '修改功能权限';
                }
                $.get('/backstage/api/permission/edit/'+ permissionId, {}, function(str){
                    var popLayerUtil = layui.popLayerUtil;
                    popLayerUtil.doPopUp({
                        index: layer.open({
                            id: '{{makeElUniqueName('editPermission')}}',
                            title: title,
                            type: 1,
                            content: str,
                            area: ['800px', height + 'px']
                        }),
                        onClose: function() {
                            layui.permissionFuncs.refreshTableGrid();
                        }
                    });
                });
            },
            doDelete:function(permissionId) {
                layer.confirm('确定删除该权限？', {
                    btn: ['确定','放弃'],
                    icon: 3
                }, function(){
                    var index = layer.load(1);
                    $.ajax({
                        contentType: "application/json",
                        type: 'post',
                        url: '/backstage/api/permission/delete',
                        data: JSON.stringify({
                            id: permissionId
                        }),
                        success: function (outResult) {
                            layer.close(index);
                            if (outResult.Success) {
                                layer.msg(outResult.Message, { icon: 6 });
                                layui.permissionFuncs.refreshNaviTree();
                            } else {
                                layer.msg(outResult.Message, { icon: 5 });
                            }
                        },
                        error: function (error) {
                            layer.close(index);
                            layui.validator.processValidateError(error);
                        }
                    });
                }, function(){
                });
            },
            refreshTableGrid: function() {
                $('input[name=\'{{makeElUniqueName('permission_name')}}\']').val('');
                $("#{{makeElUniqueName('tbPermission')}}").jfTable("reload");
            },
            initNaviTree: function() {
                this.doInitNaviTree(layui.permissionFuncs.initTableGrid);
            },
            refreshNaviTree: function() {
                $('#{{makeElUniqueName('left-navi')}}').find('li').each(function() {
                    $(this).remove();
                });
                this.doInitNaviTree(layui.permissionFuncs.refreshTableGrid);
            },
            doInitNaviTree: function(callback) {
                var index = layer.load(1);
                $.ajax({
                    contentType: "application/json",
                    type: 'get',
                    url: '/backstage/api/permission/gettree',
                    data: JSON.stringify({}),
                    success: function (outResult) {
                        layer.close(index);
                        layui.tree({
                            elem: '#{{makeElUniqueName('left-navi')}}',
                            skin: 'shihuang',
                            nodes: outResult,
                            click: function(node) {
                                $('input[name=\'{{makeElUniqueName('parent_perm_id')}}\']').val(node.id);
                                if (node.permission_type == {{\App\MenuPermission::$PERMISSION_TYPE_MENU_GROUP}}) {
                                    $('input[name=\'{{makeElUniqueName('permission_type')}}\']').val({{\App\MenuPermission::$PERMISSION_TYPE_MENU}});
                                } else if (node.permission_type == {{\App\MenuPermission::$PERMISSION_TYPE_TOP_NAVI}}) {
                                    $('input[name=\'{{makeElUniqueName('permission_type')}}\']').val({{\App\MenuPermission::$PERMISSION_TYPE_MENU_GROUP}});
                                } else if (node.permission_type == {{\App\MenuPermission::$PERMISSION_TYPE_MENU}}) {
                                    $('input[name=\'{{makeElUniqueName('permission_type')}}\']').val({{\App\MenuPermission::$PERMISSION_TYPE_OPERATION}});
                                }

                                layui.permissionFuncs.refreshTableGrid();
                            }
                        });
                        callback.call(this);
                    },
                    error: function (error) {
                        layer.close(index);
                        layui.validator.processValidateError(error);
                    }
                });
            },
            initTableGrid: function() {
                $("#{{makeElUniqueName('tbPermission')}}").jfTable({
                    url: '/backstage/api/permission/query',
                    pageSize:5,
                    page: true,
                    skip: true,
                    first:'首页',
                    last:'尾页',
                    columns: [{
                        text:'操作',
                        name: 'id',
                        width: 200,
                        align: 'center',
                        formatter: function(value, dataItem, index) {
                            var html = '<a class="layui-btn layui-btn-small layui-btn-normal" onclick="layui.permissionFuncs.doEdit(' + value + ')"><i class="layui-icon">&#xe642;</i> 编辑</a>';
                            html += '&nbsp;&nbsp;<a class="layui-btn layui-btn-small layui-btn-danger" onclick="layui.permissionFuncs.doDelete(' + value + ')"><i class="layui-icon">&#xe640;</i> 删除</a>';
                            return html;
                        }
                    },{
                        text:'菜单权限名称',
                        name: 'name',
                        width: 160,
                        align: 'center',
                    },{
                        text:'父权限名称',
                        name: 'parent.name',
                        width: 160,
                        align: 'center',
                    },{
                        text:'图标',
                        name: 'icon',
                        width: 80,
                        align: 'center',
                        formatter:function(value,dataItem,index){
                            var html = ""
                            if (value == null) {
                                html = "";
                            } else {
                                html = "<i class=\"layui-icon\" style=\"top: 3px;\">"+value+"</i>"
                            }
                            return html;
                        }
                    },{
                        text:'排序号',
                        name: 'sort_order',
                        width: 80,
                        align: 'center',
                        formatter:function(value,dataItem,index){
                            var html = ""
                            if (value == null) {
                                html = "";
                            } else {
                                html = "<i class=\"layui-icon\" style=\"top: 3px;\">"+value+"</i>"
                            }
                            return html;
                        }
                    },{
                        text:'是否默认',
                        name: 'is_default_text',
                        width: 80,
                        align: 'center'
                    },{
                        text:'关联路由',
                        name: 'route.name',
                        width: 180,
                        align: 'left',
                    },{
                        text:'关联路由路径',
                        name: 'route.route',
                        width: 240,
                        align: 'left',
                    }],
                    method: 'get',
                    queryParam: {
                        permissionName:$('input[name=\'{{makeElUniqueName('permission_name')}}\']').val(),
                        parentId:$('input[name=\'{{makeElUniqueName('parent_perm_id')}}\']').val(),
                        permissionType:$('input[name=\'{{makeElUniqueName('permission_type')}}\']').val(),
                    },
                    toolbarClass: 'layui-btn-small',
                    onBeforeLoad: function (param) {
                        return $.extend(param, {
                            permissionName:$('input[name=\'{{makeElUniqueName('permission_name')}}\']').val(),
                            parentId:$('input[name=\'{{makeElUniqueName('parent_perm_id')}}\']').val(),
                            permissionType:$('input[name=\'{{makeElUniqueName('permission_type')}}\']').val(),
                        });
                    },
                    onLoadSuccess: function (data) {
                        return data;
                    },
                    dataFilter:function (data) {
                        return data;
                    }
                });
            }
        };
        exports('permissionFuncs', obj);
    });

    layui.permissionFuncs.initNaviTree();

    $('.layui-btn[lay-filter=\'{{makeElUniqueName('search_perm')}}\']').on('click', function(){
        $("#{{makeElUniqueName('tbPermission')}}").jfTable("reload");
    });

    $('.layui-btn[lay-filter=\'{{makeElUniqueName('add_perm')}}\']').on('click', function(){
        var parentId = $('input[name=\'{{makeElUniqueName('parent_perm_id')}}\']').val();
        if (parentId != '' && parentId != undefined) {
            var height = 370;
            var title = '新增菜单组';
            var permissionType = $('input[name=\'{{makeElUniqueName('permission_type')}}\']').val();
            if (permissionType == '{{\App\MenuPermission::$PERMISSION_TYPE_MENU}}') {
                height = 470;
                title = '新增菜单';
            } else if (permissionType == '{{\App\MenuPermission::$PERMISSION_TYPE_OPERATION}}') {
                height = 370;
                title = '新增功能权限';
            }
            $.get('/backstage/permission/create/' + parentId, {}, function(str){
                var popLayerUtil = layui.popLayerUtil;
                popLayerUtil.doPopUp({
                    index: layer.open({
                        id: '{{makeElUniqueName('createPerm')}}',
                        title: title,
                        type: 1,
                        content: str,
                        area: ['800px', height + 'px']
                    }),
                    onClose: function() {
                        layui.permissionFuncs.refreshNaviTree();
                    }
                });
            });
        } else {
            layer.msg('还没有选择父级权限', { icon: 5 });
        }

    });
});
</script>
