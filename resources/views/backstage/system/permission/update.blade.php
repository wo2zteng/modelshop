<div class="layui-form layui-layer-outerbox">
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 菜单名称</label>
        <div class="layui-input-block">
            <input type="text" name="{{makeElUniqueName('permission_name')}}" required  lay-verify="required" placeholder="标识该菜单权限名称，必填（*）" autocomplete="off" class="layui-input" value="{{$name}}">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 图标</label>
        <div class="layui-input-block">
            <input type="text" name="{{makeElUniqueName('permission_icon')}}" required  lay-verify="required" placeholder="菜单权限的图标" autocomplete="off" class="layui-input"  value="{{$icon}}">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 菜单分级</label>
        <div class="layui-input-block">
            <select name="{{makeElUniqueName('permission_type')}}" lay-verify="required" lay-filter="{{makeElUniqueName('permission_type_filter')}}" id="abc">
                @foreach (\App\MenuPermission::$PERMISSION_TYPE_MAP as $item)
                    <option value='{{$item['key']}}'
                        @if($permission_type==$item['key']){
                            selected = selected
                        @endif

                    >{{$item['text']}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 父级菜单</label>
        <div class="layui-input-block">
            {{--<input type="text" name="{{makeElUniqueName('permission_pid')}}" required  lay-verify="required" placeholder="父级菜单，必填（*）" autocomplete="off" class="layui-input"  value="{{$parent_id}}">--}}
            <select name="{{makeElUniqueName('permission_pid')}}" lay-verify="required"  lay-filter="{{makeElUniqueName('permission_pid_filter')}}" >
                @foreach ($parent as $item)
                    <option value='{{$item['id']}}'
                            @if($parent_id==$item['id']){
                            selected = selected
                            @endif

                    >{{$item['name']}}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">是否默认</label>
        <div class="layui-input-block">
            @foreach (\App\MenuPermission::$IS_DEFAULT_MAP as $item)
                @if ($loop->first)
                <input type='radio' name='{{makeElUniqueName('permission_default')}}' value='{{$item['key']}}' title='{{$item['text']}}'
                   @if($is_default==$item['key']){
                   checked
                    @endif
                >
                @else
                <input type='radio' name='{{makeElUniqueName('permission_default')}}' value='{{$item['key']}}' title='{{$item['text']}}'
                   @if($is_default==$item['key']){
                   checked
                    @endif
                >
                @endif
            @endforeach
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">路由id</label>
        <div class="layui-input-block">
            <input type="text" name="{{makeElUniqueName('route_id')}}" placeholder="路由id" autocomplete="off" class="layui-input"  value="{{$route_id}}">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 排序号</label>
        <div class="layui-input-block">
            <input type="text" name="{{makeElUniqueName('sort_order')}}" required  lay-verify="required|number" placeholder="路由的排序号" autocomplete="off" class="layui-input"  value="{{$sort_order}}">
        </div>
    </div>
    <div class="layui-form-item layui-form-center">
            <button class="layui-btn" lay-submit lay-filter="{{makeElUniqueName('permission_save')}}">保存</button>
            <button class="layui-btn layui-btn-primary" onclick="layer.close(layer.index);">关闭</button>
    </div>
</div>

<script>



layui.use('form', function(){
    var form = layui.form();
    var $ = layui.jquery;
    //按layui的要求，如果要生成表单select/checkbox这些，必须先调用form.render()方法
    form.render();

    form.on('select({{makeElUniqueName('permission_type_filter')}})',function(data){


        $.ajax({
            contentType: "application/json",
            type: 'post',
            url: '/backstage/api/permission/parent',
            data: JSON.stringify({
                pid:data.value,
                state:1,
                id:'{{$id}}',
                name:$('input[name=\'{{makeElUniqueName('permission_name')}}\']').val(),
                icon: $('input[name=\'{{makeElUniqueName('permission_icon')}}\']').val(),
                permission_type: $('select[name=\'{{makeElUniqueName('permission_type')}}\']').val(),
                parent_id: $('select[name=\'{{makeElUniqueName('permission_pid')}}\']').val(),
                sort_order: $('input[name=\'{{makeElUniqueName('sort_order')}}\']').val(),
                is_default: $('input:radio[name=\'{{makeElUniqueName('permission_default')}}\']:checked').val(),
                route_id: $('input[name=\'{{makeElUniqueName('route_id')}}\']').val(),
            }),
            success: function (outResult) {

                layer.closeAll('page');
                layer.open({
                    id: '{{makeElUniqueName('createPermission')}}',
                    title: '新建菜单权限',
                    type: 1,
                    content: outResult,
                    area: ['800px', '570px']
                });
            },
            error: function (error) {
                layer.msg("请求异常", { icon: 2 });
            }
        });
    })

    //所有ajax请求的api，都必须放到/backstage/api下面，避免被CSRF拦截
    form.on('submit({{makeElUniqueName('permission_save')}})', function(data){
        var index = layer.load(1);
        $.ajax({
            contentType: "application/json",
            type: 'post',
            url: '/backstage/api/permission/updatesave',
            data: JSON.stringify({
                id:'{{$id}}',
                name: data.field['{{makeElUniqueName('permission_name')}}'],
                icon: data.field['{{makeElUniqueName('permission_icon')}}'],
                permission_type: data.field['{{makeElUniqueName('permission_type')}}'],
                parent_id: data.field['{{makeElUniqueName('permission_pid')}}'],
                sort_order: data.field['{{makeElUniqueName('sort_order')}}'],
                is_default: data.field['{{makeElUniqueName('permission_default')}}'],
                route_id: data.field['{{makeElUniqueName('route_id')}}'],
            }),
            success: function (outResult) {

                layer.close(index);
                if (outResult.Success) {

                    layer.msg(outResult.Message, { icon: 6 });
                    layer.closeAll('page');

                    $('#{{makeElUniqueName('table-permission')}}').jfTable("reload");

                } else {
                    layer.msg(outResult.Message, { icon: 5 });
                }
            },
            error: function (error) {
                layer.close(index);
                console.log(error);
                if (error.status == 422) {
                    //采用laravel的validate时，如果验证不通过，返回错误码是422
                    console.log(error);
                    layer.msg(error.Message, { icon: 5 });
                } else {

                }

                layer.msg("请求异常", { icon: 2 });
            }
        });
        return false;
    });
});
</script>
