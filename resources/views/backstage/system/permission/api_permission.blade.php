<div class="layui-field-box">
    <div class="layui-form-item" style="margin:0;margin-top:15px;">
        <div class="layui-inline">
            <label class="layui-form-label"></label>
            <div class="layui-input-inline layui-short-input">
            </div>
            <label class="layui-form-label" style="width:200px">API权限名称</label>
            <div class="layui-input-inline layui-short-input">
                <input type="text" placeholder="API权限名称" name="{{makeElUniqueName('api_name')}}" autocomplete="off" class="layui-input">
            </div>
            <div class="layui-input-inline" style="width:auto">
                <button class="layui-btn" lay-filter="{{makeElUniqueName('search_api')}}"><i class="layui-icon">&#xe615;</i> 搜索</button>&nbsp;
                <button class="layui-btn layui-btn-normal" lay-filter="{{makeElUniqueName('add_api')}}"><i class="layui-icon">&#xe654; </i> 新增</button>
            </div>
        </div>
    </div>
</div>
<div id="{{makeElUniqueName('tbApi')}}"></div>
<script>
layui.use('jfTable', function(){
    var layer = layui.layer;
    var $ = layui.jquery;
    var jfTable = layui.jfTable;

    layui.define(function(exports){
        var obj = {
            doEdit:function(permissionId) {
                $.get('/backstage/api-permission/edit/'+ permissionId, {}, function(str){
                    var popLayerUtil = layui.popLayerUtil;
                    popLayerUtil.doPopUp({
                        index: layer.open({
                            id: '{{makeElUniqueName('editApi')}}',
                            title: '修改API权限',
                            type: 1,
                            content: str,
                            area: ['700px', '300px']
                        }),
                        onClose: function() {
                            layui.apiFuncs.refreshTableGrid();
                        }
                    });
                });
            },
            doDelete:function(permissionId) {
                layer.confirm('确定删除该API权限？', {
                    btn: ['确定','放弃'],
                    icon: 3
                }, function(){
                    var index = layer.load(1);
                    $.ajax({
                        contentType: "application/json",
                        type: 'post',
                        url: '/backstage/api/api-permission/delete',
                        data: JSON.stringify({
                            id: permissionId
                        }),
                        success: function (outResult) {
                            layer.close(index);
                            if (outResult.Success) {
                                layer.msg(outResult.Message, { icon: 6 });
                                layui.apiFuncs.refreshTableGrid();
                            } else {
                                layer.msg(outResult.Message, { icon: 5 });
                            }
                        },
                        error: function (error) {
                            layer.close(index);
                            layui.validator.processValidateError(error);
                        }
                    });
                }, function(){
                });
            },
            refreshTableGrid: function() {
                $('input[name=\'{{makeElUniqueName('api_name')}}\']').val('');
                $("#{{makeElUniqueName('tbApi')}}").jfTable("reload");
            }
        };
        exports('apiFuncs', obj);
    });

    $("#{{makeElUniqueName('tbApi')}}").jfTable({
        url: '/backstage/api/api-permission/query',
        pageSize:5,
        page: true,
        skip: true,
        first:'首页',
        last:'尾页',
        columns: [{
            text:'操作',
            name: 'id',
            width: 200,
            align: 'center',
            formatter: function(value, dataItem, index) {
                var html = '<a class="layui-btn layui-btn-small layui-btn-normal" onclick="layui.apiFuncs.doEdit(' + value + ')"><i class="layui-icon">&#xe642;</i> 编辑</a>';
                html += '&nbsp;&nbsp;<a class="layui-btn layui-btn-small layui-btn-danger" onclick="layui.apiFuncs.doDelete(' + value + ')"><i class="layui-icon">&#xe640;</i> 删除</a>';
                return html;
            }
        },{
            text:'API权限名称',
            name: 'name',
            width: 160,
            align: 'center',
        },{
            text:'排序号',
            name: 'sort_order',
            width: 80,
            align: 'center',
            formatter:function(value,dataItem,index){
                var html = ""
                if (value == null) {
                    html = "";
                } else {
                    html = "<i class=\"layui-icon\" style=\"top: 3px;\">"+value+"</i>"
                }
                return html;
            }
        },{
            text:'关联路由',
            name: 'route.name',
            width: 180,
            align: 'left',
        },{
            text:'关联路由代码',
            name: 'route.code',
            width: 180,
            align: 'left',
        },{
            text:'关联路由路径',
            name: 'route.route',
            width: 240,
            align: 'left',
        }],
        method: 'get',
        queryParam: {
            permissionName:$('input[name=\'{{makeElUniqueName('api_name')}}\']').val()
        },
        toolbarClass: 'layui-btn-small',
        onBeforeLoad: function (param) {
            return $.extend(param, {
                permissionName:$('input[name=\'{{makeElUniqueName('api_name')}}\']').val()
            });
        },
        onLoadSuccess: function (data) {
            return data;
        },
        dataFilter:function (data) {
            return data;
        }
    });

    $('.layui-btn[lay-filter=\'{{makeElUniqueName('search_api')}}\']').on('click', function(){
        $("#{{makeElUniqueName('tbApi')}}").jfTable("reload");
    });

    $('.layui-btn[lay-filter=\'{{makeElUniqueName('add_api')}}\']').on('click', function(){
        $.get('/backstage/api-permission/create', {}, function(str){
            var popLayerUtil = layui.popLayerUtil;
            popLayerUtil.doPopUp({
                index: layer.open({
                    id: '{{makeElUniqueName('createApi')}}',
                    title: '新建API权限',
                    type: 1,
                    content: str,
                    area: ['700px', '300px']
                }),
                onClose: function() {
                    layui.apiFuncs.refreshTableGrid();
                }
            });
        });
    });
});
</script>
