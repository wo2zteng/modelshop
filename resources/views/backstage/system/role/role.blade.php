<div class="layui-field-box">
    <div class="layui-form-item" style="margin:0;margin-top:15px;">
        <div class="layui-inline">
            <label class="layui-form-label"></label>
            <div class="layui-input-inline layui-short-input">
            </div>
            <label class="layui-form-label">角色名称</label>
            <div class="layui-input-inline layui-short-input">
                <input type="text" placeholder="" name="{{makeElUniqueName('role_name')}}" autocomplete="off" class="layui-input">
            </div>
            <div class="layui-input-inline" style="width:auto">
                <button class="layui-btn" lay-filter="{{makeElUniqueName('search_role')}}"><i class="layui-icon">&#xe615;</i> 搜索</button>&nbsp;
                <button class="layui-btn layui-btn-normal" lay-filter="{{makeElUniqueName('add_role')}}"><i class="layui-icon">&#xe654; </i> 新增</button>
            </div>
        </div>
    </div>
</div>
<div id="{{makeElUniqueName('tbRole')}}"></div>
<script>
layui.use(['jfTable', 'validator'], function(){
    var layer = layui.layer;
    var $ = layui.jquery;
    var jfTable = layui.jfTable;

    layui.define(function(exports){
        var obj = {
            doEdit:function(roleId) {
                $.get('/backstage/role/edit/'+ roleId, {}, function(str){
                    var popLayerUtil = layui.popLayerUtil;
                    popLayerUtil.doPopUp({
                        index: layer.open({
                            id: '{{makeElUniqueName('editRole')}}',
                            title: '修改角色',
                            type: 1,
                            content: str,
                            area: ['500px', '330px']
                        }),
                        onClose: function() {
                            layui.roleFuncs.refreshTableGrid();
                        }
                    });
                });
            },
            doEditPermission:function(roleId, roleName) {
                $.get('/backstage/role/permission/'+ roleId, {}, function(str){
                    var popLayerUtil = layui.popLayerUtil;
                    popLayerUtil.doPopUp({
                        index: layer.open({
                            id: '{{makeElUniqueName('editRolePerms')}}',
                            title: '修改角色权限-' + roleName,
                            type: 1,
                            content: str,
                            area: ['950px', '600px']
                        }),
                        onClose: function() {
                            layui.roleFuncs.refreshTableGrid();
                        }
                    });
                });
            },
            doDelete:function(roleId) {
                layer.confirm('确定删除该角色？', {
                    btn: ['确定','放弃'],
                    icon: 3
                }, function(){
                    var index = layer.load(1);
                    $.ajax({
                        contentType: "application/json",
                        type: 'post',
                        url: '/backstage/api/role/delete',
                        data: JSON.stringify({
                            id: roleId
                        }),
                        success: function (outResult) {
                            layer.close(index);
                            if (outResult.Success) {
                                layer.msg(outResult.Message, { icon: 6 });
                                layui.roleFuncs.refreshTableGrid();
                            } else {
                                layer.msg(outResult.Message, { icon: 5 });
                            }
                        },
                        error: function (error) {
                            layer.close(index);
                            layui.validator.processValidateError(error);
                        }
                    });
                }, function(){
                });
            },
            refreshTableGrid: function() {
                $('input[name=\'{{makeElUniqueName('role_name')}}\']').val('');
                $("#{{makeElUniqueName('tbRole')}}").jfTable("reload");
            }
        };
        exports('roleFuncs', obj);
    });

    $("#{{makeElUniqueName('tbRole')}}").jfTable({
        url: '/backstage/api/role/query',
        pageSize:5,
        page: true,
        skip: true,
        first:'首页',
        last:'尾页',
        columns: [{
            text:'操作',
            name: 'id',
            width: 350,
            align: 'center',
            formatter: function(value, dataItem, index) {
                var html = '<a class="layui-btn layui-btn-small layui-btn-normal" onclick="layui.roleFuncs.doEdit(' + value + ')"><i class="layui-icon">&#xe642;</i> 编辑</a>';
                html += '&nbsp;&nbsp;<a class="layui-btn layui-btn-small layui-btn-danger" onclick="layui.roleFuncs.doDelete(' + value + ')"><i class="layui-icon">&#xe640;</i> 删除</a>';
                html += '&nbsp;&nbsp;<a class="layui-btn layui-btn-small" onclick="layui.roleFuncs.doEditPermission(' + value + ', \'' + dataItem.name + '\')"><i class="layui-icon">&#xe620;</i> 设置角色权限</a>';
                return html;
            }
        },{
            text:'角色名称',
            name: 'name',
            width: 160,
            align: 'center',
        },{
            text:'角色描述',
            name: 'description',
            width: 460,
            align: 'left',
        }],
        method: 'get',
        queryParam: {
            name:$('input[name=\'{{makeElUniqueName('role_name')}}\']').val()
        },
        toolbarClass: 'layui-btn-small',
        onBeforeLoad: function (param) {
            return $.extend(param, {
                name:$('input[name=\'{{makeElUniqueName('role_name')}}\']').val()
            });
        },
        onLoadSuccess: function (data) {
            return data;
        },
        dataFilter:function (data) {
            return data;
        }
    });

    $('.layui-btn[lay-filter=\'{{makeElUniqueName('search_role')}}\']').on('click', function(){
        $("#{{makeElUniqueName('tbRole')}}").jfTable("reload");
    });

    $('.layui-btn[lay-filter=\'{{makeElUniqueName('add_role')}}\']').on('click', function(){
        $.get('/backstage/role/create', {}, function(str){
            var popLayerUtil = layui.popLayerUtil;
            popLayerUtil.doPopUp({
                index: layer.open({
                    id: '{{makeElUniqueName('createRole')}}',
                    title: '新建角色',
                    type: 1,
                    content: str,
                    area: ['500px', '330px']
                }),
                onClose: function() {
                    layui.roleFuncs.refreshTableGrid();
                }
            });
        });
    });
});
</script>
