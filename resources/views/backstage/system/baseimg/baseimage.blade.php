<div class="layui-form layui-field-box">
    <div class="layui-form-item" style="margin:0;margin-top:15px;">
        <div class="layui-inline">
            <label class="layui-form-label">图片类型</label>
            <div class="layui-input-inline layui-short-input">
                <select name="{{makeElUniqueName('image_type')}}" lay-verify="required">
                    @foreach (\App\BaseImg::$BASE_IMAGE_TYPE_MAP as $item)
                        <option value='{{$item['key']}}'>{{$item['text']}}</option>
                    @endforeach
                </select>
            </div>
            <label class="layui-form-label">图片城市</label>
            <div class="layui-input-inline layui-short-input">
                <select name="{{makeElUniqueName('image_city')}}" lay-filter="{{makeElUniqueName('image_city')}}" lay-search>
                    <option value="0">(不限城市)</option>
                    @foreach ($allcities as $city)
                        <option value='{{$city->id}}'>{{ $city->name }}({{ $city->code }})</option>
                    @endforeach
                </select>
            </div>
            <div class="layui-input-inline" style="width:auto">
                <button class="layui-btn" lay-filter="{{makeElUniqueName('search_images')}}"><i class="layui-icon">&#xe615;</i> 搜索</button>&nbsp;
                <button class="layui-btn layui-btn-normal" lay-filter="{{makeElUniqueName('add_image')}}"><i class="layui-icon">&#xe654; </i> 新增</button>
            </div>
        </div>
    </div>
</div>
<div id="{{makeElUniqueName('tbBaseImage')}}"></div>
<script>
layui.use(['jfTable', 'form'], function(){
    var layer = layui.layer;
    var $ = layui.jquery;
    var jfTable = layui.jfTable;
    var form = layui.form();

    form.render();

    layui.define(function(exports){
        var obj = {
            doEdit:function(imageId) {
                $.get('/backstage/base-image/edit/'+ imageId, {}, function(str){
                    var popLayerUtil = layui.popLayerUtil;
                    popLayerUtil.doPopUp({
                        index: layer.open({
                            id: '{{makeElUniqueName('editImage')}}',
                            title: '修改运营图片',
                            type: 1,
                            content: str,
                            area: ['800px', '570px']
                        }),
                        onClose: function() {
                            layui.baseimageFuncs.refreshTableGrid();
                        }
                    });
                });
            },
            doDelete:function(imageId) {
                layer.confirm('确定删除该图片？', {
                    btn: ['确定','放弃'],
                    icon: 3
                }, function(){
                    var index = layer.load(1);
                    $.ajax({
                        contentType: "application/json",
                        type: 'post',
                        url: '/backstage/api/base-image/delete',
                        data: JSON.stringify({
                            id: imageId
                        }),
                        success: function (outResult) {
                            layer.close(index);
                            if (outResult.Success) {
                                layer.msg(outResult.Message, { icon: 6 });
                                layui.baseimageFuncs.refreshTableGrid();
                            } else {
                                layer.msg(outResult.Message, { icon: 5 });
                            }
                        },
                        error: function (error) {
                            layer.close(index);
                            layui.validator.processValidateError(error);
                        }
                    });
                }, function(){
                });
            },
            refreshTableGrid: function() {
                $("#{{makeElUniqueName('tbBaseImage')}}").jfTable("reload");
            }
        };
        exports('baseimageFuncs', obj);
    });

    $("#{{makeElUniqueName('tbBaseImage')}}").jfTable({
        url: '/backstage/api/base-image/query',
        pageSize:5,
        page: true,
        skip: true,
        first:'首页',
        last:'尾页',
        columns: [{
            text:'操作',
            name: 'id',
            width: 200,
            align: 'center',
            formatter: function(value, dataItem, index) {
                var html = '<a class="layui-btn layui-btn-small layui-btn-normal" onclick="layui.baseimageFuncs.doEdit(' + value + ')"><i class="layui-icon">&#xe642;</i> 编辑</a>';
                html += '&nbsp;&nbsp;<a class="layui-btn layui-btn-small layui-btn-danger" onclick="layui.baseimageFuncs.doDelete(' + value + ')"><i class="layui-icon">&#xe640;</i> 删除</a>';
                return html;
            }
        },{
            text:'图片类型',
            name: 'img_type_text',
            width: 160,
            align: 'center',
            formatter:function(value, dataItem, index) {
                if (dataItem.method == {{\App\BaseImg::$BASE_IMAGE_TYPE_MAINPAGE_BANNER}}) {
                    return '<span style="color:red">' + value + '</span>';
                }
                return value;
            }
        },{
            text:'图片',
            name: 'getFullPicturePath',
            width: 80,
            align: 'center',
            formatter:function(value, dataItem, index) {
                return '<img src=\'' + value + '\' width=40 height=40></img>';
            }
        },{
            text:'城市',
            name: 'city.name',
            width: 100,
            align: 'center',
        },{
            text:'链接',
            name: 'link_url',
            width: 350,
            align: 'left',
        },{
            text:'序号',
            name: 'sort_order',
            width: 100,
            align: 'center',
        }],
        method: 'get',
        queryParam: {
            imgType:$('select[name=\'{{makeElUniqueName('image_type')}}\']').val(),
            cityId:$('select[name=\'{{makeElUniqueName('image_city')}}\']').val()
        },
        toolbarClass: 'layui-btn-small',
        onBeforeLoad: function (param) {
            return $.extend(param, {
                imgType:$('select[name=\'{{makeElUniqueName('image_type')}}\']').val(),
                cityId:$('select[name=\'{{makeElUniqueName('image_city')}}\']').val()
            });
        },
        onLoadSuccess: function (data) {
            return data;
        },
        dataFilter:function (data) {
            return data;
        }
    });

    $('.layui-btn[lay-filter=\'{{makeElUniqueName('search_images')}}\']').on('click', function(){
        $("#{{makeElUniqueName('tbBaseImage')}}").jfTable("reload");
    });

    $('.layui-btn[lay-filter=\'{{makeElUniqueName('add_image')}}\']').on('click', function(){
        $.get('/backstage/base-image/create', {}, function(str){
            var popLayerUtil = layui.popLayerUtil;
            popLayerUtil.doPopUp({
                index: layer.open({
                    id: '{{makeElUniqueName('createImage')}}',
                    title: '新建基本图片',
                    type: 1,
                    content: str,
                    area: ['800px', '570px']
                }),
                onClose: function() {
                    layui.baseimageFuncs.refreshTableGrid();
                }
            });
        });
    });
});
</script>
