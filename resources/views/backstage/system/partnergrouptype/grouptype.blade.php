<div class="layui-field-box">
    <div class="layui-form-item" style="margin:0;margin-top:15px;">
        <div class="layui-inline">
            <label class="layui-form-label"></label>
            <div class="layui-input-inline layui-short-input">
            </div>
            <label class="layui-form-label" style="width:200px">原始用户组类型</label>
            <div class="layui-form layui-input-inline layui-short-input">
                <select name="{{makeElUniqueName('group_type')}}">
                    <option value='0'>(请选择)</option>
                    @foreach (\App\UserGroup::$GROUP_TYPE_MAP as $item)
                        @if ($item['key'] == \App\UserGroup::$GROUP_TYPE_EXTERNAL_ENTERPRISE || $item['key'] == \App\UserGroup::$GROUP_TYPE_USER)
                        <option value='{{$item['key']}}'>{{$item['text']}}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="layui-input-inline" style="width:auto">
                <button class="layui-btn" lay-filter="{{makeElUniqueName('search_group_type')}}"><i class="layui-icon">&#xe615;</i> 搜索</button>&nbsp;
                <button class="layui-btn layui-btn-normal" lay-filter="{{makeElUniqueName('add_group_type')}}"><i class="layui-icon">&#xe654; </i> 新增</button>
            </div>
        </div>
    </div>
</div>
<div id="{{makeElUniqueName('tbPartnerGroupType')}}"></div>
<script>
layui.use(['form', 'element', 'jfTable'], function(){
    var layer = layui.layer;
    var $ = layui.jquery;
    var jfTable = layui.jfTable;
    var form = layui.form();
    var element = layui.element();

    form.render();

    layui.define(function(exports){
        var obj = {
            doEdit:function(typeId) {
                $.get('/backstage/partner-group-type/edit/'+ typeId, {}, function(str){
                    var popLayerUtil = layui.popLayerUtil;
                    popLayerUtil.doPopUp({
                        index: layer.open({
                            id: '{{makeElUniqueName('editPartnerType')}}',
                            title: '修改合作伙伴类型',
                            type: 1,
                            content: str,
                            area: ['400px', '320px']
                        }),
                        onClose: function() {
                            layui.groupTypeFuncs.refreshTableGrid();
                        }
                    });
                });
            },
            doEditPermission:function(typeId, typeName) {
                $.get('/backstage/partner-group-type/permission/'+ typeId, {}, function(str){
                    var popLayerUtil = layui.popLayerUtil;
                    popLayerUtil.doPopUp({
                        index: layer.open({
                            id: '{{makeElUniqueName('editPartnerTypePerms')}}',
                            title: '修改合作伙伴默认权限-' + typeName,
                            type: 1,
                            content: str,
                            area: ['950px', '600px']
                        }),
                        onClose: function() {
                            layui.groupTypeFuncs.refreshTableGrid();
                        }
                    });
                });
            },
            doDelete:function(typeId) {
                layer.confirm('确定删除该合作伙伴类型？', {
                    btn: ['确定','放弃'],
                    icon: 3
                }, function(){
                    var index = layer.load(1);
                    $.ajax({
                        contentType: "application/json",
                        type: 'post',
                        url: '/backstage/api/partner-group-type/delete',
                        data: JSON.stringify({
                            id: typeId
                        }),
                        success: function (outResult) {
                            layer.close(index);
                            if (outResult.Success) {
                                layer.msg(outResult.Message, { icon: 6 });
                                layui.groupTypeFuncs.refreshTableGrid();
                            } else {
                                layer.msg(outResult.Message, { icon: 5 });
                            }
                        },
                        error: function (error) {
                            layer.close(index);
                            layui.validator.processValidateError(error);
                        }
                    });
                }, function(){
                });
            },
            refreshTableGrid: function() {
                $('select[name=\'{{makeElUniqueName('group_type')}}\']').val('0');
                $("#{{makeElUniqueName('tbPartnerGroupType')}}").jfTable("reload");
            }
        };
        exports('groupTypeFuncs', obj);
    });

    $("#{{makeElUniqueName('tbPartnerGroupType')}}").jfTable({
        url: '/backstage/api/partner-group-type/query',
        pageSize:5,
        page: true,
        skip: true,
        first:'首页',
        last:'尾页',
        columns: [{
            text:'操作',
            name: 'id',
            width: 320,
            align: 'center',
            formatter: function(value, dataItem, index) {
                var html = '<a class="layui-btn layui-btn-small layui-btn-normal" onclick="layui.groupTypeFuncs.doEdit(' + value + ')"><i class="layui-icon">&#xe642;</i> 编辑</a>';
                html += '&nbsp;&nbsp;<a class="layui-btn layui-btn-small layui-btn-danger" onclick="layui.groupTypeFuncs.doDelete(' + value + ')"><i class="layui-icon">&#xe640;</i> 删除</a>';
                html += '&nbsp;&nbsp;<a class="layui-btn layui-btn-small" onclick="layui.groupTypeFuncs.doEditPermission(' + value + ', \'' + dataItem.name + '\')"><i class="layui-icon">&#xe620;</i> 设置默认权限</a>';
                return html;
            }
        },{
            text:'类型名称',
            name: 'name',
            width: 180,
            align: 'center',
        },{
            text:'原始用户组类型',
            name: 'group_type_text',
            width: 180,
            align: 'center',
            formatter:function(value, dataItem, index) {
                if (dataItem.group_type == {{\App\UserGroup::$GROUP_TYPE_EXTERNAL_ENTERPRISE}}) {
                    return '<span style="color:red">' + value + '</span>';
                } else if (dataItem.group_type == {{\App\UserGroup::$GROUP_TYPE_INTERNAL_ORGANIZATION}}) {
                    return '<span style="color:green">' + value + '</span>';
                } else if (dataItem.group_type == {{\App\UserGroup::$GROUP_TYPE_INTERNAL_DEPARTMENT}}) {
                    return '<span style="color:gray">' + value + '</span>';
                } else if (dataItem.group_type == {{\App\UserGroup::$GROUP_TYPE_USER}}) {
                    return '<span style="color:blue">' + value + '</span>';
                }
                return value;
            }
        },{
            text:'排序',
            name: 'sort_order',
            width: 180,
            align: 'center',
        }],
        method: 'get',
        queryParam: {
            groupType:$('select[name=\'{{makeElUniqueName('group_type')}}\']').val()
        },
        toolbarClass: 'layui-btn-small',
        onBeforeLoad: function (param) {
            return $.extend(param, {
                groupType:$('select[name=\'{{makeElUniqueName('group_type')}}\']').val()
            });
        },
        onLoadSuccess: function (data) {
            return data;
        },
        dataFilter:function (data) {
            return data;
        }
    });

    $('.layui-btn[lay-filter=\'{{makeElUniqueName('search_group_type')}}\']').on('click', function(){
        $("#{{makeElUniqueName('tbPartnerGroupType')}}").jfTable("reload");
    });

    $('.layui-btn[lay-filter=\'{{makeElUniqueName('add_group_type')}}\']').on('click', function(){
        $.get('/backstage/partner-group-type/create', {}, function(str){
            var popLayerUtil = layui.popLayerUtil;
            popLayerUtil.doPopUp({
                index: layer.open({
                    id: '{{makeElUniqueName('createGroupType')}}',
                    title: '新建合作伙伴类型',
                    type: 1,
                    content: str,
                    area: ['400px', '320px']
                }),
                onClose: function() {
                    layui.groupTypeFuncs.refreshTableGrid();
                }
            });
        });
    });
});
</script>
