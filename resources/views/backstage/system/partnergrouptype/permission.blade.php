<div class="layui-form">
    <div class="layui-layer-outerbox">
        <table class="layui-table" lay-skin="line" style="margin-bottom: 80px">
            <colgroup>
                <col width="250">
                <col width="550">
            </colgroup>
            <thead>
                <tr>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($menuPermissions as $topNaviPermission)
                <tr>
                    <td style="text-align:left">
                        <input type="checkbox" lay-filter="{{makeElUniqueName('perm')}}" name="{{makeElUniqueName('perm')}}" value="{{$topNaviPermission['id']}}" parent-value="" title="{{$topNaviPermission['name']}}" lay-skin="primary" {{ in_array($topNaviPermission['id'], $currentPermssionIds)?'checked':'' }}>
                    </td>
                    <td></td>
                </tr>
                    @foreach ($topNaviPermission['children'] as $menuGroupPermission)
                    <tr>
                        <td style="text-align:left">
                            <div style="padding-left:30px"><input type="checkbox" lay-filter="{{makeElUniqueName('perm')}}" name="{{makeElUniqueName('perm')}}" value="{{$menuGroupPermission['id']}}" parent-value="{{$topNaviPermission['id']}}" title="{{$menuGroupPermission['name']}}" lay-skin="primary" {{ in_array($menuGroupPermission['id'], $currentPermssionIds)?'checked':'' }}></div>
                        </td>
                        <td></td>
                    </tr>
                        @foreach ($menuGroupPermission['children'] as $menuPermission)
                        <tr>
                            <td style="text-align:left">
                                <div style="padding-left:60px"><input type="checkbox" lay-filter="{{makeElUniqueName('perm')}}" name="{{makeElUniqueName('perm')}}" value="{{$menuPermission['id']}}" parent-value="{{$menuGroupPermission['id']}}" title="{{$menuPermission['name']}}{{$menuPermission['is_default']==\App\BaseDictionary::$KEY_YES?'(默认菜单)':''}}" lay-skin="primary" {{ in_array($menuPermission['id'], $currentPermssionIds)?'checked':'' }}></div>
                            </td>
                            <td>
                                <ul>
                                @foreach ($menuPermission['children'] as $operationPermission)
                                    <li style="float:left;list-style:none">
                                        <div style="width:250px; text-align:left"><input type="checkbox" lay-filter="{{makeElUniqueName('perm')}}" name="{{makeElUniqueName('perm')}}" value="{{$operationPermission['id']}}" parent-value="{{$menuPermission['id']}}" title="{{$operationPermission['name']}}" lay-skin="primary" {{ in_array($operationPermission['id'], $currentPermssionIds)?'checked':'' }}></div>
                                    </li>
                                @endforeach
                                </ul>
                            </td>
                        </tr>
                        @endforeach
                    @endforeach
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="layui-dialog-bar">
        <button class="layui-btn" lay-submit lay-filter="{{makeElUniqueName('btn_type_permission_save')}}"><i class="layui-icon">&#xe605;</i> 保存</button>&nbsp;
        <button class="layui-btn layui-btn-primary" onclick="layer.close(layui.popLayerUtil.index);"><i class="layui-icon">&#x1006;</i>  关闭</button>
    </div>
</div>
<script>
layui.use(['form', 'validator'], function(){
    var form = layui.form();
    var $ = layui.jquery;
    var popLayerUtil = layui.popLayerUtil;
    //按layui的要求，如果要生成表单select/checkbox这些，必须先调用form.render()方法
    form.render();

    form.on('checkbox({{makeElUniqueName('perm')}})', function(data){
        layui.groupTypePermFuncs.setParentElement($(data.elem), data.elem.checked);
        layui.groupTypePermFuncs.setChildElement($(data.elem), data.elem.checked, data.value);
    });

    form.on('submit({{makeElUniqueName('btn_type_permission_save')}})', function(data){
        var index = layer.load(1);
        var permissions = '';
        $('input[name=\'{{makeElUniqueName('perm')}}\']').each(function(){
            if ($(this).prop('checked')) {
                permissions += $(this).val() + ',';
            }
        });
        if (permissions != '') {
            permissions = permissions.substring(0, permissions.length-1);
        }

        $.ajax({
            contentType: "application/json",
            type: 'post',
            url: '/backstage/api/partner-group-type/permission/save',
            data: JSON.stringify({
                typeId: '{{$groupTypeId}}',
                perms: permissions
            }),
            success: function (outResult) {
                layer.close(index);
                if (outResult.Success) {
                    layer.msg(outResult.Message, { icon: 6 });
                    layer.close(popLayerUtil.index);
                    popLayerUtil.onClose();
                } else {
                    layer.msg(outResult.Message, { icon: 5 });
                }
            },
            error: function (error) {
                layer.close(index);
                layui.validator.processValidateError(error);
            }
        });
        return false;
    });

    layui.define(function(exports){
        var obj = {
            setParentElement: function(currentElement, checked) {
                if (checked) {
                    var parentValue = currentElement.attr('parent-value');
                    if (parentValue != null && parentValue != undefined && parentValue != '') {
                        var parentElement = $('input[value=' + parentValue + ']');
                        parentElement.prop('checked', true);
                        form.render();
                        layui.groupTypePermFuncs.setParentElement(parentElement, true);
                    }
                } else {
                    layui.groupTypePermFuncs.setChildElement(currentElement, false, currentElement.attr('value'));
                }
            },
            setChildElement: function(currentElement, checked, ovalue) {
                if (checked) {
                    if (ovalue != null && ovalue != undefined && ovalue != '') {
                        $('input[parent-value=' + ovalue + ']').each(function(){
                            var current = $(this);
                            current.prop('checked', true);
                            form.render();
                            layui.groupTypePermFuncs.setChildElement(current, true, current.attr('value'));
                        });
                    }
                } else {
                    if (ovalue != null && ovalue != undefined && ovalue != '') {
                        $('input[parent-value=' + ovalue + ']').each(function(){
                            var current = $(this);
                            current.prop('checked', false);
                            form.render();
                            layui.groupTypePermFuncs.setChildElement(current, false, current.attr('value'));
                        });
                    }
                }
            }
        }
        exports('groupTypePermFuncs', obj);
    });
});
</script>
