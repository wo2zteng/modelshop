<div class="layui-form layui-layer-outerbox">
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 类型名称</label>
        <div class="layui-input-block">
            <input type="hidden" name="{{makeElUniqueName('type_id')}}" value="{{ isset($partnerGroupType)?$partnerGroupType->id:'' }}">
            <input type="text" name="{{makeElUniqueName('type_name')}}" value="{{ isset($partnerGroupType)?$partnerGroupType->name:'' }}" required lay-verify="required" placeholder="合作伙伴类型名称，必填（*）" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 原始组类别</label>
        <div class="layui-input-block">
            <select name="{{makeElUniqueName('group_type')}}" lay-verify="required">
                @foreach (\App\UserGroup::$GROUP_TYPE_MAP as $item)
                    @if ($item['key'] == \App\UserGroup::$GROUP_TYPE_EXTERNAL_ENTERPRISE || $item['key'] == \App\UserGroup::$GROUP_TYPE_USER)
                        @if (isset($partnerGroupType))
                            <option value='{{$item['key']}}' {{ $partnerGroupType->group_type==$item['key']?'selected':'' }}>{{$item['text']}}</option>
                        @else
                            <option value='{{$item['key']}}'>{{$item['text']}}</option>
                        @endif
                    @endif
                @endforeach
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 排序号</label>
        <div class="layui-input-block">
            <input type="text" name="{{makeElUniqueName('sort_order')}}" value="{{ isset($partnerGroupType)?$partnerGroupType->sort_order:'' }}" required lay-verify="required|number" placeholder="合作伙伴类型的显示排序号" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item layui-form-center">
            <button class="layui-btn" lay-submit lay-filter="{{makeElUniqueName('group_type_save')}}">保存</button>
            <button class="layui-btn layui-btn-primary" onclick="layer.close(layui.popLayerUtil.index);">关闭</button>
    </div>
</div>

<script>
layui.use(['form', 'validator'], function(){
    var form = layui.form();
    var $ = layui.jquery;
    var popLayerUtil = layui.popLayerUtil;
    //按layui的要求，如果要生成表单select/checkbox这些，必须先调用form.render()方法
    form.render();

    //所有ajax请求的api，都必须放到/backstage/api下面，避免被CSRF拦截
    form.on('submit({{makeElUniqueName('group_type_save')}})', function(data){
        var index = layer.load(1);
        var url = '/backstage/api/partner-group-type/savenew';
        var postParam = {
            name: data.field['{{makeElUniqueName('type_name')}}'],
            group_type: data.field['{{makeElUniqueName('group_type')}}'],
            sort_order: data.field['{{makeElUniqueName('sort_order')}}'],
        };
        var typeId = data.field['{{makeElUniqueName('type_id')}}'];
        if (typeId != '') {
            //修改
            url = '/backstage/api/partner-group-type/update';
            postParam.id = typeId;
        }
        $.ajax({
            contentType: "application/json",
            type: 'post',
            url: url,
            data: JSON.stringify(postParam),
            success: function (outResult) {
                layer.close(index);
                if (outResult.Success) {
                    layer.msg(outResult.Message, { icon: 6 });
                    layer.close(popLayerUtil.index);
                    //利用自定义的扩展popLayerUtil，来实现关闭当前layer的效果
                    //popLayerUtil定义了一个回调函数onClose，可以在创建layer的时候设置该回调函数
                    //一旦前面执行完毕，则开始执行此回调，回调可以做一些动作，比如新增记录后重新刷新网格
                    popLayerUtil.onClose();
                } else {
                    layer.msg(outResult.Message, { icon: 5 });
                }
            },
            error: function (error) {
                layer.close(index);
                layui.validator.processValidateError(error);
            }
        });
        return false;
    });
});
</script>
