<div class="layui-field-box">
    <div class="layui-form-item" style="margin:0;margin-top:15px;">
        <div class="layui-inline">
            <label class="layui-form-label">路由路径</label>
            <div class="layui-input-inline layui-short-input">
                <input type="text" placeholder="形如:/home" name="{{makeElUniqueName('route_path')}}" autocomplete="off" class="layui-input">
            </div>
            <label class="layui-form-label">路由Action</label>
            <div class="layui-input-inline layui-short-input">
                <input type="text" placeholder="形如:Controller/index" name="{{makeElUniqueName('route_action')}}" autocomplete="off" class="layui-input">
            </div>
            <label class="layui-form-label">路由别名</label>
            <div class="layui-input-inline layui-short-input">
                <input type="text" placeholder="路由的slug" name="{{makeElUniqueName('route_name')}}" autocomplete="off" class="layui-input">
            </div>
            <div class="layui-input-inline" style="width:auto">
                <button class="layui-btn" lay-filter="{{makeElUniqueName('search_route')}}"><i class="layui-icon">&#xe615;</i> 搜索</button>&nbsp;
                <button class="layui-btn layui-btn-normal" lay-filter="{{makeElUniqueName('add_route')}}"><i class="layui-icon">&#xe654; </i> 新增</button>
            </div>
        </div>
    </div>
</div>
<div id="{{makeElUniqueName('tbRoute')}}"></div>
<script>
layui.use('jfTable', function(){
    var layer = layui.layer;
    var $ = layui.jquery;
    var jfTable = layui.jfTable;

    layui.define(function(exports){
        var obj = {
            doEdit:function(routeId) {
                $.get('/backstage/route/edit/'+ routeId, {}, function(str){
                    var popLayerUtil = layui.popLayerUtil;
                    popLayerUtil.doPopUp({
                        index: layer.open({
                            id: '{{makeElUniqueName('editRoute')}}',
                            title: '修改路由',
                            type: 1,
                            content: str,
                            area: ['800px', '570px']
                        }),
                        onClose: function() {
                            layui.routeFuncs.refreshTableGrid();
                        }
                    });
                });
            },
            doDelete:function(routeId) {
                layer.confirm('确定删除该路由？', {
                    btn: ['确定','放弃'],
                    icon: 3
                }, function(){
                    var index = layer.load(1);
                    $.ajax({
                        contentType: "application/json",
                        type: 'post',
                        url: '/backstage/api/route/delete',
                        data: JSON.stringify({
                            id: routeId
                        }),
                        success: function (outResult) {
                            layer.close(index);
                            if (outResult.Success) {
                                layer.msg(outResult.Message, { icon: 6 });
                                layui.routeFuncs.refreshTableGrid();
                            } else {
                                layer.msg(outResult.Message, { icon: 5 });
                            }
                        },
                        error: function (error) {
                            layer.close(index);
                            layui.validator.processValidateError(error);
                        }
                    });
                }, function(){
                });
            },
            refreshTableGrid: function() {
                $('input[name=\'{{makeElUniqueName('route_path')}}\']').val('');
                $('input[name=\'{{makeElUniqueName('route_action')}}\']').val('');
                $('input[name=\'{{makeElUniqueName('route_name')}}\']').val('');
                $("#{{makeElUniqueName('tbRoute')}}").jfTable("reload");
            }
        };
        exports('routeFuncs', obj);
    });

    $("#{{makeElUniqueName('tbRoute')}}").jfTable({
        url: '/backstage/api/route/query',
        pageSize:5,
        page: true,
        skip: true,
        first:'首页',
        last:'尾页',
        columns: [{
            text:'操作',
            name: 'id',
            width: 200,
            align: 'center',
            formatter: function(value, dataItem, index) {
                var html = '<a class="layui-btn layui-btn-small layui-btn-normal" onclick="layui.routeFuncs.doEdit(' + value + ')"><i class="layui-icon">&#xe642;</i> 编辑</a>';
                html += '&nbsp;&nbsp;<a class="layui-btn layui-btn-small layui-btn-danger" onclick="layui.routeFuncs.doDelete(' + value + ')"><i class="layui-icon">&#xe640;</i> 删除</a>';
                return html;
            }
        },{
            text:'路由名称',
            name: 'name',
            width: 160,
            align: 'left',
        },{
            text:'路由代码',
            name: 'code',
            width: 160,
            align: 'left',
        },{
            text:'路由方法',
            name: 'method_text',
            width: 80,
            align: 'center',
            formatter:function(value, dataItem, index) {
                if (dataItem.method == {{\App\Route::$ROUTE_METHOD_GET}}) {
                    return '<span style="color:red">' + value + '</span>';
                } else if (dataItem.method == {{\App\Route::$ROUTE_METHOD_POST}}) {
                    return '<span style="color:green">' + value + '</span>';
                } else if (dataItem.method == {{\App\Route::$ROUTE_METHOD_PUT}}) {
                    return '<span style="color:gray">' + value + '</span>';
                } else if (dataItem.method == {{\App\Route::$ROUTE_METHOD_DELETE}}) {
                    return '<span style="color:blue">' + value + '</span>';
                } else if (dataItem.method == {{\App\Route::$ROUTE_METHOD_PATCH}}) {
                    return '<span style="color:orange">' + value + '</span>';
                } else if (dataItem.method == {{\App\Route::$ROUTE_METHOD_OPTIONS}}) {
                    return '<span style="color:purple">' + value + '</span>';
                }
                return value;
            }
        },{
            text:'路由',
            name: 'route',
            width: 200,
            align: 'left',
        },{
            text:'Action',
            name: 'action',
            width: 350,
            align: 'left',
        },{
            text:'别名',
            name: 'slug',
            width: 200,
            align: 'left',
        },{
            text:'路由类型',
            name: 'route_type_text',
            width: 100,
            align: 'center',
        }],
        method: 'get',
        queryParam: {
            routePath:$('input[name=\'{{makeElUniqueName('route_path')}}\']').val(),
            routeAction:$('input[name=\'{{makeElUniqueName('route_action')}}\']').val(),
            routeName:$('input[name=\'{{makeElUniqueName('route_name')}}\']').val()
        },
        toolbarClass: 'layui-btn-small',
        onBeforeLoad: function (param) {
            return $.extend(param, {
                routePath:$('input[name=\'{{makeElUniqueName('route_path')}}\']').val(),
                routeAction:$('input[name=\'{{makeElUniqueName('route_action')}}\']').val(),
                routeName:$('input[name=\'{{makeElUniqueName('route_name')}}\']').val()
            });
        },
        onLoadSuccess: function (data) {
            return data;
        },
        dataFilter:function (data) {
            return data;
        }
    });

    $('.layui-btn[lay-filter=\'{{makeElUniqueName('search_route')}}\']').on('click', function(){
        $("#{{makeElUniqueName('tbRoute')}}").jfTable("reload");
    });

    $('.layui-btn[lay-filter=\'{{makeElUniqueName('add_route')}}\']').on('click', function(){
        $.get('/backstage/route/create', {}, function(str){
            var popLayerUtil = layui.popLayerUtil;
            popLayerUtil.doPopUp({
                index: layer.open({
                    id: '{{makeElUniqueName('createRoute')}}',
                    title: '新建路由',
                    type: 1,
                    content: str,
                    area: ['800px', '570px']
                }),
                onClose: function() {
                    layui.routeFuncs.refreshTableGrid();
                }
            });
        });
    });
});
</script>
