<div class="layui-form layui-layer-outerbox">
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 路由名称</label>
        <div class="layui-input-block">
            <input type="hidden" name="{{makeElUniqueName('route_id')}}" value="{{ isset($route)?$route->id:'' }}">
            <input type="text" name="{{makeElUniqueName('route_name')}}" value="{{ isset($route)?$route->name:'' }}" required lay-verify="required" placeholder="标识该路由的显式名称，必填（*）" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 路由代码</label>
        <div class="layui-input-block">
            <input type="text" name="{{makeElUniqueName('route_code')}}" value="{{ isset($route)?$route->code:'' }}" required lay-verify="required" placeholder="路由的唯一标识符，必填（*）" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 路由方法</label>
        <div class="layui-input-block">
            <select name="{{makeElUniqueName('route_method')}}" lay-verify="required">
                @foreach (\App\Route::$ROUTE_METHOD_MAP as $item)
                    @if (isset($route))
                        <option value='{{$item['key']}}' {{ $route->method==$item['key']?'selected':'' }}>{{$item['text']}}</option>
                    @else
                        <option value='{{$item['key']}}'>{{$item['text']}}</option>
                    @endif
                @endforeach
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 路由路径</label>
        <div class="layui-input-block">
            <input type="text" name="{{makeElUniqueName('route_path')}}" value="{{ isset($route)?$route->route:'' }}" required lay-verify="required" placeholder="[laravel]路由所指代的访问路径，必填（*）" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 路由Action</label>
        <div class="layui-input-block">
            <input type="text" name="{{makeElUniqueName('route_action')}}" value="{{ isset($route)?$route->action:'' }}" required lay-verify="required" placeholder="[laravel]路由所指向的程序Controller的Action，必填（*）" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">路由别名</label>
        <div class="layui-input-block">
            <input type="text" name="{{makeElUniqueName('route_slug')}}" value="{{ isset($route)?$route->slug:'' }}" placeholder="[laravel]路由的别名" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">路由类型</label>
        <div class="layui-input-block">
            @foreach (\App\Route::$ROUTE_TYPE_MAP as $item)
                @if (isset($route))
                <input type='radio' name='{{makeElUniqueName('route_type')}}' value='{{$item['key']}}' title='{{$item['text']}}' {{ $route->route_type==$item['key']?'checked':'' }}>
                @else
                    @if ($loop->first)
                    <input type='radio' name='{{makeElUniqueName('route_type')}}' value='{{$item['key']}}' title='{{$item['text']}}' checked>
                    @else
                    <input type='radio' name='{{makeElUniqueName('route_type')}}' value='{{$item['key']}}' title='{{$item['text']}}' >
                    @endif
                @endif
            @endforeach
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"><span style="color:red">*</span> 排序号</label>
        <div class="layui-input-block">
            <input type="text" name="{{makeElUniqueName('sort_order')}}" value="{{ isset($route)?$route->sort_order:'' }}" required lay-verify="required|number" placeholder="路由的排序号" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item layui-form-center">
            <button class="layui-btn" lay-submit lay-filter="{{makeElUniqueName('route_save')}}">保存</button>
            <button class="layui-btn layui-btn-primary" onclick="layer.close(layui.popLayerUtil.index);">关闭</button>
    </div>
</div>

<script>
layui.use(['form', 'validator'], function(){
    var form = layui.form();
    var $ = layui.jquery;
    var popLayerUtil = layui.popLayerUtil;
    //按layui的要求，如果要生成表单select/checkbox这些，必须先调用form.render()方法
    form.render();

    //所有ajax请求的api，都必须放到/backstage/api下面，避免被CSRF拦截
    form.on('submit({{makeElUniqueName('route_save')}})', function(data){
        var index = layer.load(1);
        var url = '/backstage/api/route/savenew';
        var postParam = {
            name: data.field['{{makeElUniqueName('route_name')}}'],
            code: data.field['{{makeElUniqueName('route_code')}}'],
            route: data.field['{{makeElUniqueName('route_path')}}'],
            action: data.field['{{makeElUniqueName('route_action')}}'],
            slug: data.field['{{makeElUniqueName('route_slug')}}'],
            route_type: data.field['{{makeElUniqueName('route_type')}}'],
            method: data.field['{{makeElUniqueName('route_method')}}'],
            sort_order: data.field['{{makeElUniqueName('sort_order')}}'],
        };
        var routeId = data.field['{{makeElUniqueName('route_id')}}'];
        if (routeId != '') {
            //修改
            url = '/backstage/api/route/update';
            postParam.id = routeId;
        }
        $.ajax({
            contentType: "application/json",
            type: 'post',
            url: url,
            data: JSON.stringify(postParam),
            success: function (outResult) {
                layer.close(index);
                if (outResult.Success) {
                    layer.msg(outResult.Message, { icon: 6 });
                    layer.close(popLayerUtil.index);
                    //利用自定义的扩展popLayerUtil，来实现关闭当前layer的效果
                    //popLayerUtil定义了一个回调函数onClose，可以在创建layer的时候设置该回调函数
                    //一旦前面执行完毕，则开始执行此回调，回调可以做一些动作，比如新增记录后重新刷新网格
                    popLayerUtil.onClose();
                } else {
                    layer.msg(outResult.Message, { icon: 5 });
                }
            },
            error: function (error) {
                layer.close(index);
                layui.validator.processValidateError(error);
            }
        });
        return false;
    });
});
</script>
