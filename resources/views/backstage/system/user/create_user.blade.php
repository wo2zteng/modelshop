<div class="layui-form">
    <div class="layui-layer-outerbox" style="margin-bottom:80px">
        <div class="layui-form-item">
            <label class="layui-form-label"><span style="color:red">*</span> 用户手机号</label>
            <div class="layui-input-block">
                <input type="hidden" name="{{makeElUniqueName('groupId')}}" value="{{$groupId}}">
                <input type="text" name="{{makeElUniqueName('user_mobile')}}" readonly value="{{ $mobile }}" placeholder="用户的手机号，必填，唯一" required lay-verify="required" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">用户登录名</label>
            <div class="layui-input-block">
                <input type="text" name="{{makeElUniqueName('user_name')}}" value="{{ $mobile }}" placeholder="用户的系统账号名，只能支持大小写之母以及数字下划线，缺省为用户手机号" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">用户名称</label>
            <div class="layui-input-block">
                <input type="text" name="{{makeElUniqueName('user_nick_name')}}" value="{{ $mobile }}" placeholder="用户的系统昵称，缺省为用户手机号" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label"><span style="color:red">*</span> 初始密码</label>
            <div class="layui-input-block">
                <input type="password" name="{{makeElUniqueName('user_password')}}" value="" placeholder="用户的初始密码，最长20位，支持大小写字符数字以及特殊符号" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label"><span style="color:red">*</span> 确认密码</label>
            <div class="layui-input-block">
                <input type="password" name="{{makeElUniqueName('user_password_again')}}" value="" placeholder="再次确认密码" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">用户邮箱</label>
            <div class="layui-input-block">
                <input type="text" name="{{makeElUniqueName('user_email')}}" value="" placeholder="用户的邮箱" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div>
            <fieldset class="layui-elem-field" style="margin-top: 20px;margin-bottom:20px">
                <legend class="layui-default-fieldsets-title" style="font-size:14px;font-weight:bold">用户角色列表</legend>
                <ul>
                    @foreach ($roles as $role)
                        <li><input type="checkbox" lay-filter="{{makeElUniqueName('role')}}" name="{{makeElUniqueName('role')}}" lay-skin="primary" value="{{$role->id}}" title="{{$role->name}}"></li>
                    @endforeach
                </ul>
            </fieldset>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">手机验证码</label>
            <div class="layui-input-inline layui-short-input">
                <input type="text" name="{{makeElUniqueName('verify_code')}}" value="" placeholder="" autocomplete="off" class="layui-input">
            </div>
            <div class="layui-input-inline layui-short-input">
                <button class="layui-btn" lay-filter="{{makeElUniqueName('btn_get_verify')}}">点击获得验证码</button>
            </div>
        </div>
    </div>
    <div class="layui-dialog-bar">
        <button class="layui-btn" lay-submit lay-filter="{{makeElUniqueName('btn_save')}}"><i class="layui-icon">&#xe605;</i> 保存</button>&nbsp;
        <button class="layui-btn layui-btn-primary" onclick="layer.close(layui.popLayerUtil.index);"><i class="layui-icon">&#x1006;</i>  关闭</button>
    </div>
</div>

<script>
layui.use(['form', 'validator'], function(){
    var form = layui.form();
    var $ = layui.jquery;
    var popLayerUtil = layui.popLayerUtil;
    var seconds = 60;
    var remain = seconds;

    form.render();

    layui.define(function(exports) {
        var obj = {
            intervalHandler: 0,
            getVerifyCode: function() {
                $.ajax({
                    contentType: "application/json",
                    type: 'post',
                    url: '/backstage/api/user/register-user/getverifycode',
                    data: JSON.stringify({
                        mobile: '{{ $mobile }}',
                    }),
                    success: function (outResult) {
                        layui.deptCreateUserFuncs.disableVerifyButton();
                    },
                    error: function (error) {
                        layui.validator.processValidateError(error);
                    }
                });
                return false;
            },
            disableVerifyButton: function() {
                var btn = $('.layui-btn[lay-filter=\'{{makeElUniqueName('btn_get_verify')}}\']');
                btn.addClass('layui-btn-primary');
                btn.attr('disabled', 'disabled');
                remain = seconds;
                btn.text('剩余' + remain + '秒');
                layui.deptCreateUserFuncs.intervalHandler = setInterval(layui.deptCreateUserFuncs.enableVerifyButton, 1000);
            },
            enableVerifyButton: function() {
                var btn = $('.layui-btn[lay-filter=\'{{makeElUniqueName('btn_get_verify')}}\']');
                remain = remain - 1;
                if (remain <= 0) {
                    btn.removeClass('layui-btn-primary');
                    btn.removeAttr('disabled');
                    btn.text('点击获得验证码');
                    clearInterval(layui.deptCreateUserFuncs.intervalHandler);
                } else {
                    btn.text('剩余' + remain + '秒');
                }
            },
            saveUser: function(data) {
                var index = layer.load(1);
                var url = '/backstage/api/user/save';
                var roles = '';
                $('input[name=\'{{makeElUniqueName('role')}}\']').each(function(){
                    if ($(this).prop('checked')) {
                        roles += $(this).val() + ',';
                    }
                });
                if (roles != '') {
                    roles = roles.substring(0, roles.length-1);
                }

                $.ajax({
                    contentType: "application/json",
                    type: 'post',
                    url: url,
                    data: JSON.stringify({
                        mobile: data.field['{{makeElUniqueName('user_mobile')}}'],
                        name: data.field['{{makeElUniqueName('user_name')}}'],
                        nick_name: data.field['{{makeElUniqueName('user_nick_name')}}'],
                        password: data.field['{{makeElUniqueName('user_password')}}'],
                        password_again: data.field['{{makeElUniqueName('user_password_again')}}'],
                        email: data.field['{{makeElUniqueName('user_email')}}'],
                        verifyCode: data.field['{{makeElUniqueName('verify_code')}}'],
                        roleIds: roles,
                        group_id: data.field['{{makeElUniqueName('groupId')}}'],
                    }),
                    success: function (outResult) {
                        layer.close(index);
                        if (outResult.Success) {
                            layer.msg(outResult.Message, { icon: 6 });
                            layer.close(popLayerUtil.index);
                            layui.deptUserFuncs.reloadContent();
                        } else {
                            layer.msg(outResult.Message, { icon: 5 });
                        }
                    },
                    error: function (error) {
                        layer.close(index);
                        layui.validator.processValidateError(error);
                    }
                });

                return false;
            }
        }
        exports('deptCreateUserFuncs', obj);
    });

    layui.deptCreateUserFuncs.getVerifyCode();

    $('.layui-btn[lay-filter=\'{{makeElUniqueName('btn_get_verify')}}\']').on('click', layui.deptCreateUserFuncs.getVerifyCode);

    form.on('submit({{makeElUniqueName('btn_save')}})', layui.deptCreateUserFuncs.saveUser);
});
</script>
