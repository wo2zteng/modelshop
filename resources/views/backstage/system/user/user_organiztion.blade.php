<div class="layui-default-tree-left">
    <ul id="{{makeElUniqueName('left-navi')}}"></ul>
</div>
<div class="layui-default-tree-navi">
    <div class="layui-field-box">
        <div class="layui-form-item layui-form" style="margin:0;margin-top:15px;">
            <div class="layui-inline">
                <label class="layui-form-label">用户手机</label>
                <div class="layui-input-inline layui-short-input">
                    <input type="hidden" name="{{makeElUniqueName('group_id')}}">
                    <input type="text" placeholder="用户手机" name="{{makeElUniqueName('mobile')}}" autocomplete="off" class="layui-input">
                </div>
                <label class="layui-form-label">用户名称</label>
                <div class="layui-input-inline layui-short-input">
                    <input type="text" placeholder="用户昵称/登录名" name="{{makeElUniqueName('nick_name')}}" autocomplete="off" class="layui-input">
                </div>
                <label class="layui-form-label">用户角色</label>
                <div class="layui-input-inline layui-short-input">
                    <select name="{{makeElUniqueName('roles')}}">
                        <option value="0">(请选择角色)</option>
                        @foreach ($roles as $role)
                            <option value='{{$role->id}}'>{{$role->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="layui-input-inline" style="width:auto">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">用户状态</label>
                <div class="layui-input-inline layui-short-input">
                    <select name="{{makeElUniqueName('status')}}">
                        <option value="0">(请选择状态)</option>
                        @foreach (\App\User::$USER_STATUS_MAP as $item)
                            <option value='{{$item['key']}}' {{ $item['key']==\App\User::$USER_STATUS_ACTIVE?'selected':'' }}>{{$item['text']}}</option>
                        @endforeach
                    </select>
                </div>
                <label class="layui-form-label"></label>
                <div class="layui-input-inline" style="width:auto">
                    <button class="layui-btn" lay-filter="{{makeElUniqueName('search_user')}}"><i class="layui-icon">&#xe615;</i> 搜索</button>&nbsp;
                    <button class="layui-btn layui-btn-normal" lay-filter="{{makeElUniqueName('add_user')}}"><i class="layui-icon">&#xe654; </i> 新增</button>
                </div>
            </div>
        </div>
    </div>
    <div id="{{makeElUniqueName('tbUser')}}"></div>
</div>
<script>
layui.use(['jfTable', 'tree', 'form'], function(){
    var layer = layui.layer;
    var $ = layui.jquery;
    var jfTable = layui.jfTable;
    var form = layui.form();

    form.render();

    layui.define(function(exports){
        var obj = {
            doEdit:function(userId) {
                $.get('/backstage/user/edit/'+ userId, {}, function(str){
                    var popLayerUtil = layui.popLayerUtil;
                    popLayerUtil.doPopUp({
                        index: layer.open({
                            id: '{{makeElUniqueName('editUser')}}',
                            title: '修改用户信息',
                            type: 1,
                            content: str,
                            area: ['750px', '510px']
                        }),
                        onClose: function() {
                            layui.deptUserFuncs.refreshNaviTree();
                        }
                    });
                });
            },
            doDelete:function(userId) {
                layer.confirm('确定将该用户从部门移除？', {
                    btn: ['确定','放弃'],
                    icon: 3
                }, function(){
                    var index = layer.load(1);
                    $.ajax({
                        contentType: "application/json",
                        type: 'post',
                        url: '/backstage/api/user/removefromgroup',
                        data: JSON.stringify({
                            id: userId
                        }),
                        success: function (outResult) {
                            layer.close(index);
                            if (outResult.Success) {
                                layer.msg(outResult.Message, { icon: 6 });
                                layui.deptUserFuncs.refreshNaviTree();
                            } else {
                                layer.msg(outResult.Message, { icon: 5 });
                            }
                        },
                        error: function (error) {
                            layer.close(index);
                            layui.validator.processValidateError(error);
                        }
                    });
                }, function(){
                });
            },
            refreshTableGrid: function() {
                $('input[name=\'{{makeElUniqueName('mobile')}}\']').val('');
                $('input[name=\'{{makeElUniqueName('nick_name')}}\']').val('');
                $("#{{makeElUniqueName('tbUser')}}").jfTable("reload");
            },
            initNaviTree: function() {
                this.doInitNaviTree(layui.deptUserFuncs.initTableGrid);
            },
            refreshNaviTree: function() {
                $('#{{makeElUniqueName('left-navi')}}').find('li').each(function() {
                    $(this).remove();
                });
                this.doInitNaviTree(layui.deptUserFuncs.refreshTableGrid);
            },
            reloadContent: function() {
                this.refreshNaviTree();
            },
            doInitNaviTree: function(callback) {
                var index = layer.load(1);
                $.ajax({
                    contentType: "application/json",
                    type: 'get',
                    url: '/backstage/api/user-department/gettree',
                    success: function (outResult) {
                        layer.close(index);
                        if (outResult.length > 0) {
                            $('input[name=\'{{makeElUniqueName('group_id')}}\']').val(outResult[0].id);
                        }
                        layui.tree({
                            elem: '#{{makeElUniqueName('left-navi')}}',
                            skin: 'shihuang',
                            nodes: outResult,
                            click: function(node) {
                                $('input[name=\'{{makeElUniqueName('group_id')}}\']').val(node.id);
                                layui.deptUserFuncs.refreshTableGrid();
                            }
                        });
                        callback.call(this);
                    },
                    error: function (error) {
                        layer.close(index);
                        layui.validator.processValidateError(error);
                    }
                });
            },
            initTableGrid: function() {
                $("#{{makeElUniqueName('tbUser')}}").jfTable({
                    url: '/backstage/api/user-department/query',
                    pageSize:5,
                    page: true,
                    skip: true,
                    first:'首页',
                    last:'尾页',
                    columns: [{
                        text:'操作',
                        name: 'id',
                        width: 420,
                        align: 'center',
                        formatter: function(value, dataItem, index) {
                            var html = '<a class="layui-btn layui-btn-small layui-btn-normal" onclick="layui.deptUserFuncs.doEdit(' + value + ')"><i class="layui-icon">&#xe642;</i> 修改用户信息</a>';
                            html += '&nbsp;&nbsp;<a class="layui-btn layui-btn-small layui-btn-danger" onclick="layui.deptUserFuncs.doDelete(' + value + ')"><i class="layui-icon">&#xe640;</i> 从当前组织移除</a>';
                            html += '&nbsp;&nbsp;<a class="layui-btn layui-btn-small" onclick="layui.deptUserFuncs.doSetRole(' + value + ', \'' + dataItem.name + '\')"><i class="layui-icon">&#xe620;</i> 设置用户角色</a>';
                            return html;
                        }
                    },{
                        text:'用户昵称',
                        name: 'nick_name',
                        width: 120,
                        align: 'center',
                    },{
                        text:'登录名',
                        name: 'name',
                        width: 120,
                        align: 'center',
                    },{
                        text:'手机号',
                        name: 'mobile',
                        width: 160,
                        align: 'center',
                    },{
                        text:'邮箱',
                        name: 'email',
                        width: 260,
                        align: 'center',
                    },{
                        text:'状态',
                        name: 'user_active_text',
                        width: 100,
                        align: 'center',
                        formatter:function(value, dataItem, index) {
                            if (dataItem.user_active == {{\App\User::$USER_STATUS_INACTIVE}}) {
                                return '<span style="color:red">' + value + '</span>';
                            } else if (dataItem.user_active == {{\App\User::$USER_STATUS_ACTIVE}}) {
                                return '<span style="color:blue">' + value + '</span>';
                            } else if (dataItem.user_active == {{\App\User::$USER_STATUS_BANNED}}) {
                                return '<span style="color:gray">' + value + '</span>';
                            }
                            return value;
                        }
                    }],
                    method: 'get',
                    queryParam: {
                        name:$('input[name=\'{{makeElUniqueName('nick_name')}}\']').val(),
                        deptId:$('input[name=\'{{makeElUniqueName('group_id')}}\']').val(),
                        mobile:$('input[name=\'{{makeElUniqueName('mobile')}}\']').val(),
                        activeStatus:$('select[name=\'{{makeElUniqueName('status')}}\']').val(),
                        roleId:$('select[name=\'{{makeElUniqueName('roles')}}\']').val(),
                    },
                    toolbarClass: 'layui-btn-small',
                    onBeforeLoad: function (param) {
                        return $.extend(param, {
                            name:$('input[name=\'{{makeElUniqueName('nick_name')}}\']').val(),
                            deptId:$('input[name=\'{{makeElUniqueName('group_id')}}\']').val(),
                            mobile:$('input[name=\'{{makeElUniqueName('mobile')}}\']').val(),
                            activeStatus:$('select[name=\'{{makeElUniqueName('status')}}\']').val(),
                            roleId:$('select[name=\'{{makeElUniqueName('roles')}}\']').val(),
                        });
                    },
                    onLoadSuccess: function (data) {
                        return data;
                    },
                    dataFilter:function (data) {
                        return data;
                    }
                });
            }
        };
        exports('deptUserFuncs', obj);
    });

    layui.deptUserFuncs.initNaviTree();

    $('.layui-btn[lay-filter=\'{{makeElUniqueName('search_user')}}\']').on('click', function(){
        $("#{{makeElUniqueName('tbUser')}}").jfTable("reload");
    });

    $('.layui-btn[lay-filter=\'{{makeElUniqueName('add_user')}}\']').on('click', function(){
        var departGroupId = $('input[name=\'{{makeElUniqueName('group_id')}}\']').val();
        if (departGroupId != '' && departGroupId != undefined) {
            $.get('/backstage/user-to-group/select/' + departGroupId, {}, function(str){
                var popLayerUtil = layui.popLayerUtil;
                popLayerUtil.doPopUp({
                    index: layer.open({
                        id: '{{makeElUniqueName('createUser')}}',
                        title: '新增部门用户',
                        type: 1,
                        content: str,
                        area: ['750px', '510px']
                    }),
                    onClose: function(callback) {
                        callback.call(this, $('#{{makeElUniqueName('createUser')}}'));
                    }
                });
            });
        } else {
            layer.msg('还没有选择所在部门组织', { icon: 5 });
        }

    });
});
</script>
