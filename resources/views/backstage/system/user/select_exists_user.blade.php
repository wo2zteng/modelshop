<div class="layui-form">
    <div class="layui-layer-outerbox layui-form-pane" style="margin-bottom:80px">
        <div class="layui-form-item">
            <label style="color:red;font-weight:boldstyle;width:auto">（找到如下用户，可以将其添加到当前组织）</label>
        </div>
        <div class="layui-form-item" pane>
            <label class="layui-form-label">用户手机号</label>
            <div class="layui-input-block">
                <input type="hidden" name="{{makeElUniqueName('user_id')}}" value="{{ isset($user)?$user->id:'' }}">
                <input type="hidden" name="{{makeElUniqueName('group_id')}}" value="{{$groupId}}">
                <input type="text" name="{{makeElUniqueName('user_mobile')}}" style="color:blue;border-width:0px" readonly value="{{ isset($user)?$user->mobile:'' }}" placeholder="" required lay-verify="required" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item" pane>
            <label class="layui-form-label">用户登录名</label>
            <div class="layui-input-block">
                <input type="text" name="{{makeElUniqueName('user_name')}}" style="color:blue;border-width:0px" readonly value="{{ isset($user)?$user->name:'' }}" placeholder="" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item" pane>
            <label class="layui-form-label">用户名称</label>
            <div class="layui-input-block">
                <input type="text" name="{{makeElUniqueName('user_nick_name')}}" style="color:blue;border-width:0px" readonly value="{{ isset($user)?$user->nick_name:'' }}" placeholder="" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item" pane>
            <label class="layui-form-label">用户邮箱</label>
            <div class="layui-input-block">
                <input type="text" name="{{makeElUniqueName('user_email')}}" style="color:blue;border-width:0px" readonly value="{{ isset($user)?$user->email:'' }}" placeholder="" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div>
            <fieldset class="layui-elem-field" style="margin-top: 20px;margin-bottom:20px">
                <legend class="layui-default-fieldsets-title" style="font-size:14px;font-weight:bold">用户角色列表</legend>
                <ul>
                    @foreach ($roles as $role)
                        <li><input type="checkbox" lay-filter="{{makeElUniqueName('role')}}" name="{{makeElUniqueName('role')}}" lay-skin="primary" value="{{$role->id}}" title="{{$role->name}}"></li>
                    @endforeach
                </ul>
            </fieldset>
        </div>
    </div>
    <div class="layui-dialog-bar">
        <button class="layui-btn" lay-submit lay-filter="{{makeElUniqueName('user_add')}}"><i class="layui-icon">&#xe654;</i> 添加用户到当前组织</button>&nbsp;
        <button class="layui-btn layui-btn-primary" onclick="layer.close(layui.popLayerUtil.index);"><i class="layui-icon">&#x1006;</i>  关闭</button>
    </div>
</div>
<script>
layui.use(['form', 'validator'], function(){
    var form = layui.form();
    var $ = layui.jquery;
    var popLayerUtil = layui.popLayerUtil;

    form.render();
    
    form.on('submit({{makeElUniqueName('user_add')}})', function(data) {
        var index = layer.load(1);
        var url = '/backstage/api/user/addtogroup';

        var roles = '';
        $('input[name=\'{{makeElUniqueName('role')}}\']').each(function(){
            if ($(this).prop('checked')) {
                roles += $(this).val() + ',';
            }
        });
        if (roles != '') {
            roles = roles.substring(0, roles.length-1);
        }

        $.ajax({
            contentType: "application/json",
            type: 'post',
            url: url,
            data: JSON.stringify({
                id: data.field['{{makeElUniqueName('user_id')}}'],
                groupId: data.field['{{makeElUniqueName('group_id')}}'],
                roleIds: roles
            }),
            success: function (outResult) {
                layer.close(index);
                if (outResult.Success) {
                    layer.msg(outResult.Message, { icon: 6 });
                    layer.close(popLayerUtil.index);
                    layui.deptUserFuncs.reloadContent();
                } else {
                    layer.msg(outResult.Message, { icon: 5 });
                }
            },
            error: function (error) {
                layer.close(index);
                layui.validator.processValidateError(error);
            }
        });

        return false;
    });
});
</script>
