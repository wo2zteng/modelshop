<div class="layui-form">
    <div class="layui-layer-outerbox" style="margin-bottom:80px">
        <div class="layui-form-item">
            <label class="layui-form-label"><span style="color:red">*</span> 用户手机号</label>
            <div class="layui-input-block">
                <input type="hidden" name="{{makeElUniqueName('user_id')}}" value="{{$user->id}}">
                @if (!$enableParentOrg)
                    <input type="hidden" name="{{makeElUniqueName('user_group')}}" value="{{ isset($currentUserGroup)?$currentUserGroup->id:'' }}">
                @endif
                <input type="text" name="{{makeElUniqueName('user_mobile')}}" readonly value="{{ $user->mobile }}" placeholder="用户的手机号，必填，唯一" required lay-verify="required" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">用户名称</label>
            <div class="layui-input-block">
                <input type="text" name="{{makeElUniqueName('user_nick_name')}}" value="{{ $user->nick_name }}" placeholder="用户的系统昵称，缺省为用户手机号" autocomplete="off" class="layui-input">
            </div>
        </div>
        @if ($enableParentOrg)
        <div class="layui-form-item">
            <label class="layui-form-label"><span style="color:red">*</span> 所属部门</label>
            <div class="layui-input-block">
                <select name="{{makeElUniqueName('user_group')}}" lay-search required lay-verify="required">
                    @foreach ($groups as $group)
                        <option value='{{$group->id}}' {{ $currentUserGroup->id==$group->id?'selected':'' }}>{{ $group->getGroupPath() }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        @endif
        <div class="layui-form-item">
            <label class="layui-form-label">初始密码</label>
            <div class="layui-input-block">
                <input type="password" name="{{makeElUniqueName('user_password')}}" value="" placeholder="用户的初始密码，最长20位，支持大小写字符数字以及特殊符号，为空不修改密码" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">确认密码</label>
            <div class="layui-input-block">
                <input type="password" name="{{makeElUniqueName('user_password_again')}}" value="" placeholder="再次确认密码" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">用户邮箱</label>
            <div class="layui-input-block">
                <input type="text" name="{{makeElUniqueName('user_email')}}" value="{{ $user->email }}" placeholder="用户的邮箱" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">手机验证码</label>
            <div class="layui-input-inline layui-short-input">
                <input type="text" name="{{makeElUniqueName('verify_code')}}" value="" placeholder="" autocomplete="off" class="layui-input">
            </div>
            <div class="layui-input-inline layui-short-input">
                <button class="layui-btn" lay-filter="{{makeElUniqueName('btn_get_verify')}}">点击获得验证码</button>
            </div>
        </div>
    </div>
    <div class="layui-dialog-bar">
        <button class="layui-btn" lay-submit lay-filter="{{makeElUniqueName('btn_save')}}"><i class="layui-icon">&#xe605;</i> 保存</button>&nbsp;
        <button class="layui-btn layui-btn-primary" onclick="layer.close(layui.popLayerUtil.index);"><i class="layui-icon">&#x1006;</i>  关闭</button>
    </div>
</div>
<script>
layui.use(['form', 'validator'], function(){
    var form = layui.form();
    var $ = layui.jquery;
    var popLayerUtil = layui.popLayerUtil;
    var seconds = 60;
    var remain = seconds;

    form.render();

    layui.define(function(exports) {
        var obj = {
            intervalHandler: 0,
            getVerifyCode: function() {
                $.ajax({
                    contentType: "application/json",
                    type: 'post',
                    url: '/backstage/api/user/modify-user/getverifycode',
                    data: JSON.stringify({
                        mobile: '{{ $user->mobile }}',
                    }),
                    success: function (outResult) {
                        layui.deptEditUserFuncs.disableVerifyButton();
                    },
                    error: function (error) {
                        layui.validator.processValidateError(error);
                    }
                });
                return false;
            },
            disableVerifyButton: function() {
                var btn = $('.layui-btn[lay-filter=\'{{makeElUniqueName('btn_get_verify')}}\']');
                btn.addClass('layui-btn-primary');
                btn.attr('disabled', 'disabled');
                remain = seconds;
                btn.text('剩余' + remain + '秒');
                layui.deptEditUserFuncs.intervalHandler = setInterval(layui.deptEditUserFuncs.enableVerifyButton, 1000);
            },
            enableVerifyButton: function() {
                var btn = $('.layui-btn[lay-filter=\'{{makeElUniqueName('btn_get_verify')}}\']');
                remain = remain - 1;
                if (remain <= 0) {
                    btn.removeClass('layui-btn-primary');
                    btn.removeAttr('disabled');
                    btn.text('点击获得验证码');
                    clearInterval(layui.deptEditUserFuncs.intervalHandler);
                } else {
                    btn.text('剩余' + remain + '秒');
                }
            },
            saveUser: function(data) {
                var index = layer.load(1);
                var url = '/backstage/api/user/saveupdate';

                $.ajax({
                    contentType: "application/json",
                    type: 'post',
                    url: url,
                    data: JSON.stringify({
                        id: data.field['{{makeElUniqueName('user_id')}}'],
                        mobile: data.field['{{makeElUniqueName('user_mobile')}}'],

                        nick_name: data.field['{{makeElUniqueName('user_nick_name')}}'],
                        password: data.field['{{makeElUniqueName('user_password')}}'],
                        password_again: data.field['{{makeElUniqueName('user_password_again')}}'],
                        email: data.field['{{makeElUniqueName('user_email')}}'],
                        verifyCode: data.field['{{makeElUniqueName('verify_code')}}'],
                        @if ($enableParentOrg)
                        group_id: data.field['{{makeElUniqueName('user_group')}}'],
                        @else
                        group_id: data.field['{{makeElUniqueName('user_group')}}'],
                        @endif
                    }),
                    success: function (outResult) {
                        layer.close(index);
                        if (outResult.Success) {
                            layer.msg(outResult.Message, { icon: 6 });
                            layer.close(popLayerUtil.index);
                            layui.deptUserFuncs.reloadContent();
                        } else {
                            layer.msg(outResult.Message, { icon: 5 });
                        }
                    },
                    error: function (error) {
                        layer.close(index);
                        layui.validator.processValidateError(error);
                    }
                });

                return false;
            }
        }
        exports('deptEditUserFuncs', obj);
    });

    clearInterval(layui.deptEditUserFuncs.intervalHandler);

    $('.layui-btn[lay-filter=\'{{makeElUniqueName('btn_get_verify')}}\']').on('click', layui.deptEditUserFuncs.getVerifyCode);

    form.on('submit({{makeElUniqueName('btn_save')}})', layui.deptEditUserFuncs.saveUser);
});
</script>
