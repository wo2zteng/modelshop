<div class="layui-form layui-layer-outerbox">
    <div class="layui-form-item" style="margin-top:180px">
        <label class="layui-form-label" style="width:220px">请输入用户手机号</label>
        <div class="layui-input-inline">
            <input type="hidden" name="{{makeElUniqueName('groupId')}}" value="{{$groupId}}">
            <input type="text" name="{{makeElUniqueName('mobile')}}" placeholder="输入用户手机号以定位用户" required lay-verify="required" autocomplete="off" class="layui-input">
        </div>
        <div class="layui-input-inline" style="width:auto">
            <button class="layui-btn" lay-submit lay-filter="{{makeElUniqueName('search_user')}}"><i class="layui-icon">&#xe615;</i> 下一步</button>
        </div>
    </div>
</div>
<script>
layui.use(['form', 'validator'], function(){
    var form = layui.form();
    var $ = layui.jquery;
    var popLayerUtil = layui.popLayerUtil;

    form.render();

    form.on('submit({{makeElUniqueName('search_user')}})', function(data){
        var index = layer.load(1);
        var url = '/backstage/api/query-user-bymobile';
        var mobile = data.field['{{makeElUniqueName('mobile')}}'];
        var groupId = data.field['{{makeElUniqueName('groupId')}}'];
        $.ajax({
            contentType: "application/json",
            type: 'post',
            url: url,
            data: JSON.stringify({
                mobile: mobile,
                groupId:groupId
            }),
            success: function (outResult) {
                if (outResult.Success) {
                    if (outResult.ErrorCode != 0) {
                        layer.close(index);
                        layer.msg(outResult.Message, { icon: 5 });
                    } else {
                        layer.close(index);
                        popLayerUtil.onClose(function(dialogBox) {
                            $.get('/backstage/user/create-user-bymobile', { mobile:mobile, groupId:groupId }, function(str){
                                dialogBox.html(str);
                            });
                        });
                    }
                } else {
                    layer.close(index);
                    popLayerUtil.onClose(function(dialogBox) {
                        $.get('/backstage/user/create-user-bymobile', { mobile:mobile, groupId:groupId }, function(str){
                            dialogBox.html(str);
                        });
                    });
                }
            },
            error: function (error) {
                layer.close(index);
                layui.validator.processValidateError(error);
            }
        });

        return false;
    });
});
</script>
