<div class="layui-form layui-layer-outerbox">
    <div class="layui-form-item">
        <label class="layui-form-label" style="width:70px; padding: 2px 0px ">请选择城市</label>
    </div>
    <div class="layui-form-item">
        <select name="{{makeElUniqueName('city_select')}}" lay-filter="{{makeElUniqueName('city_select')}}" lay-search>
            @foreach ($allCities as $city)
                <option value='{{$city->id}}'>{{ $city->name }}({{ $city->code }})</option>
            @endforeach
        </select>
    </div>
</div>
<script>
layui.use(['form', 'popLayerUtil'], function(){
    var form = layui.form();
    var $ = layui.jquery;
    var popLayerUtil = layui.popLayerUtil;

    form.render();

    form.on('select({{makeElUniqueName('city_select')}})', function(data){
        var options = $(data.elem).find('option');
        var cityname = '';
        for (var i=0; i<options.length; i++) {
            if (options[i].value == data.value) {
                cityname = options[i].text;
                cityname = cityname.substring(0, cityname.indexOf('('));
                break;
            }
        }

        layer.open({
            content: '确定选中城市[' + cityname + ']？',
            btn: ['确定','取消'],
            yes: function(index, layero){
                layer.close(index);
                if (popLayerUtil.onClose) {
                    if (popLayerUtil.onClose.call(this, data.value, cityname)) {
                        layer.close(layui.popLayerUtil.index);
                    }
                }
            }
            ,btn2: function(index, layero){

            }
        });
    });
});
</script>
