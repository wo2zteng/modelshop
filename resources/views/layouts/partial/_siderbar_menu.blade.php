<div class="layui-side-scroll">
    <ul class="layui-nav layui-nav-tree site-demo-nav" lay-filter="leftnav" {!! config('theme.current')['enable']?('style="background-color:'.config('theme.current')['sidebar_menu_item_color'].'"'):'' !!}>
        @foreach ($menuAndModules as $module)
            @if (!empty($module['on']))
                @foreach ($module['groups'] as $group)
                <li class="layui-nav-item layui-nav-itemed">
                    <a class="javascript:;" href="javascript:;" {!! config('theme.current')['enable']?('style="background-color:'.config('theme.current')['sidebar_sub_menu_color'].' !important"'):'' !!}><i class="layui-icon" style="top: 3px;">{{htmlspecialchars_decode($group['icon'])}}</i><cite>{{$group['name']}}</cite></a>
                    @foreach ($group['menus'] as $menu)
                    <dl class="layui-nav-child">
                        <dd {!! !empty($menu['on'])?'class="layui-this"':'' !!}>
                            <a href="javascript:;" data-url="{{$menu['route']}}" data-id="{{$menu['id']}}" data-module-id="{{$menu['module_id']}}" {!! config('theme.current')['enable']?('style="color:'.config('theme.current')['sidebar_menu_item_fore_color'].'"'):'' !!}><i class="layui-icon" style="top: 3px;">{{!empty($menu['icon'])?htmlspecialchars_decode($menu['icon']):'&#xe623;'}}</i><cite>{{$menu['name']}}</cite></a>
                        </dd>
                    </dl>
                    @endforeach

                </li>
                @endforeach
            @endif
        @endforeach
    </ul>
</div>
