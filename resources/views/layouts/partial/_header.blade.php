<div class="layui-main">
    <a class="logo" href="/">
        <img src="{{config('system.logo_image')}}" alt="{{config('app.name')}}">
    </a>
    <div class="layui-form component">
    @if ($isSuperUser)
    <select lay-search lay-filter="user-groups">
    @foreach ($userGroups as $userGroup)
        <option value="{{$userGroup['group_id']}}" {{ $userGroup['current']==\App\BaseDictionary::$KEY_YES?'selected':'' }} >{{$userGroup['group_name']}}</option>
    @endforeach
    </select>
    @endif
    </div>
    <ul class="layui-nav" pc>
        @foreach ($menuAndModules as $module)
        <li class="layui-nav-item {{ !empty($module['on'])?'layui-this':'' }}">
            <a href="javascript:;" onclick="layui.topNaviUtils.changeLeftNaviByModule({{$module['id']}})">{{$module['name']}}</a>
        </li>
        @endforeach
        <li class="layui-nav-item">
            <a href="javascript:;"><cite>{{\Auth::user()->getDisplayName()}}</cite></a>
            <dl class="layui-nav-child">
                <dd><a href="http://layim.layui.com/" target="_blank"><i class="layui-icon" style="top: 3px;">&#xe642;</i><cite>修改密码</cite></a></dd>
                <dd><form class="logout-form" action="/logout" method="post">{{ csrf_field() }}<a href="javascript:logout()"><i class="layui-icon" style="top: 3px;">&#xe612;</i><cite>注&nbsp;&nbsp;&nbsp;&nbsp;销</cite></a><form></dd>
            </dl>
        </li>
    </ul>
</div>
