<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Expires" content="0">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-control" content="no-cache">
<meta http-equiv="Cache" content="no-cache">
<title>{{config('app.name')}}后台管理系统</title>
<link rel="stylesheet" href="/vendor/layui/css/layui.css" media="all">
<link rel="stylesheet" href="/vendor/webuploader/css/style.css" media="all">
<link rel="stylesheet" href="/vendor/webuploader/css/webuploader.css" media="all">
<link rel="stylesheet" href="/css/admin/global.css" media="all">
<link rel="stylesheet" href="/css/backstage/menu.css?v=0.1.2"  />
<style>
    .layui-btn-small {padding: 0 15px;}
    .layui-form-checkbox {margin: 0;}
    .layui-block-middle {margin-left: 130px !important;}
    .layui-block-offset {margin-left: 180px !important;}
    .layui-short-input {width: 160px !important;}
    .layui-long-input {width: 380px !important;}
    .layui-layer-outerbox {margin:30px 30px 30px 30px}
    .layui-form-center {text-align:center;margin:0px auto}
    .layui-tree-skin-shihuang .layui-tree-branch{color: #EDCA50;}
    .layui-default-tree-left {width:14%;float:left;border-right:solid 1px #e3e3e3;margin-top:0px;overflow:auto}
    .layui-default-tree-navi {width:85%;float:left;padding-left:5px}
    .layui-dialog-bar {position: fixed;bottom: 0px;height: 50px; justify-content: center; width: 100%; color:#333; border-top: solid 1px #e3e3e3; background-color: #F8F8F8; display:flex; align-items:center}
    .layui-default-fieldsets-title {font-size:12px;font-weight:bold;color:#009688}
    .layui-elem-field ul {display:flex;flex-direction:row;flex-wrap:wrap}
    .layui-elem-field ul li {align-items:center;display:flex;justify-content:flex-start;margin-left:30px;width:28%;height:50px;}
    .layui-big-upload-box {width: 500px; height: 180px; background-color: #e2e2e2; position: relative; border: dashed 1px #ccc}
    .layui-big-upload-box img {position: absolute; left: 0px; top: 0px; width: 500px; height: 180px}
    .layui-big-upload-box .site-demo-upbar{position: absolute; top: 50%; left: 50%; margin: -18px 0 0 -56px;}
    .layui-big-upload-box .layui-upload-button{background-color: rgba(0,0,0,.2); color: rgba(255,255,255,1);}
    .layui-select-box {display:flex;flex-direction:row;flex-wrap:wrap}
    .layui-select-box li {align-items:center;display:flex;justify-content:flex-start;margin-right:50px;height:36px;}
    .layui-select-box li .box-label{width:100px;height:36px;color:#bbb;background-color:#f2f2f2;border-radius:5px;display:flex;align-items:center;justify-content:space-around}
    .layui-select-box li .box-icon{display: flex;align-items: center;padding-top: 5px;cursor: pointer;}
    tr td:not(:nth-child(0)),
    tr th:not(:nth-child(0)) {text-align: center;}
</style>
</head>
<body>
    @yield('content')
</body>
</html>
