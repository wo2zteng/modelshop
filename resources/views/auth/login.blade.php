<!DOCTYPE html>
<html>
<head>
    <title>{{config('app.name')}}后台管理系统</title>
    <link rel="shortcut icon" href="/Images/Global/Logo_40.png" type="image/x-icon">
    <link href="/vendor/layui/css/layui.css" rel="stylesheet" />
    <link href="/css/admin/adminindex.css" rel="stylesheet" />
</head>
<body>
    <div class="mask"></div>
    <div class="main">
        <img src="{{config('system.logo_image')}}" width="220" height="64">
        <p id="time"></p>
        <div class="enter">
            后台管理系统
        </div>
    </div>
    <script src="/vendor/layui/layui.js"></script>
    <script type="text/javascript">
        layui.use(['element', 'layer', 'form'], function () {
            var form = layui.form();
            var $ = layui.jquery;

            login();
            //自定义验证
            form.verify({
                passWord: [/^[\S]{6,12}$/, '密码必须6到12位'],
                username: function (value) {
                    if (value.length <= 0 || value.length > 20) {
                        return "账号必须1到20位"
                    }
                    var reg = /^[a-zA-Z0-9]*$/;
                    if (!reg.test(value)) {
                        return "账号只能为英文或数字";
                    }
                },
                @if (config('captcha.enable'))
                result_response: function (value) {
                    if (value.length < 1) {
                        return '请点击人机识别验证';
                    }
                },
                @endif
            });
            //监听登陆提交
            form.on('submit(login)', function (data) {
                var index = layer.load(1);
                $.ajax({
                    contentType: "application/json",
                    type: 'post',
                    url: '/login',
                    data: JSON.stringify(data.field),
                    success: function (outResult) {
                        layer.close(index);
                        if (outResult.Success) {
                            layer.msg(outResult.Message, { icon: 6 });
                            layer.closeAll('page');
                            location.href = "/home";
                        } else {
                            layer.msg(outResult.Message, { icon: 5 });
                            LUOCAPTCHA.reset();
                        }
                    },
                    error: function (outResult) {
                        layer.close(index);
                        layer.msg("请求异常", { icon: 2 });
                        LUOCAPTCHA.reset();
                    }
                });
                return false;
            });
            $('body').keydown(function (e) {
                if (e.keyCode == 13) {
                    if ($('#layer-login').length <= 0) {
                        login();
                    } else {
                        $('button[lay-filter=login]').click();
                    }
                }
            });
        });
        function login() {
            var $ = layui.jquery;
            $.get('/toRenderLoginForm', {}, function (str) {
                layer.open({
                    id: 'layer-login',
                    type: 1,
                    title: false,
                    shade: 0.4,
                    shadeClose: false,
                    area: ['480px', '270px'],
                    closeBtn: 0,
                    anim: 1,
                    skin: 'pm-layer-login',
                    content: str
                });
                layui.form().render('checkbox');
            });
        }
    </script>
    <script type="text/javascript">
        function systemTime() {
            //获取系统时间。
            var dateTime = new Date();
            var year = dateTime.getFullYear();
            var month = dateTime.getMonth() + 1;
            var day = dateTime.getDate();
            var hh = dateTime.getHours();
            var mm = dateTime.getMinutes();
            var ss = dateTime.getSeconds();

            //分秒时间是一位数字，在数字前补0。
            mm = extra(mm);
            ss = extra(ss);

            //将时间显示到ID为time的位置，时间格式形如：19:18:02
            document.getElementById("time").innerHTML = year + "-" + month + "-" + day + " " + hh + ":" + mm + ":" + ss;
            //每隔1000ms执行方法systemTime()。
            setTimeout("systemTime()", 1000);
        }

        //补位函数。
        function extra(x) {
            //如果传入数字小于10，数字前补一位0。
            if (x < 10) {
                return "0" + x;
            } else {
                return x;
            }
        }
        systemTime();
    </script>
</body>
</html>
