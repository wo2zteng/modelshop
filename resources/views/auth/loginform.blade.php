<form class="layui-form" action="">
    {{ csrf_field() }}
    <div class="layui-form-item">
        <label class="layui-form-label">账号</label>
        <div class="layui-input-inline pm-login-input">
            <input type="text" name="username" lay-verify="username" placeholder="请输入账号" value="" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">密码</label>
        <div class="layui-input-inline pm-login-input">
            <input type="password" name="password" lay-verify="passWord" placeholder="请输入密码" value="" autocomplete="off" class="layui-input">
        </div>
    </div>
    @if (config('captcha.enable'))
    <div class="layui-form-item">
        <label class="layui-form-label">人机验证</label>
        <div class="layui-input-block">
            <div data-width="280px" class="l-captcha" data-site-key="{{config('captcha.client_site_key')}}" data-callback="getResponse"></div>
            <input type="hidden" id="result_response" name="result_response" lay-verify="result_response">
        </div>
    </div>
    @endif
    <div class="layui-form-item" style="margin-top:25px;margin-bottom:0;">
        <div class="layui-input-block">
            <button class="layui-btn" style="width:230px;" lay-submit="" lay-filter="login">立即登录</button>
        </div>
    </div>
</form>
<script src="{{config('captcha.client_url')}}"></script>
<script type="text/javascript">
    function getResponse(data) {
        var $ = layui.jquery;
        $("#result_response").val(data);
    }
</script>
