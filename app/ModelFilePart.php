<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelFilePart extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'model_id', 'trunk_id', 'sort_order', 'file_id'
    ];
}