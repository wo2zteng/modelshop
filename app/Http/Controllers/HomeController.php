<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lib\Auth\UserProfileHelper;
use App\Lib\Core\ModuleHelper;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function changeNaviModule($moduleId)
    {
        $profileHelper = new UserProfileHelper();

        $moduleHelper = new ModuleHelper($profileHelper);

        $menuAndModuleResults = $moduleHelper->setActiveModuleFlagByNaviModule($moduleId);

        return view('layouts.partial._siderbar_menu', [
            'menuAndModules' => $menuAndModuleResults['menus']
        ]);
    }

    public function toTestLoginForm(Request $request)
    {
        //dd(Route::current()->getAction());
        return view('home');
    }
}
