<?php

namespace App\Http\Controllers\Extra\Publish;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\HelpTutorialService;
use App\HelpTutorial;

class PublicInfoController extends Controller
{
    private $helpTutorialService = null;

    public function __construct(HelpTutorialService $helpTutorialService)
    {
        $this->helpTutorialService = $helpTutorialService;
    }

    public function index()
    {
        return view('extra.publish.publicinfo');
    }

    public function getInitTutorialTrees()
    {
        return $this->helpTutorialService->getCascadeTutorials();
    }

    public function getTutorialInfo($tutorialId)
    {
        return HelpTutorial::findOrFail($tutorialId);
    }

    public function getTutorialImages(Request $request)
    {
        return $this->helpTutorialService->getHelpTutorialImages($request->all());
    }
}
