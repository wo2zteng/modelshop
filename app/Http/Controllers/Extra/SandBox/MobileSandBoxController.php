<?php

namespace App\Http\Controllers\Extra\SandBox;

use App\Services\UserService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MobileSandBoxController extends Controller
{
    private $userService = null;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function index()
    {
        if (!config('sandbox.enable_verify_code_sandbox')) {
            abort(404);
        }

        return view('extra.sandbox.mobile_sandbox');
    }

    public function getVerifyCode(Request $request)
    {
        if (!config('sandbox.enable_verify_code_sandbox')) {
            abort(404);
        }

        $behavior = $request->behavior;

        if ($behavior == 1) {
            $code = $this->userService->getRegisterVerifyCode($request->mobile);
        } else if ($behavior == 2) {
            $code = $this->userService->getRegisterVerifyCode($request->mobile);
        } else if ($behavior == 3) {
            $code = $this->userService->getRegisterVerifyCode($request->mobile);
        } else if ($behavior == 4) {
            $code = $this->userService->getModifyVerifyCode($request->mobile);
        } else {
            $code = '';
        }

        return response()->json([
            'verifyCode' => $code
        ]);
    }
}
