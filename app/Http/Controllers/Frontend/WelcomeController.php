<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\BaseImgService;
use App\Services\ModelCategoryService;
use App\Services\ModelService;
use App\BaseImg;

class WelcomeController extends Controller
{
    private $baseImgService = null;
    private $modelCategoryService = null;
    private $modelService = null;

    public function __construct(BaseImgService $baseImgService,
        ModelCategoryService $modelCategoryService,
        ModelService $modelService)
    {
        $this->baseImgService = $baseImgService;
        $this->modelCategoryService = $modelCategoryService;
        $this->modelService = $modelService;
    }

    public function index()
    {
        $banners = $this->baseImgService->getBaseImages([
            'cityId' => 0,
            'imgType' => BaseImg::$BASE_IMAGE_TYPE_MAINPAGE_BANNER
        ], false);

        return view('welcome', [
            'banners' => $banners,
            'newModels' => $this->modelService->getMainPageModels(1),
            'recommandModels' => $this->modelService->getMainPageModels(2),
            'materials' => $this->modelService->getMainPageModels(3),
        ]);
    }
}