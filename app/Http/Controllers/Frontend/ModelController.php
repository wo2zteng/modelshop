<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\ModelCategoryService;
use App\Services\ModelService;
use App\Services\ModelClassService;
use App\Model;
use App\ModelCategory;

class ModelController extends Controller
{
    private $modelCategoryService = null;
    private $modelService = null;
    private $modelClassService = null;

    public function __construct(ModelCategoryService $modelCategoryService,
        ModelService $modelService,
        ModelClassService $modelClassService)
    {
        $this->modelCategoryService = $modelCategoryService;
        $this->modelService = $modelService;
        $this->modelClassService = $modelClassService;
    }

    public function index($modelId)
    {
        $models = $this->modelService->getModels([
            'modelId' => $modelId,
            'withProps' => true
        ], false);

        if (count($models) > 0) {
            $modelEntity = Model::findOrFail($models[0]['id']);
            $modelCates = $modelEntity->modelCates();
            $classId = $models[0]['class_id'];
            $props = $models[0]['properties'];

            return view('frontend.model', [
                'model_cates' => $modelCates,
                'model_class' => $modelEntity->modelClass,
                'model' => $modelEntity,
                'thumb' => $models[0]['getFullImage'],
                'broadCastImages' => $models[0]['getFullBroadCastImages'],
                'props' => $this->buildProps($props)
            ]);
        } else {
            abort(404);
        }
        
    }

    public function query($categoryId)
    {
        if ($categoryId == 0) {
            $categories = $this->modelCategoryService->getChildlevelCategories(null);
            $treeCates = [];
        } else {
            $categories = $this->modelCategoryService->getChildlevelCategories($categoryId);
            $category = ModelCategory::findOrFail($categoryId);
            $treeCates = $category->treeCates();
        }

        $modelClasses = $this->modelClassService->getCategoryClass($categoryId);
        
        return view('frontend.findmodel', [
            'categories' => $categories,
            'modelClasses' => $modelClasses,
            'treeCates' => $treeCates,
            'cateId' => $categoryId
        ]);
    }


    public function queryResult(Request $request)
    {
        $input = $request->all();

        return $this->modelService->getModels($input);
    }

    public function getChildCategories($categoryId)
    {
        if ($categoryId == 0) {
            $categories = $this->modelCategoryService->getChildlevelCategories(null);
        } else {
            $categories = $this->modelCategoryService->getChildlevelCategories($categoryId);
        }

        return $categories;
    }

    public function getModelClass($categoryId)
    {
        return $this->modelClassService->getCategoryClass($categoryId);
    }

    public function getClassProps($classId)
    {
        return $this->modelClassService->getClassDetails($classId);
    }

    public function download($modelId)
    {
        
    }

    private function buildProps($props)
    {
        $result = [];

        foreach ($props as $prop) {
            $exist = false;
            foreach ($result as &$item) {
                if ($item['key'] == $prop['name']) {
                    $item['value'] = $item['value'].','.$prop['value'];
                    $exist = true;
                    break;
                }
            }
            if (!$exist) {
                array_push($result, [
                    'key' => $prop['name'],
                    'value' => $prop['value']
                ]);
            }
        }

        return $result;
    }
}