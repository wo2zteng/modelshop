<?php

namespace App\Http\Controllers\Backstage\Common;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\ProvinceService;
use App\Services\CityService;

class SelectorController extends Controller
{
    private $provinceService = null;
    private $cityService = null;

    public function __construct(ProvinceService $provinceService,
        CityService $cityService)
    {
        $this->provinceService = $provinceService;
        $this->cityService = $cityService;
    }

    public function getCitySelector()
    {
        return view('backstage.common.city_selector', [
            'allCities' => $this->cityService->getAllCities()
        ]);
    }
}
