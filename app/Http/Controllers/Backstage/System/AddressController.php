<?php

namespace App\Http\Controllers\Backstage\System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\ProvinceService;
use App\Services\CityService;

class AddressController extends Controller
{
    private $provinceService = null;
    private $cityService = null;

    public function __construct(ProvinceService $provinceService,
        CityService $cityService)
    {
        $this->provinceService = $provinceService;
        $this->cityService = $cityService;
    }

    public function getProvinces()
    {
        return $this->provinceService->getAllProvince();
    }

    public function getCitiesByProvince($provinceId)
    {
        return $this->cityService->getProvinceCities($provinceId);
    }

    public function getDistrictsByCity($cityId)
    {
        return $this->cityService->getCityDistricts($cityId);
    }
}
