<?php

namespace App\Http\Controllers\Backstage\System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Lib\FileService\FastDFS;
use App\Lib\Util\FileUpload;
use App\Lib\Util\StringUtil;
use App\UploadedFileState;
use Storage;

class BaseUploadController extends Controller
{
    public function doUpload(Request $request)
    {
        $fileEntrys = $request->all();
        $file = null;
        $fileKey = null;
        foreach ($fileEntrys as $key => $value) {
            $file = $value;
            $fileKey = $key;
        }

        if (isset($file)) {
            $fdfs = new FastDFS();
            $fileResult = $fdfs->directSaveFile($file);

            if ($fileResult['status'] === 0) {
                $fileId = $fileResult['group'].'/'.$fileResult['fileName'];
                UploadedFileState::create([
                    'file_id' => $fileId,
                    'state' => 0
                ]);

                return response()->json([
                    'status' => 0,
                    'info' => $fileResult['info'],
                    'fileKey' => $fileKey,
                    'fileId' => $fileId,
                    'filePath' => FastDFS::getFullUrl($fileId)
                ]);
            } else {
                return response()->json([
                    'status' => $fileResult['status'],
                    'info' => $fileResult['info']
                ]);
            }
        }

        return response()->json([
            'status' => 1000,
            'info' => '文件为空'
        ]);
    }

    public function doRichEditorUpload(Request $request)
    {
        $fileEntrys = $request->all();
        $file = null;
        $fileKey = null;
        foreach ($fileEntrys as $key => $value) {
            $file = $value;
            $fileKey = $key;
        }

        if (isset($file)) {
            $fdfs = new FastDFS();
            $fileResult = $fdfs->directSaveFile($file);

            if ($fileResult['status'] === 0) {
                $fileId = $fileResult['group'].'/'.$fileResult['fileName'];
                UploadedFileState::create([
                    'file_id' => $fileId,
                    'state' => 0
                ]);

                return response()->json([
                    'status' => 0,
                    'code' => 0,
                    'msg' => '',
                    'fileKey' => $fileKey,
                    'fileId' => $fileId,
                    'data' => [
                        'src' => FastDFS::getFullUrl($fileId)
                    ]
                ]);
            } else {
                return response()->json([
                    'status' => $fileResult['status'],
                    'info' => $fileResult['info']
                ]);
            }
        }

        return response()->json([
            'status' => 1000,
            'info' => '文件为空'
        ]);
    }

    public function doBatchUpload(Request $request)
    {
        $fileEntrys = $request->all();

        $file = null;

        $originFileName = $fileEntrys['name'];

        if (!file_exists(storage_path().'/upload/')) {
            mkdir(storage_path().'/upload/');
        }

        $tempFilePath = storage_path().'/upload/'.$originFileName;

        @file_put_contents($tempFilePath, file_get_contents('php://input'));

        if (isset($fileEntrys['id'])) {
            $fdfs = new FastDFS();
            $fileResult = $fdfs->saveFile($tempFilePath);

            unlink($tempFilePath);

            $uniqueName = $fileEntrys['guid'];

            if ($fileResult['status'] === 0) {
                $fileId = $fileResult['group'].'/'.$fileResult['fileName'];
                UploadedFileState::create([
                    'file_id' => $fileId,
                    'state' => 0,
                    'batch_upload_origin_name' => $originFileName,
                    'guid' => $uniqueName
                ]);

                return response()->json([
                    'status' => 0,
                    'info' => $fileResult['info'],
                    'filePath' => FastDFS::getFullUrl($fileId)
                ]);
            } else {
                return response()->json([
                    'status' => $fileResult['status'],
                    'info' => $fileResult['info']
                ]);
            }
        }

        return response()->json([
            'status' => 1000,
            'info' => '文件为空'
        ]);
    }

    public function doThunkUpload(Request $request)
    {
        $fileEntrys = $request->all();
        $file = null;
        $fileKey = null;
        foreach ($fileEntrys as $key => $value) {
            $file = $value;
            $fileKey = $key;
        }

        $originFileName = $fileEntrys['name'];
        
        if (!file_exists(storage_path().'/upload/')) {
            mkdir(storage_path().'/upload/');
        }

        $randomDir = storage_path().'/upload/'.$fileEntrys['guid'].'/';
        if (!file_exists($randomDir)) {
            mkdir($randomDir);
            @file_put_contents($randomDir.'menifest.dat', $originFileName);
        }

        if (array_key_exists('chunk', $fileEntrys)) {
            $tempFilePath = $randomDir.$fileEntrys['chunk'];
        } else {
            $tempFilePath = $randomDir.'0';
        }

        @file_put_contents($tempFilePath, file_get_contents('php://input'));
    }
}
