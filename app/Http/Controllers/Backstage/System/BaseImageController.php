<?php

namespace App\Http\Controllers\Backstage\System;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\BaseImgService;
use App\Services\CityService;
use Illuminate\Validation\Rule;
use App\BaseImg;

class BaseImageController extends Controller
{
    private $baseImgService = null;
    private $cityService = null;

    public function __construct(BaseImgService $baseImgService,
        CityService $cityService)
    {
        $this->baseImgService = $baseImgService;
        $this->cityService = $cityService;
    }

    public function index()
    {
        return view('backstage.system.baseimg.baseimage', [
            'allcities' => $this->cityService->getAllCities()
        ]);
    }

    public function getImages(Request $request)
    {
        return $this->baseImgService
            ->getBaseImages($request->all());
    }

    public function create()
    {
        return view('backstage.system.baseimg.create_image', [
            'allcities' => $this->cityService->getAllCities()
        ]);
    }

    public function saveNew(Request $request)
    {
        $input = $request->all();

        $this->validateWhenSaveNew($input);

        if ($input['city_id'] == 0) {
            $input['city_id'] = null;
        }
        BaseImg::create($input);

        return response()->json([
            'Success' => true,
            'Message' => '新增图片成功',
        ]);
    }

    public function edit($imgId)
    {
        $image = BaseImg::findOrFail($imgId);
        return view('backstage.system.baseimg.create_image', [
            'allcities' => $this->cityService->getAllCities(),
            'baseImage' => $image
        ]);
    }

    public function update(Request $request)
    {
        $input = $request->all();

        $this->validateWhenSaveUpdate($input);

        $baseImg = BaseImg::findOrFail($input['id']);
        $baseImg->update([
            'id' => $input['id'],
            'picture' => $input['picture'],
            'img_type' => $input['img_type'],
            'city_id' => !empty($input['city_id']) ? $input['city_id'] : null,
            'link_url' => $input['link_url'],
            'sort_order' => $input['sort_order']
        ]);

        return response()->json([
            'Success' => true,
            'Message' => '保存修改运营图片成功',
        ]);
    }

    public function delete(Request $request)
    {
        $input = $request->all();

        $this->validateWhenDelete($input);

        $baseImg = BaseImg::findOrFail($input['id']);
        $baseImg->delete();

        return response()->json([
            'Success' => true,
            'Message' => '删除运营图片成功',
        ]);
    }

    private function validateWhenSaveNew(Array $input)
    {
        return Validator::make($input, [
            //基础性验证
            'picture' => 'required|max:200',
            'img_type' => [
                'required',
                Rule::in(BaseImg::getBaseImageTypeKeys())
            ],
            'city_id' => [
                'required',
                Rule::in($this->getIllegalCities())
            ],
            'link_url' => 'max:255',
            'sort_order' => 'required|numeric'
        ], $this->getValidateMessagesWhenSaveNew())->validate();
    }

    private function getValidateMessagesWhenSaveNew() {
        return [
            'picture.required' => '图片必须存在',
            'picture.max'  => '图片地址长度不可超过:max',
            'img_type.required' => '图片类型必须存在',
            'img_type.in' => '图片类型值不合法',
            'city_id.required'  => '城市ID必须存在，所有城市传入0',
            'city_id.in' => '城市ID值不合法',
            'link_url.max' => '图片链接长度不可超过:max',
            'sort_order.required' => '排序号必须存在',
            'sort_order.numeric' => '排序号必须是数字',
        ];
    }

    private function getIllegalCities()
    {
        $cityIds = $this->cityService->getAllCities()->pluck('id')->toArray();

        array_push($cityIds, 0);

        return $cityIds;
    }

    private function validateWhenSaveUpdate(Array $input)
    {
        return Validator::make($input, [
            //基础性验证
            'id' => 'required|exists:base_imgs,id',
            'picture' => 'required|max:200',
            'img_type' => [
                'required',
                Rule::in(BaseImg::getBaseImageTypeKeys())
            ],
            'city_id' => [
                'required',
                Rule::in($this->getIllegalCities())
            ],
            'link_url' => 'max:255',
            'sort_order' => 'required|numeric'
        ], $this->getValidateMessagesWhenSaveUpdate())->validate();
    }

    private function getValidateMessagesWhenSaveUpdate() {
        return [
            'id.required' => '必须传入图片ID',
            'id.exists' => '该图片ID参数有误，图片不存在',
            'picture.required' => '图片必须存在',
            'picture.max'  => '图片地址长度不可超过:max',
            'img_type.required' => '图片类型必须存在',
            'img_type.in' => '图片类型值不合法',
            'city_id.required'  => '城市ID必须存在，所有城市传入0',
            'city_id.in' => '城市ID值不合法',
            'link_url.max' => '图片链接长度不可超过:max',
            'sort_order.required' => '排序号必须存在',
            'sort_order.numeric' => '排序号必须是数字',
        ];
    }

    private function validateWhenDelete(Array $input)
    {
        return Validator::make($input, [
                'id' => 'required|exists:base_imgs,id'
            ], [
                'id.required' => '必须传入图片ID',
                'id.exists' => '该图片ID参数有误，图片不存在'
            ])->validate();
    }
}
