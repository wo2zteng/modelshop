<?php

namespace App\Http\Controllers\Backstage\Workspace;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Route;

class DashboardController extends Controller
{
    public function index()
    {
        return view('backstage.workspace.dashboard.dashboard');
    }
}
