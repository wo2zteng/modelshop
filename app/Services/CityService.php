<?php

namespace App\Services;

use App\City;
use App\District;

class CityService
{
    public function getProvinceCities($provinceId, $sort = true)
    {
        if ($sort) {
            return City::where('province_id', $provinceId)->orderBy('sort_order')->get();
        }
        return City::where('province_id', $provinceId)->get();
    }

    public function getCityDistricts($cityId, $sort = true)
    {
        if ($sort) {
            return District::where('city_id', $cityId)->orderBy('sort_order')->get();
        }
        return District::where('city_id', $cityId)->get();
    }

    public function getAllCities($sort = true)
    {
        if ($sort) {
            return City::orderBy('sort_order')->get();
        }
        return City::all();
    }

    public function getUserDefaultCityId($userId = null)
    {
        return config('constants.DEFAULT_CITY_ID');
    }
}
