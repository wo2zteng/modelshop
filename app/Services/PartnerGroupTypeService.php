<?php
namespace App\Services;

use App\PartnerGroupType;
use App\PartnerGroupTypePermission;
use App\UserGroup;
use App\Lib\Util\QueryPager;

class PartnerGroupTypeService
{
    public function getPartnerGroupTypes(Array $input, $paging = true)
    {
        $query = $this->baseQuery();

        if (!empty($input['groupType'])) {
            $query = $query->where('group_type', $input['groupType']);
        }

        $pager = new QueryPager($query);
        $pager->mapField('group_type', UserGroup::$GROUP_TYPE_MAP);

        return $paging ? $pager->doPaginate($input, 'sort_order') :
            $pager->queryWithoutPaginate($input, 'sort_order');
    }

    private function baseQuery()
    {
        return PartnerGroupType::select('id', 'name', 'group_type', 'sort_order');
    }

    public function hasConstrait($partnerGroupTypeId)
    {
        return UserGroup::where('partner_group_type_id', $partnerGroupTypeId)->count() > 0;
    }

    public function hasPermission($partnerGroupTypeId)
    {
        return PartnerGroupTypePermission::where('group_type_id', $partnerGroupTypeId)->count() > 0;
    }
}
