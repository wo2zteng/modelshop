<?php

namespace App\Services;

use App\City;
use App\Province;

class ProvinceService
{
    public function getAllProvince($sort = true)
    {
        if ($sort) {
            return Province::orderBy('sort_order')->get();
        }
        return Province::all();
    }
}
