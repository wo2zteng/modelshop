<?php
namespace App\Services;

use App\Route;
use App\MenuPermission;
use App\BaseDictionary;
use App\Lib\Util\QueryPager;

class RouteService
{
    public function createRoute($route)
    {
        return Route::create($route);
    }

    public function getAllWebRoutes()
    {
        return Route::select('method', 'code', 'route', 'action', 'slug')
            ->where('route_type', Route::$ROUTE_TYPE_WEB)
            ->orderBy('sort_order')
            ->get();
    }

    public function getAllApiRoutes()
    {
        return Route::select('method', 'code', 'route', 'action', 'slug')
            ->where('route_type', Route::$ROUTE_TYPE_API)
            ->orderBy('sort_order')
            ->get();
    }

    /**
    * 构建一个用于查询的基本query，这样以后所有需要查询的地方就不需要重写该query
    */
    private function baseQuery()
    {
        return Route::select('id', 'method', 'code', 'route', 'action', 'slug', 'route_type', 'name', 'sort_order');
    }

    public function getRoutes(Array $input, $paging = true)
    {
        $query = $this->baseQuery();

        if (!empty($input['routePath'])) {
            $query = $query->where('route', 'like' , '%'.$input['routePath'].'%');
        }

        if (!empty($input['routeAction'])) {
            $query = $query->where('action', 'like' , '%'.$input['routeAction'].'%');
        }

        if (!empty($input['routeName'])) {
            $query = $query->where('slug', 'like' , '%'.$input['routeName'].'%');
        }

        if (!empty($input['routeType'])) {
            $query = $query->where('route_type', $input['routeType']);
        }

        if (!empty($input['routeMethod'])) {
            $query = $query->where('method', $input['routeMethod']);
        }

        $pager = new QueryPager($query);

        //进行字典映射
        //数据库的字段有两种映射，一种是字典映射，一种是relation映射
        //字典映射一般是没有对应的外键关联表，只有定义好的字典，通过QueryPager的mapField方法传入字段名和字典
        //字典映射完成后，会生成一个key_text的字段，作为字典映射对应的值列
        //relation映射，采用Eloquent的manytoone方式映射对象
        //然后在客户端的表单里直接用object.field方式取值
        $pager->mapField('method', Route::$ROUTE_METHOD_MAP);
        $pager->mapField('route_type', Route::$ROUTE_TYPE_MAP);

        return $paging ? $pager->doPaginate($input, 'sort_order') :
            $pager->queryWithoutPaginate($input, 'sort_order');
    }

    public function hasConstrait($routeId)
    {
        return MenuPermission::where('route_id', $routeId)->count() > 0;
    }

}
