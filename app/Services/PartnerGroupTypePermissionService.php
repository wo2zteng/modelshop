<?php
namespace App\Services;

use App\PartnerGroupTypePermission;
use DB;

class PartnerGroupTypePermissionService
{
    public function getPermissionByPartnerGroupType($groupTypeId)
    {
        return PartnerGroupTypePermission::where('group_type_id', $groupTypeId)->get();
    }

    public function clearOriginalPermissionsByType($groupTypeId)
    {
        PartnerGroupTypePermission::where('group_type_id', $groupTypeId)->delete();
    }

    public function setPartnerGroupTypePermissions($groupTypeId, $permissionIds)
    {
        DB::transaction(function () use ($groupTypeId, $permissionIds) {
            $this->clearOriginalPermissionsByType($groupTypeId);

            foreach ($permissionIds as $permissionId) {
                if (!empty($permissionId)) {
                    PartnerGroupTypePermission::create([
                        'group_type_id' => $groupTypeId,
                        'permission_id' => $permissionId
                    ]);
                }
            }
        });
    }
}
