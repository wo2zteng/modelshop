<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Lib\Core\RouteSerializer;

class Route extends Model
{
    public static $ROUTE_METHOD_ANY = 1;
    public static $ROUTE_METHOD_GET = 2;
    public static $ROUTE_METHOD_POST = 3;
    public static $ROUTE_METHOD_PUT = 4;
    public static $ROUTE_METHOD_DELETE = 5;
    public static $ROUTE_METHOD_PATCH = 6;
    public static $ROUTE_METHOD_OPTIONS = 7;

    public static $ROUTE_METHOD_MAP = [
        ['key' => 1, 'text' => 'any'],
        ['key' => 2, 'text' => 'get'],
        ['key' => 3, 'text' => 'post'],
        ['key' => 4, 'text' => 'put'],
        ['key' => 5, 'text' => 'delete'],
        ['key' => 6, 'text' => 'patch'],
        ['key' => 7, 'text' => 'options'],
    ];

    public static $ROUTE_TYPE_WEB = 1;
    public static $ROUTE_TYPE_API = 2;

    public static $ROUTE_TYPE_MAP = [
        ['key' => 1, 'text' => 'WEB路由'],
        ['key' => 2, 'text' => 'API路由'],
    ];

    public $timestamps = false;

    protected $fillable = [
        'name', 'code', 'route', 'action', 'slug', 'route_type', 'method', 'sort_order'
    ];

    protected static function boot()
    {
        Route::saved (function ($route) {
            $serializer = new RouteSerializer();
            $serializer->generateRoute();
        });
        Route::deleted (function ($route) {
            $serializer = new RouteSerializer();
            $serializer->generateRoute();
        });
    }

    public static function getRouteMethodKeys()
    {
        return collect(self::$ROUTE_METHOD_MAP)->pluck('key')->toArray();
    }

    public static function getRouteTypeKeys()
    {
        return collect(self::$ROUTE_TYPE_MAP)->pluck('key')->toArray();
    }
}
