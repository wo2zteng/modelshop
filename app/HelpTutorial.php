<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HelpTutorial extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'title', 'content', 'sort_order', 'parent_id'
    ];

    public function parent()
    {
        return $this->belongsTo('App\HelpTutorial', 'parent_id');
    }

    public function fullPath()
    {
        if (isset($this->parent)) {
            return $this->parent->fullPath().'>>'.$this->title;
        }
        return $this->title;
    }
}
