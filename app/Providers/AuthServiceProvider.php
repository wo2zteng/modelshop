<?php

namespace App\Providers;

use App\Lib\Core\RouteBuilder;
use App\Lib\Auth\UserProfileHelper;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        $routes = RouteBuilder::buildWebRoutes();

        foreach ($routes as $route) {
            $permissionCode = $route['code'];
            Gate::define($permissionCode, function($user) use($permissionCode) {
                if ($user->isSuper()){
                    return true;
                }

                $routes = UserProfileHelper::routes();
                return in_array($permissionCode, $routes);
            });
        }
    }
}
