<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPlatformOpenid extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'user_id', 'platform_id', 'openid', 'avatar', 'nickname'
    ];
}
