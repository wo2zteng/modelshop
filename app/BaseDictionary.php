<?php

namespace App;

class BaseDictionary
{
    public static $KEY_NO = 0;
    public static $KEY_YES = 1;

    public static $YES_NO_MAP = [
        ['key' => 0, 'text' => '否'],
        ['key' => 1, 'text' => '是'],
    ];
}
