<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelCategory extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name', 'sort_order', 'parent_id'
    ];

    public function parent()
    {
        return $this->belongsTo('App\ModelCategory', 'parent_id');
    }

    public function getFullPath()
    {
        if (isset($this->parent)) {
            return $this->parent->getFullPath().'>>'.$this->name;
        } else {
            return $this->name;
        }
    }

    private $cates = [];
    
    public function treeCates()
    {
        $this->parentCate($this);

        return array_reverse($this->cates);
    }

    private function parentCate($category)
    {
        $this->cates[] = $category;
        if (isset($category->parent)) {
            $this->parentCate($category->parent);
        }
    }
}
