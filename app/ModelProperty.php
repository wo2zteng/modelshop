<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelProperty extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'model_id', 'sequence', 'sort_order', 'name', 'value'
    ];

}
