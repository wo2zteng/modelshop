<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Lib\FileService\FastDFS;

class UserGroup extends Model
{
    public static $GROUP_TYPE_EXTERNAL_ENTERPRISE = 1;
    public static $GROUP_TYPE_INTERNAL_ORGANIZATION = 2;
    public static $GROUP_TYPE_INTERNAL_DEPARTMENT = 3;
    public static $GROUP_TYPE_USER = 4;

    public static $GROUP_TYPE_MAP = [
        ['key' => 1, 'text' => '合作伙伴单位'],
        ['key' => 2, 'text' => '内部组织机构'],
        ['key' => 3, 'text' => '内部部门'],
        ['key' => 4, 'text' => '个人'],
    ];

    public static $GROUP_STATUS_INACTIVE = 1;
    public static $GROUP_STATUS_ACTIVE = 2;
    public static $GROUP_STATUS_BANNED = 3;

    public static $GROUP_STATUS_MAP = [
        ['key' => 1, 'text' => '未生效'],
        ['key' => 2, 'text' => '已生效'],
        ['key' => 3, 'text' => '已禁用'],
    ];

    public $timestamps = false;

    protected $fillable = [
        'name', 'code', 'business_code', 'group_type', 'active_status',
        'parent_id', 'sort_order', 'manager_id', 'partner_group_type_id',
        'province_id', 'city_id', 'district_id', 'detailed_address', 'icon'
    ];

    protected static function boot()
    {
        UserGroup::saving (function ($userGroup) {
            $state = UploadedFileState::where('file_id', $userGroup['icon'])->first();
            if (isset($state)) {
                $state->state = 1;
                $state->save();
            }
        });
    }

    public function getGroupPath()
    {
        if (isset($this->parent)) {
            return $this->parent->getGroupPath().'>>'.$this->name;
        } else {
            return $this->name;
        }
    }

    public function managedUsers()
    {
        return $this->belongsToMany('App\User', 'group_users', 'group_id', 'user_id');
    }

    public function getFullIconPath()
    {
        if (!empty($this->icon)) {
            return FastDFS::getFullUrl($this->icon);
        }
        return '/images/no-pic-back.png';
    }

    public function manager()
    {
        return $this->belongsTo('App\User', 'manager_id');
    }

    public function parent()
    {
        return $this->belongsTo('App\UserGroup', 'parent_id');
    }

    public function partnerGroupType()
    {
        return $this->belongsTo('App\PartnerGroupType', 'partner_group_type_id');
    }

    public function province()
    {
        return $this->belongsTo('App\Province', 'province_id');
    }

    public function city()
    {
        return $this->belongsTo('App\City', 'city_id');
    }

    public function district()
    {
        return $this->belongsTo('App\District', 'district_id');
    }

    public function getFullAddress()
    {
        if (!empty($this->province_id) && !empty($this->city_id) && !empty($this->district_id)) {
            return $this->province->full_name
                .$this->city->full_name
                .$this->district->full_name
                .$this->detailed_address;
        }

        return $this->detailed_address;
    }

    public function coreOrganization()
    {
        if (isset($this->parent)) {
            return $this->parent->coreOrganization();
        } else {
            return $this;
        }
    }

    public function isActive()
    {
        return $this->active_status === self::$GROUP_STATUS_ACTIVE;
    }

    public function isExternalEnterprise()
    {
        return $this->group_type === self::$GROUP_TYPE_EXTERNAL_ENTERPRISE;
    }

    public function isInternalOrganization()
    {
        return $this->group_type === self::$GROUP_TYPE_INTERNAL_ORGANIZATION;
    }

    public function isInternalDepartment()
    {
        return $this->group_type === self::$GROUP_TYPE_INTERNAL_DEPARTMENT;
    }

    public function isSimpleUserGroup()
    {
        return $this->group_type === self::$GROUP_TYPE_USER;
    }
}
