<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuPermission extends Model
{
    public static $PERMISSION_TYPE_TOP_NAVI = 1;
    public static $PERMISSION_TYPE_MENU_GROUP = 2;
    public static $PERMISSION_TYPE_MENU = 3;
    public static $PERMISSION_TYPE_OPERATION = 4;
    public static $PERMISSION_TYPE_API = 5;

    public static $PERMISSION_TYPE_MAP = [
        ['key' => 1, 'text' => '顶部导航模块'],
        ['key' => 2, 'text' => '菜单组'],
        ['key' => 3, 'text' => '菜单'],
        ['key' => 4, 'text' => '功能权限'],
        ['key' => 5, 'text' => 'api'],
    ];

    public static $IS_DEFAULT_MAP = [
        ['key' => 0, 'text' => '不默认'],
        ['key' => 1, 'text' => '默认'],
    ];

    public $timestamps = false;

    protected $fillable = [
        'name', 'icon', 'permission_type', 'parent_id', 'sort_order', 'route_id', 'is_default'
    ];

    protected static function boot()
    {
        MenuPermission::saving (function ($menuPermission) {
            $menuPermission['icon'] = htmlspecialchars($menuPermission['icon']);
        });
    }

    public function parent()
    {
        return $this->belongsTo('App\MenuPermission', 'parent_id');
    }

    public function route()
    {
        return $this->belongsTo('App\Route', 'route_id');
    }

    public function fullPath()
    {
        if (isset($this->parent)) {
            return $this->parent->fullPath().'>>'.$this->name;
        }
        return $this->name;
    }
}
