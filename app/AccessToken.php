<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccessToken extends Model
{
	protected $primaryKey = 'user_id';

	public static $CLIENT_TYPE_IOS = 1;
    public static $CLIENT_TYPE_ANDROID = 2;
    public static $CLIENT_TYPE_WEB = 3;

	public static $CLIENT_TYPE_MAP = [
        ['key' => 1, 'text' => 'IOS'],
        ['key' => 2, 'text' => 'Android'],
        ['key' => 3, 'text' => 'Web'],
    ];

	public static $APP_TYPE_DEFAULT = 1;

	public static $APP_TYPE_MAP = [
        ['key' => 1, 'text' => '默认程序'],
    ];

	protected $fillable = [
		'user_id', 'app_id', 'client_type', 'device_ip_addr', 'access_token', 'last_updated_time'
    ];
}
