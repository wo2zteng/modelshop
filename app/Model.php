<?php

namespace App;

use Illuminate\Database\Eloquent\Model as EloquentModel;
use App\Lib\FileService\FastDFS;
use App\UploadedFileState;

class Model extends EloquentModel
{
    public $timestamps = false;

    protected $fillable = [
        'name', 'description', 'main_image', 'broadcast_images', 'line_images', 'author_id', 
        'owner_id', 'class_id', 'sort_order', 'price', 'file_guid', 'file_name', 'md5_name', 
        'visit_count', 'vote_count', 'is_new', 'is_recommand', 'is_recommand_materials', 'is_new_order', 'is_recommand_order', 'is_recommand_materials_order', 'download_count', 'upload_date'
    ];

    protected static function boot()
    {
        Model::saving (function ($model) {
            $state = UploadedFileState::where('file_id', $model['main_image'])->first();
            if (isset($state)) {
                $state->state = 1;
                $state->save();
            }
        });
    }

    public function getFullImage()
    {
        if (!empty($this->main_image)) {
            return FastDFS::getFullUrl($this->main_image);
        }
        return '/images/no-pic-back.png';
    }

    public function getFullImageThumb()
    {
        if (!empty($this->main_image)) {
            if (strpos($this->main_image, 'jpg')) {
                return FastDFS::getFullUrl($this->main_image);//.'!t280x280.jpg';
            } else if (strpos($this->main_image, 'jpeg')) {
                return FastDFS::getFullUrl($this->main_image);//.'!t280x280.jpeg';
            }
            return FastDFS::getFullUrl($this->main_image).'!t280x280.png';
        }
        return '/images/no-pic-back.png';
    }

    public function author()
    {
        return $this->belongsTo('App\User', 'author_id');
    }

    public function owner()
    {
        return $this->belongsTo('App\UserGroup', 'owner_id');
    }

    public function modelClass()
    {
        return $this->belongsTo('App\ModelClass', 'class_id');
    }

    private $cates = [];

    public function modelCates()
    {
        $leafCate = $this->modelClass->category;

        $this->parentCate($leafCate);

        return array_reverse($this->cates);
    }

    private function parentCate($category)
    {
        $this->cates[] = $category;
        if (isset($category->parent)) {
            $this->parentCate($category->parent);
        }
    }

    public function classPath()
    {
        return $this->modelClass->getFullPath();
    }

    public function properties()
    {
        return $this->hasMany('App\ModelProperty', 'model_id', 'id')->orderBy('sort_order');
    }

    public function getOwnerIconPath()
    {
        if (!empty($this->owner->icon)) {
            return FastDFS::getFullUrl($this->owner->icon);
        }
        return '/res/images/noavatar_small.gif';
    }

    public function getFullBroadCastImages()
    {
        $result = [];

        if (!empty($this->broadcast_images)) {
            $images = explode(',', $this->broadcast_images);

            foreach ($images as $image) {
                array_push($result, FastDFS::getFullUrl($image));
            }
        }

        return $result;
    }

    public function shortTitle()
    {
        if (mb_strlen($this->name) > 15) {
            return mb_substr($this->name, 0, 15).'...';
        }

        return $this->name;
    }

    public function getFullLineImages()
    {
        $result = [];

        if (!empty($this->line_images)) {
            $images =  explode(',', $this->line_images);

            foreach ($images as $image) {
                array_push($result, FastDFS::getFullUrl($image));
            }
        }

        return $result;
    }
}
