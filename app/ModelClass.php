<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelClass extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'code', 'name', 'sort_order', 'category_id'
    ];

    public function category()
    {
        return $this->belongsTo('App\ModelCategory', 'category_id');
    }

    public function getFullPath()
    {
        if (isset($this->category_id)) {
            return $this->category->getFullPath().'>>'.$this->name;
        }
        return '[未设置分类]>>'.$this->name;
    }

    public function properties()
    {
        return $this->hasMany('App\ModelClassProperty', 'class_id', 'id')->orderBy('sort_order');
    }
}
