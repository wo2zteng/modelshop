<?php
namespace App\Lib\WeChat;

class WechatConstants
{
    public static $MENU_ACTION_TYPE_TEXT = 1;
    public static $MENU_ACTION_TYPE_EXEC = 2;
}
