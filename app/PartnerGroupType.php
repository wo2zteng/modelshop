<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class PartnerGroupType extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name', 'sort_order', 'group_type'
    ];
}
