<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelClassProperty extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name', 'value', 'sort_order', 'class_id'
    ];

    public function modelClass()
    {
        return $this->belongsTo('App\ModelClass', 'class_id');
    }
}
