<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class TempUser extends Model
{
    protected $primaryKey = 'mobile';
    
    public $timestamps = false;

    protected $fillable = [
        'name', 'nick_name', 'email', 'password', 'mobile', 'mobile_verify_code', 'mobile_verify_begin'
    ];
}
