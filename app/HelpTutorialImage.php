<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Lib\FileService\FastDFS;

class HelpTutorialImage extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'tutorial_id', 'file_id'
    ];

    public function getFullImagePath()
    {
        if (!empty($this->file_id)) {
            return FastDFS::getFullUrl($this->file_id);
        }
        return '/images/no-pic-back.png';
    }
}
