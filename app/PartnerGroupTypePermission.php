<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class PartnerGroupTypePermission extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'group_type_id', 'permission_id'
    ];
}
