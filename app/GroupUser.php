<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupUser extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'user_id', 'group_id', 'is_primary_group'
    ];
}
