<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

$routes = \App\Lib\Core\RouteBuilder::buildApiRoutes();
foreach ($routes as $route) {
    if ($route['method'] === \App\Route::$ROUTE_METHOD_ANY) {
        if (!empty($route['slug'])) {
            Route::any($route['route'], $route['action'])->middleware('can:'.$route['code']);
        } else {
            Route::any($route['route'], $route['action'])->name($route['slug'])->middleware('can:'.$route['code']);
        }
    } else if ($route['method'] === \App\Route::$ROUTE_METHOD_GET) {
        if (!empty($route['slug'])) {
            Route::get($route['route'], $route['action'])->middleware('can:'.$route['code']);
        } else {
            Route::get($route['route'], $route['action'])->name($route['slug'])->middleware('can:'.$route['code']);
        }
    } else if ($route['method'] === \App\Route::$ROUTE_METHOD_POST) {
        if (!empty($route['slug'])) {
            Route::post($route['route'], $route['action'])->middleware('can:'.$route['code']);
        } else {
            Route::post($route['route'], $route['action'])->name($route['slug'])->middleware('can:'.$route['code']);
        }
    } else if ($route['method'] === \App\Route::$ROUTE_METHOD_PUT) {
        if (!empty($route['slug'])) {
            Route::put($route['route'], $route['action'])->middleware('can:'.$route['code']);
        } else {
            Route::put($route['route'], $route['action'])->name($route['slug'])->middleware('can:'.$route['code']);
        }
    } else if ($route['method'] === \App\Route::$ROUTE_METHOD_DELETE) {
        if (!empty($route['slug'])) {
            Route::delete($route['route'], $route['action'])->middleware('can:'.$route['code']);
        } else {
            Route::delete($route['route'], $route['action'])->name($route['slug'])->middleware('can:'.$route['code']);
        }
    } else if ($route['method'] === \App\Route::$ROUTE_METHOD_PATCH) {
        if (!empty($route['slug'])) {
            Route::patch($route['route'], $route['action'])->middleware('can:'.$route['code']);
        } else {
            Route::patch($route['route'], $route['action'])->name($route['slug'])->middleware('can:'.$route['code']);
        }
    } else if ($route['method'] === \App\Route::$ROUTE_METHOD_OPTIONS) {
        if (!empty($route['slug'])) {
            Route::options($route['route'], $route['action'])->middleware('can:'.$route['code']);
        } else {
            Route::options($route['route'], $route['action'])->name($route['slug'])->middleware('can:'.$route['code']);
        }
    }
}
