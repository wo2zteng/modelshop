<?php

/** ==== Start Extras ==== */
if (config('sandbox.enable_verify_code_sandbox')) {
    Route::get('/extra/sandbox/verifycode-testor', 'Extra\SandBox\MobileSandBoxController@index');

    Route::post('/extra/sandbox/getverifycode', 'Extra\SandBox\MobileSandBoxController@getVerifyCode');
}

Route::get('/publish/info', 'Extra\Publish\PublicInfoController@index');

Route::get('/publish/tutorial/gettree', 'Extra\Publish\PublicInfoController@getInitTutorialTrees');

Route::get('/publish/tutorial/{tutorialId}', 'Extra\Publish\PublicInfoController@getTutorialInfo');

Route::get('/publish/tutorial/getdetail/getimages', 'Extra\Publish\PublicInfoController@getTutorialImages');
/** ==== End Extras ==== */
