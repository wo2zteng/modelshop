<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/index.htm');
});

Auth::routes();

/** common **/

Route::get('/home', 'HomeController@index');

Route::get('/changeNaviModule/{moduleId}', 'HomeController@changeNaviModule');

Route::get('/toRenderLoginForm', 'Auth\LoginController@toRenderLoginForm');

Route::get('/backstage/api/province/getall', 'Backstage\System\AddressController@getProvinces')->name('province.queryapi.getall');

Route::get('/backstage/api/city/getbyprovince/{provinceId}', 'Backstage\System\AddressController@getCitiesByProvince')->name('city.queryapi.getbyprovince');

Route::get('/backstage/api/district/getbycity/{cityId}', 'Backstage\System\AddressController@getDistrictsByCity')->name('district.queryapi.getbycity');

Route::post('/backstage/api/upload', 'Backstage\System\BaseUploadController@doUpload')->name('backstage.uploader.api');

Route::post('/backstage/api/richeditor/upload', 'Backstage\System\BaseUploadController@doRichEditorUpload')->name('backstage.richeditor.uploader.api');

Route::post('/backstage/api/batchupload', 'Backstage\System\BaseUploadController@doBatchUpload')->name('backstage.batch.uploader.api');

Route::post('/backstage/api/thunkupload', 'Backstage\System\BaseUploadController@doThunkUpload')->name('backstage.thunk.uploader.api');

Route::get('/backstage/common/getcityselector', 'Backstage\Common\SelectorController@getCitySelector')->name('common.city.getselector');

/** end common **/

$routes = \App\Lib\Core\RouteBuilder::buildWebRoutes();
foreach ($routes as $route) {
    if ($route['method'] === \App\Route::$ROUTE_METHOD_ANY) {
        if (!empty($route['slug'])) {
            Route::any($route['route'], $route['action'])->middleware('can:'.$route['code']);
        } else {
            Route::any($route['route'], $route['action'])->name($route['slug'])->middleware('can:'.$route['code']);
        }
    } else if ($route['method'] === \App\Route::$ROUTE_METHOD_GET) {
        if (!empty($route['slug'])) {
            Route::get($route['route'], $route['action'])->middleware('can:'.$route['code']);
        } else {
            Route::get($route['route'], $route['action'])->name($route['slug'])->middleware('can:'.$route['code']);
        }
    } else if ($route['method'] === \App\Route::$ROUTE_METHOD_POST) {
        if (!empty($route['slug'])) {
            Route::post($route['route'], $route['action'])->middleware('can:'.$route['code']);
        } else {
            Route::post($route['route'], $route['action'])->name($route['slug'])->middleware('can:'.$route['code']);
        }
    } else if ($route['method'] === \App\Route::$ROUTE_METHOD_PUT) {
        if (!empty($route['slug'])) {
            Route::put($route['route'], $route['action'])->middleware('can:'.$route['code']);
        } else {
            Route::put($route['route'], $route['action'])->name($route['slug'])->middleware('can:'.$route['code']);
        }
    } else if ($route['method'] === \App\Route::$ROUTE_METHOD_DELETE) {
        if (!empty($route['slug'])) {
            Route::delete($route['route'], $route['action'])->middleware('can:'.$route['code']);
        } else {
            Route::delete($route['route'], $route['action'])->name($route['slug'])->middleware('can:'.$route['code']);
        }
    } else if ($route['method'] === \App\Route::$ROUTE_METHOD_PATCH) {
        if (!empty($route['slug'])) {
            Route::patch($route['route'], $route['action'])->middleware('can:'.$route['code']);
        } else {
            Route::patch($route['route'], $route['action'])->name($route['slug'])->middleware('can:'.$route['code']);
        }
    } else if ($route['method'] === \App\Route::$ROUTE_METHOD_OPTIONS) {
        if (!empty($route['slug'])) {
            Route::options($route['route'], $route['action'])->middleware('can:'.$route['code']);
        } else {
            Route::options($route['route'], $route['action'])->name($route['slug'])->middleware('can:'.$route['code']);
        }
    }
}
