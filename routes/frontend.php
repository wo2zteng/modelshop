<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/index.htm', 'Frontend\WelcomeController@index');

Route::get('/model/{modelId}.htm', 'Frontend\ModelController@index');

Route::get('/model/query/{categoryId}.htm', 'Frontend\ModelController@query');

Route::get('/getcates/{categoryId}', 'Frontend\ModelController@getChildCategories');

Route::get('/getclasses/{categoryId}', 'Frontend\ModelController@getModelClass');

Route::get('/getclassprops/{classId}', 'Frontend\ModelController@getClassProps');

Route::post('/getmodels/query', 'Frontend\ModelController@queryResult');