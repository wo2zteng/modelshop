<?php

use Illuminate\Database\Seeder;
use App\MenuPermission;
use App\Route;
use App\BaseDictionary;
use App\Services\MenuPermissionService;
use App\Services\RouteService;

class DefaultModuleSeeder extends Seeder
{
    private $menuPermissionService = null;
    private $routeService = null;

    private $provinceWebApiRoute = null;
    private $cityWebApiRoute = null;
    private $districtWebApiRoute = null;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->menuPermissionService = new MenuPermissionService();
        $this->routeService = new RouteService();

        $baseModule = $this->menuPermissionService->createMenuPermission([
            'name' => '基础运营',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_TOP_NAVI,
            'parent_id' => null,
            'sort_order' => 0,
        ]);

        $this->createWorkspace($baseModule);
        $this->createSystemManage($baseModule);
    }

    private function createWorkspace($baseModule)
    {
        $workspace = $this->menuPermissionService->createMenuPermission([
            'name' => '我的空间',
            'icon' => '&#xe612;',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU_GROUP,
            'parent_id' => $baseModule->id,
            'sort_order' => 0,
        ]);

        $dashboard = $this->menuPermissionService->createMenuPermission([
            'name' => '仪表板',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id' => $this->routeService->createRoute([
                'name' => '仪表板主页面',
                'code' => 'perm_dashbord',
                'route' => '/backstage/dashboard',
                'action' => 'Backstage\Workspace\DashboardController@index',
                'slug' => 'route.workspace.dashboard',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 0,
            ])->id,
            'parent_id' => $workspace->id,
            'is_default' => BaseDictionary::$KEY_YES,
            'sort_order' => 0,
        ]);

        $todos = $this->menuPermissionService->createMenuPermission([
            'name' => '待办事宜',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id' => $this->routeService->createRoute([
                'name' => '待办事宜主页面',
                'code' => 'perm_todo_things',
                'route' => '/home1',
                'action' => 'HomeController@index',
                'slug' => 'route.workspace.todo',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 1,
            ])->id,
            'parent_id' => $workspace->id,
            'sort_order' => 1,
        ]);
    }

    private function createSystemManage($baseModule)
    {
        $systemManage = $this->menuPermissionService->createMenuPermission([
            'name' => '系统管理',
            'icon' => '&#xe614;',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU_GROUP,
            'parent_id' => $baseModule->id,
            'sort_order' => 1,
        ]);

        $routeManage = $this->menuPermissionService->createMenuPermission([
            'name' => '路由管理',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id' => $this->routeService->createRoute([
                'name' => '路由管理主页面',
                'code' => 'perm_route_manage',
                'route' => '/backstage/route',
                'action' => 'Backstage\System\RouteController@index',
                'slug' => 'route.manage',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 0,
            ])->id,
            'parent_id' => $systemManage->id,
            'sort_order' => 0,
        ]);

        $routeCreate = $this->menuPermissionService->createMenuPermission([
            'name' => '进入新增路由页面',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '新增路由',
                'code' => 'perm_route_create_new',
                'route' => '/backstage/route/create',
                'action' => 'Backstage\System\RouteController@create',
                'slug' => 'route.manage.createnew',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 1,
            ])->id,
            'parent_id' => $routeManage->id,
            'sort_order' => 1,
        ]);

        $routeCreateSave = $this->menuPermissionService->createMenuPermission([
            'name' => '保存新增',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '新增路由保存',
                'code' => 'perm_route_save_new',
                'route' => '/backstage/api/route/savenew',
                'action' => 'Backstage\System\RouteController@saveNew',
                'slug' => 'route.manage.savenew',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 6,
            ])->id,
            'parent_id' => $routeManage->id,
            'sort_order' => 0,
        ]);

        $routeEdit = $this->menuPermissionService->createMenuPermission([
            'name' => '进入修改路由页面',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '修改路由',
                'code' => 'perm_route_edit',
                'route' => '/backstage/route/edit/{routeId}',
                'action' => 'Backstage\System\RouteController@edit',
                'slug' => 'route.manage.edit',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 7,
            ])->id,
            'parent_id' => $routeManage->id,
            'sort_order' => 0,
        ]);

        $routeEditSave = $this->menuPermissionService->createMenuPermission([
            'name' => '保存修改',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '修改路由保存',
                'code' => 'perm_route_save_update',
                'route' => '/backstage/api/route/update',
                'action' => 'Backstage\System\RouteController@update',
                'slug' => 'route.manage.saveupdate',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 8,
            ])->id,
            'parent_id' => $routeManage->id,
            'sort_order' => 0,
        ]);

        $routeDelete = $this->menuPermissionService->createMenuPermission([
            'name' => '删除路由',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '删除路由',
                'code' => 'perm_route_delete',
                'route' => '/backstage/api/route/delete',
                'action' => 'Backstage\System\RouteController@delete',
                'slug' => 'route.manage.delete',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 9,
            ])->id,
            'parent_id' => $routeManage->id,
            'sort_order' => 0,
        ]);

        $routeQuery = $this->menuPermissionService->createMenuPermission([
            'name' => '查询路由',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '查询路由',
                'code' => 'perm_route_query',
                'route' => '/backstage/api/route/query',
                'action' => 'Backstage\System\RouteController@getRoutes',
                'slug' => 'route.manage.query',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 5,
            ])->id,
            'parent_id' => $routeManage->id,
            'sort_order' => 0,
        ]);

        $naviPermissionManage = $this->menuPermissionService->createMenuPermission([
            'name' => '导航权限管理',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id' => $this->routeService->createRoute([
                'name' => '导航权限管理主页面',
                'code' => 'perm_navi_permission_manage',
                'route' => '/backstage/navi-permission',
                'action' => 'Backstage\System\PermissionController@indexNavi',
                'slug' => 'permission.navi.manage',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 10,
            ])->id,
            'parent_id' => $systemManage->id,
            'sort_order' => 1,
        ]);

        $naviPermissionQuery = $this->menuPermissionService->createMenuPermission([
            'name' => '导航权限查询',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '导航权限查询路由',
                'code' => 'perm_navi_permission_query',
                'route' => '/backstage/api/navi-permission/query',
                'action' => 'Backstage\System\PermissionController@queryNavi',
                'slug' => 'permission.navi.query',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 11,
            ])->id,
            'parent_id' => $naviPermissionManage->id,
            'sort_order' => 1,
        ]);

        $naviPermissionCreate = $this->menuPermissionService->createMenuPermission([
            'name' => '进入新增导航权限页面',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '导航权限新增主页面',
                'code' => 'perm_navi_permission_create',
                'route' => '/backstage/navi-permission/create',
                'action' => 'Backstage\System\PermissionController@createNavi',
                'slug' => 'permission.navi.manage.createnew',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 12,
            ])->id,
            'parent_id' => $naviPermissionManage->id,
            'sort_order' => 1,
        ]);

        $naviPermissionCreateSave = $this->menuPermissionService->createMenuPermission([
            'name' => '保存新增',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '新增导航权限保存',
                'code' => 'perm_navi_permission_save_new',
                'route' => '/backstage/api/navi-permission/savenew',
                'action' => 'Backstage\System\PermissionController@saveNavi',
                'slug' => 'permission.navi.manage.savenew',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 13,
            ])->id,
            'parent_id' => $naviPermissionManage->id,
            'sort_order' => 1,
        ]);

        $naviPermissionUpdate = $this->menuPermissionService->createMenuPermission([
            'name' => '进入修改导航权限页面',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '导航权限修改主页面',
                'code' => 'perm_navi_permission_edit',
                'route' => '/backstage/navi-permission/edit/{permissionId}',
                'action' => 'Backstage\System\PermissionController@editNavi',
                'slug' => 'permission.navi.manage.edit',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 14,
            ])->id,
            'parent_id' => $naviPermissionManage->id,
            'sort_order' => 1,
        ]);

        $naviPermissionUpdateSave = $this->menuPermissionService->createMenuPermission([
            'name' => '保存修改',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '修改导航权限保存',
                'code' => 'perm_navi_permission_save_update',
                'route' => '/backstage/api/navi-permission/update',
                'action' => 'Backstage\System\PermissionController@updateNavi',
                'slug' => 'permission.navi.manage.saveupdate',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 15,
            ])->id,
            'parent_id' => $naviPermissionManage->id,
            'sort_order' => 1,
        ]);

        $naviPermissionDelete = $this->menuPermissionService->createMenuPermission([
            'name' => '删除导航',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '删除导航权限',
                'code' => 'perm_navi_permission_delete',
                'route' => '/backstage/api/navi-permission/delete',
                'action' => 'Backstage\System\PermissionController@deleteNavi',
                'slug' => 'permission.navi.manage.delete',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 15,
            ])->id,
            'parent_id' => $naviPermissionManage->id,
            'sort_order' => 1,
        ]);

        $permissionManage = $this->menuPermissionService->createMenuPermission([
            'name' => '菜单权限管理',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id' => $this->routeService->createRoute([
                'name' => '菜单权限管理主页面',
                'code' => 'perm_permission_manage',
                'route' => '/backstage/permission',
                'action' => 'Backstage\System\PermissionController@index',
                'slug' => 'permission.manage',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 1,
            ])->id,
            'parent_id' => $systemManage->id,
            'sort_order' => 1,
        ]);

        $permissionGetTree = $this->menuPermissionService->createMenuPermission([
            'name' => '获取菜单权限右侧树',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '获取菜单权限右侧树路由',
                'code' => 'perm_permission_get_tree',
                'route' => '/backstage/api/permission/gettree',
                'action' => 'Backstage\System\PermissionController@getInitTrees',
                'slug' => 'permission.manage.gettree',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 6,
            ])->id,
            'parent_id' => $permissionManage->id,
            'sort_order' => 1,
        ]);

        $permissionCreate = $this->menuPermissionService->createMenuPermission([
            'name' => '进入新增权限界面',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '新增菜单权限',
                'code' => 'perm_permission_create_new',
                'route' => '/backstage/permission/create/{parentId}',
                'action' => 'Backstage\System\PermissionController@create',
                'slug' => 'permission.manage.createnew',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 6,
            ])->id,
            'parent_id' => $permissionManage->id,
            'sort_order' => 1,
        ]);

        $permissionCreateSave = $this->menuPermissionService->createMenuPermission([
            'name' => '新增权限管理保存',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '新增菜单权限保存',
                'code' => 'perm_permission_save_new',
                'route' => '/backstage/api/permission/savenew',
                'action' => 'Backstage\System\PermissionController@saveNew',
                'slug' => 'permission.manage.savenew',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 7,
            ])->id,
            'parent_id' => $permissionManage->id,
            'sort_order' => 1,
        ]);

        $permissionUpdate = $this->menuPermissionService->createMenuPermission([
            'name' => '进入修改菜单权限管理界面',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '进入修改菜单权限页面',
                'code' => 'perm_permission_edit',
                'route' => '/backstage/api/permission/edit/{permissionId}',
                'action' => 'Backstage\System\PermissionController@edit',
                'slug' => 'permission.manage.edit',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 8,
            ])->id,
            'parent_id' => $permissionManage->id,
            'sort_order' => 1,
        ]);

        $permissionUpdateSave = $this->menuPermissionService->createMenuPermission([
            'name' => '保存菜单权限修改',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '菜单权限修改保存',
                'code' => 'perm_permission_save_update',
                'route' => '/backstage/api/permission/update',
                'action' => 'Backstage\System\PermissionController@update',
                'slug' => 'permission.manage.saveupdate',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 9,
            ])->id,
            'parent_id' => $permissionManage->id,
            'sort_order' => 1,
        ]);

        $permissionDelete = $this->menuPermissionService->createMenuPermission([
            'name' => '菜单权限管理删除',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '新增菜单权限删除',
                'code' => 'perm_permission_delete',
                'route' => '/backstage/api/permission/delete',
                'action' => 'Backstage\System\PermissionController@delete',
                'slug' => 'permission.manage.delete',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 7,
            ])->id,
            'parent_id' => $permissionManage->id,
            'sort_order' => 1,
        ]);


        $permissionQuery = $this->menuPermissionService->createMenuPermission([
            'name' => '菜单权限管理查询',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '菜单权限查询',
                'code' => 'perm_permission_query',
                'route' => '/backstage/api/permission/query',
                'action' => 'Backstage\System\PermissionController@queryPermissions',
                'slug' => 'permission.manage.query',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 6,
            ])->id,
            'parent_id' => $permissionManage->id,
            'sort_order' => 1,
        ]);

        $apiPermissionManage = $this->menuPermissionService->createMenuPermission([
            'name' => 'API权限管理',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id' => $this->routeService->createRoute([
                'name' => 'API权限管理主页面',
                'code' => 'perm_api_permission_manage',
                'route' => '/backstage/api-permission',
                'action' => 'Backstage\System\PermissionController@indexApi',
                'slug' => 'permission.api.manage',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 10,
            ])->id,
            'parent_id' => $systemManage->id,
            'sort_order' => 1,
        ]);

        $apiPermissionQuery = $this->menuPermissionService->createMenuPermission([
            'name' => 'API权限查询',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => 'API权限查询路由',
                'code' => 'perm_api_permission_query',
                'route' => '/backstage/api/api-permission/query',
                'action' => 'Backstage\System\PermissionController@queryApi',
                'slug' => 'permission.api.query',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 11,
            ])->id,
            'parent_id' => $apiPermissionManage->id,
            'sort_order' => 1,
        ]);

        $apiPermissionCreate = $this->menuPermissionService->createMenuPermission([
            'name' => '进入新增API权限页面',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => 'API权限新增主页面',
                'code' => 'perm_api_permission_create',
                'route' => '/backstage/api-permission/create',
                'action' => 'Backstage\System\PermissionController@createApi',
                'slug' => 'permission.api.manage.createnew',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 12,
            ])->id,
            'parent_id' => $apiPermissionManage->id,
            'sort_order' => 1,
        ]);

        $apiPermissionCreateSave = $this->menuPermissionService->createMenuPermission([
            'name' => '保存新增',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '新增API权限保存',
                'code' => 'perm_api_permission_save_new',
                'route' => '/backstage/api/api-permission/savenew',
                'action' => 'Backstage\System\PermissionController@saveApi',
                'slug' => 'permission.api.manage.savenew',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 13,
            ])->id,
            'parent_id' => $apiPermissionManage->id,
            'sort_order' => 1,
        ]);

        $apiPermissionUpdate = $this->menuPermissionService->createMenuPermission([
            'name' => '进入修改API权限页面',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => 'API权限修改主页面',
                'code' => 'perm_api_permission_edit',
                'route' => '/backstage/api-permission/edit/{permissionId}',
                'action' => 'Backstage\System\PermissionController@editApi',
                'slug' => 'permission.api.manage.edit',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 14,
            ])->id,
            'parent_id' => $apiPermissionManage->id,
            'sort_order' => 1,
        ]);

        $apiPermissionUpdateSave = $this->menuPermissionService->createMenuPermission([
            'name' => '保存修改',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '修改API权限保存',
                'code' => 'perm_api_permission_save_update',
                'route' => '/backstage/api/api-permission/update',
                'action' => 'Backstage\System\PermissionController@updateApi',
                'slug' => 'permission.api.manage.saveupdate',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 15,
            ])->id,
            'parent_id' => $apiPermissionManage->id,
            'sort_order' => 1,
        ]);

        $apiPermissionDelete = $this->menuPermissionService->createMenuPermission([
            'name' => '删除导航',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '删除API权限',
                'code' => 'perm_api_permission_delete',
                'route' => '/backstage/api/api-permission/delete',
                'action' => 'Backstage\System\PermissionController@deleteApi',
                'slug' => 'permission.api.manage.delete',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 15,
            ])->id,
            'parent_id' => $apiPermissionManage->id,
            'sort_order' => 1,
        ]);

        $currentOrgManage = $this->menuPermissionService->createMenuPermission([
            'name' => '当前运营单位',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id' => $this->routeService->createRoute([
                'name' => '运营单位管理主页面',
                'code' => 'perm_group_platform_manage',
                'route' => '/backstage/current-org',
                'action' => 'Backstage\System\GroupController@getCurrentUserGroup',
                'slug' => 'group.platform.manage',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 2,
            ])->id,
            'parent_id' => $systemManage->id,
            'sort_order' => 2,
        ]);

        $currentOrgSaveManage = $this->menuPermissionService->createMenuPermission([
            'name' => '保存当前运营单位信息',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '保存当前运营单位',
                'code' => 'perm_group_platform_save',
                'route' => '/backstage/api/current-org/save',
                'action' => 'Backstage\System\GroupController@saveCurrentUserGroup',
                'slug' => 'group.platform.save',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 2,
            ])->id,
            'parent_id' => $currentOrgManage->id,
            'sort_order' => 2,
        ]);

        $baseImageManage = $this->menuPermissionService->createMenuPermission([
            'name' => '设置运营图片',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id' => $this->routeService->createRoute([
                'name' => '设置运营图片主页面',
                'code' => 'perm_base_image_manage',
                'route' => '/backstage/base-img',
                'action' => 'Backstage\System\BaseImageController@index',
                'slug' => 'base.image.manage',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 2,
            ])->id,
            'parent_id' => $systemManage->id,
            'sort_order' => 2,
        ]);

        $baseImageQuery = $this->menuPermissionService->createMenuPermission([
            'name' => '运营图片查询',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '运营图片查询路由',
                'code' => 'perm_base_image_query',
                'route' => '/backstage/api/base-image/query',
                'action' => 'Backstage\System\BaseImageController@getImages',
                'slug' => 'base.image.query',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 11,
            ])->id,
            'parent_id' => $baseImageManage->id,
            'sort_order' => 1,
        ]);

        $baseImageCreate = $this->menuPermissionService->createMenuPermission([
            'name' => '进入新增基础图片页面',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '新增基础图片主页面路由',
                'code' => 'perm_base_image_create',
                'route' => '/backstage/base-image/create',
                'action' => 'Backstage\System\BaseImageController@create',
                'slug' => 'base.image.manage.createnew',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 12,
            ])->id,
            'parent_id' => $baseImageManage->id,
            'sort_order' => 1,
        ]);

        $apiPermissionCreateSave = $this->menuPermissionService->createMenuPermission([
            'name' => '保存新增',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '新增基础图片保存路由',
                'code' => 'perm_base_image_save_new',
                'route' => '/backstage/api/base-image/savenew',
                'action' => 'Backstage\System\BaseImageController@saveNew',
                'slug' => 'base.image.manage.savenew',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 13,
            ])->id,
            'parent_id' => $baseImageManage->id,
            'sort_order' => 1,
        ]);

        $groupTypeManage = $this->menuPermissionService->createMenuPermission([
            'name' => '合作伙伴类型管理',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id' => $this->routeService->createRoute([
                'name' => '合作伙伴类型管理主页面',
                'code' => 'perm_partner_group_type_manage',
                'route' => '/backstage/partner-group-type',
                'action' => 'Backstage\System\PartnerGroupTypeController@index',
                'slug' => 'partnergrouptype.manage',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 2,
            ])->id,
            'parent_id' => $systemManage->id,
            'sort_order' => 2,
        ]);

        $groupTypeQueryManage = $this->menuPermissionService->createMenuPermission([
            'name' => '合作伙伴类型查询',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '合作伙伴类型查询路由',
                'code' => 'perm_partner_group_type_query',
                'route' => '/backstage/api/partner-group-type/query',
                'action' => 'Backstage\System\PartnerGroupTypeController@getPartnerGroupTypes',
                'slug' => 'partnergrouptype.api.query',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 2,
            ])->id,
            'parent_id' => $groupTypeManage->id,
            'sort_order' => 2,
        ]);

        $groupTypeCreate = $this->menuPermissionService->createMenuPermission([
            'name' => '新增合作伙伴类型',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '新增合作伙伴类型路由',
                'code' => 'perm_partner_group_type_create_new',
                'route' => '/backstage/partner-group-type/create',
                'action' => 'Backstage\System\PartnerGroupTypeController@create',
                'slug' => 'partnergrouptype.manage.createnew',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 2,
            ])->id,
            'parent_id' => $groupTypeManage->id,
            'sort_order' => 2,
        ]);

        $groupTypeCreateSave = $this->menuPermissionService->createMenuPermission([
            'name' => '保存新增',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '新增合作伙伴类型保存',
                'code' => 'perm_partner_group_type_save_new',
                'route' => '/backstage/api/partner-group-type/savenew',
                'action' => 'Backstage\System\PartnerGroupTypeController@saveNew',
                'slug' => 'partnergrouptype.manage.savenew',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 13,
            ])->id,
            'parent_id' => $groupTypeManage->id,
            'sort_order' => 1,
        ]);

        $groupTypeUpdate = $this->menuPermissionService->createMenuPermission([
            'name' => '进入修改合作伙伴类型页面',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '合作伙伴类型修改主页面',
                'code' => 'perm_partner_group_type_edit',
                'route' => '/backstage/partner-group-type/edit/{typeId}',
                'action' => 'Backstage\System\PartnerGroupTypeController@edit',
                'slug' => 'partnergrouptype.manage.edit',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 14,
            ])->id,
            'parent_id' => $groupTypeManage->id,
            'sort_order' => 1,
        ]);

        $groupTypeUpdateSave = $this->menuPermissionService->createMenuPermission([
            'name' => '保存修改',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '修改合作伙伴类型保存',
                'code' => 'perm_partner_group_type_save_update',
                'route' => '/backstage/api/partner-group-type/update',
                'action' => 'Backstage\System\PartnerGroupTypeController@update',
                'slug' => 'partnergrouptype.manage.saveupdate',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 15,
            ])->id,
            'parent_id' => $groupTypeManage->id,
            'sort_order' => 1,
        ]);

        $groupTypeDelete = $this->menuPermissionService->createMenuPermission([
            'name' => '删除合作伙伴类型',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '删除合作伙伴类型',
                'code' => 'perm_partner_group_type_delete',
                'route' => '/backstage/api/partner-group-type/delete',
                'action' => 'Backstage\System\PartnerGroupTypeController@delete',
                'slug' => 'partnergrouptype.manage.delete',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 15,
            ])->id,
            'parent_id' => $groupTypeManage->id,
            'sort_order' => 1,
        ]);

        $groupTypePermission = $this->menuPermissionService->createMenuPermission([
            'name' => '打开设置合作伙伴默认权限',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '设置合作伙伴默认权限主页面',
                'code' => 'perm_partner_group_type_permission',
                'route' => '/backstage/partner-group-type/permission/{typeId}',
                'action' => 'Backstage\System\PartnerGroupTypeController@getPermissionsByType',
                'slug' => 'partnergrouptype.manage.permission',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 16,
            ])->id,
            'parent_id' => $groupTypeManage->id,
            'sort_order' => 1,
        ]);

        $groupTypePermissionSave = $this->menuPermissionService->createMenuPermission([
            'name' => '保存设置合作伙伴默认权限',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '设置合作伙伴默认权限保存',
                'code' => 'perm_partner_group_type_permission_save',
                'route' => '/backstage/api/partner-group-type/permission/save',
                'action' => 'Backstage\System\PartnerGroupTypeController@saveGroupTypePermissions',
                'slug' => 'partnergrouptype.manage.permission.save',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 17,
            ])->id,
            'parent_id' => $groupTypeManage->id,
            'sort_order' => 1,
        ]);

        $departManage = $this->menuPermissionService->createMenuPermission([
            'name' => '部门管理',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id' => $this->routeService->createRoute([
                'name' => '部门管理主页面',
                'code' => 'perm_group_department_manage',
                'route' => '/backstage/group-department',
                'action' => 'Backstage\System\GroupController@indexOrg',
                'slug' => 'group.department.manage',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 2,
            ])->id,
            'parent_id' => $systemManage->id,
            'sort_order' => 2,
        ]);

        $depGroupGetTree = $this->menuPermissionService->createMenuPermission([
            'name' => '获取右侧部门树',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '获取右侧部门树路由',
                'code' => 'perm_group_department_get_tree',
                'route' => '/backstage/api/group-department/gettree',
                'action' => 'Backstage\System\GroupController@getInitOrgTrees',
                'slug' => 'group.department.manage.gettree',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 6,
            ])->id,
            'parent_id' => $departManage->id,
            'sort_order' => 1,
        ]);

        $departGroupQuery = $this->menuPermissionService->createMenuPermission([
            'name' => '查询内部部门',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '内部部门查询路由',
                'code' => 'perm_group_department_query',
                'route' => '/backstage/api/group-department/query',
                'action' => 'Backstage\System\GroupController@getDepartmentGroups',
                'slug' => 'group.department.api.query',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 7,
            ])->id,
            'parent_id' => $departManage->id,
            'sort_order' => 1,
        ]);

        $departGroupCreate = $this->menuPermissionService->createMenuPermission([
            'name' => '新增内部部门或组织',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '新增内部部门或组织路由',
                'code' => 'perm_group_department_create_new',
                'route' => '/backstage/group-department/create/{parentId}',
                'action' => 'Backstage\System\GroupController@createDepartment',
                'slug' => 'group.department.manage.createnew',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 8,
            ])->id,
            'parent_id' => $departManage->id,
            'sort_order' => 2,
        ]);

        $departCreateSave = $this->menuPermissionService->createMenuPermission([
            'name' => '保存新增',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '新增内部部门或组织保存路由',
                'code' => 'perm_group_department_save_new',
                'route' => '/backstage/api/group-department/savenew',
                'action' => 'Backstage\System\GroupController@saveNewDepartment',
                'slug' => 'group.department.manage.savenew',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 13,
            ])->id,
            'parent_id' => $departManage->id,
            'sort_order' => 3,
        ]);

        $departGroupUpdate = $this->menuPermissionService->createMenuPermission([
            'name' => '进入修改内部部门组织页面',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '内部部门组织修改主页面路由',
                'code' => 'perm_group_department_edit',
                'route' => '/backstage/group-department/edit/{groupId}',
                'action' => 'Backstage\System\GroupController@editDepartment',
                'slug' => 'group.department.manage.edit',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 14,
            ])->id,
            'parent_id' => $departManage->id,
            'sort_order' => 4,
        ]);

        $departGroupUpdateSave = $this->menuPermissionService->createMenuPermission([
            'name' => '保存修改',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '修改部门或内部组织保存',
                'code' => 'perm_group_department_save_update',
                'route' => '/backstage/api/group-department/update',
                'action' => 'Backstage\System\GroupController@updateDepartment',
                'slug' => 'group.department.manage.saveupdate',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 15,
            ])->id,
            'parent_id' => $departManage->id,
            'sort_order' => 5,
        ]);

        $departGroupDelete = $this->menuPermissionService->createMenuPermission([
            'name' => '删除内部组织部门',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '删除内部组织部门',
                'code' => 'perm_group_department_delete',
                'route' => '/backstage/api/group-department/delete',
                'action' => 'Backstage\System\GroupController@deleteDepartment',
                'slug' => 'group.department.manage.delete',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 16,
            ])->id,
            'parent_id' => $departManage->id,
            'sort_order' => 6,
        ]);

        $partnerGroupManage = $this->menuPermissionService->createMenuPermission([
            'name' => '外部单位管理',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id' => $this->routeService->createRoute([
                'name' => '外部单位管理主页面',
                'code' => 'perm_group_external_manage',
                'route' => '/backstage/group-external',
                'action' => 'Backstage\System\GroupController@indexExternalGroup',
                'slug' => 'group.external.manage',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 2,
            ])->id,
            'parent_id' => $systemManage->id,
            'sort_order' => 2,
        ]);

        $partnerGroupQuery = $this->menuPermissionService->createMenuPermission([
            'name' => '查询外部单位',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '外部单位查询路由',
                'code' => 'perm_group_external_query',
                'route' => '/backstage/api/group-external/query',
                'action' => 'Backstage\System\GroupController@getExternalPartnerGroups',
                'slug' => 'group.external.api.query',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 7,
            ])->id,
            'parent_id' => $partnerGroupManage->id,
            'sort_order' => 1,
        ]);

        $partnerGroupCreate = $this->menuPermissionService->createMenuPermission([
            'name' => '新增外部单位',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '新增外部单位伙伴路由',
                'code' => 'perm_group_external_create_new',
                'route' => '/backstage/group-external/create',
                'action' => 'Backstage\System\GroupController@createPartnerGroup',
                'slug' => 'group.external.manage.createnew',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 8,
            ])->id,
            'parent_id' => $partnerGroupManage->id,
            'sort_order' => 2,
        ]);

        $partnerGroupCreateSave = $this->menuPermissionService->createMenuPermission([
            'name' => '保存新增',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '新增外部单位保存路由',
                'code' => 'perm_group_external_save_new',
                'route' => '/backstage/api/group-external/savenew',
                'action' => 'Backstage\System\GroupController@saveNewPartnerGroup',
                'slug' => 'group.external.manage.savenew',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 9,
            ])->id,
            'parent_id' => $partnerGroupManage->id,
            'sort_order' => 3,
        ]);

        $partnerGroupUpdate = $this->menuPermissionService->createMenuPermission([
            'name' => '进入修改合作伙伴页面',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '合作伙伴修改主页面路由',
                'code' => 'perm_group_external_edit',
                'route' => '/backstage/group-external/edit/{groupId}',
                'action' => 'Backstage\System\GroupController@editPartnerGroup',
                'slug' => 'group.external.manage.edit',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 10,
            ])->id,
            'parent_id' => $partnerGroupManage->id,
            'sort_order' => 4,
        ]);

        $partnerGroupUpdateSave = $this->menuPermissionService->createMenuPermission([
            'name' => '保存修改',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '修改外部合作伙伴单位保存',
                'code' => 'perm_group_external_save_update',
                'route' => '/backstage/api/group-external/update',
                'action' => 'Backstage\System\GroupController@saveUpdatePartnerGroup',
                'slug' => 'group.external.manage.saveupdate',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 11,
            ])->id,
            'parent_id' => $partnerGroupManage->id,
            'sort_order' => 5,
        ]);

        $partnerGroupDelete = $this->menuPermissionService->createMenuPermission([
            'name' => '删除外部合作单位',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '删除外部合作单位',
                'code' => 'perm_group_external_delete',
                'route' => '/backstage/api/group-external/delete',
                'action' => 'Backstage\System\GroupController@deletePartnerGroup',
                'slug' => 'group.external.manage.delete',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 12,
            ])->id,
            'parent_id' => $departManage->id,
            'sort_order' => 6,
        ]);

        $userGroupManage = $this->menuPermissionService->createMenuPermission([
            'name' => '个人伙伴管理',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id' => $this->routeService->createRoute([
                'name' => '个人伙伴管理主页面',
                'code' => 'perm_group_simple_manage',
                'route' => '/backstage/group-simple',
                'action' => 'Backstage\System\GroupController@indexSimpleGroup',
                'slug' => 'group.simple.manage',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 2,
            ])->id,
            'parent_id' => $systemManage->id,
            'sort_order' => 2,
        ]);

        $userGroupQuery = $this->menuPermissionService->createMenuPermission([
            'name' => '查询个人伙伴',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '个人伙伴查询路由',
                'code' => 'perm_group_simple_query',
                'route' => '/backstage/api/group-simple/query',
                'action' => 'Backstage\System\GroupController@getSimplePartnerGroups',
                'slug' => 'group.simple.api.query',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 7,
            ])->id,
            'parent_id' => $userGroupManage->id,
            'sort_order' => 1,
        ]);

        $userGroupCreate = $this->menuPermissionService->createMenuPermission([
            'name' => '新增个人伙伴',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '新增个人伙伴路由',
                'code' => 'perm_group_simple_create_new',
                'route' => '/backstage/group-simple/create',
                'action' => 'Backstage\System\GroupController@createSimpleGroup',
                'slug' => 'group.simple.manage.createnew',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 8,
            ])->id,
            'parent_id' => $userGroupManage->id,
            'sort_order' => 2,
        ]);

        $userGroupCreateSave = $this->menuPermissionService->createMenuPermission([
            'name' => '保存新增',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '新增个人伙伴保存路由',
                'code' => 'perm_group_simple_save_new',
                'route' => '/backstage/api/group-simple/savenew',
                'action' => 'Backstage\System\GroupController@saveNewSimpleGroup',
                'slug' => 'group.simple.manage.savenew',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 9,
            ])->id,
            'parent_id' => $userGroupManage->id,
            'sort_order' => 3,
        ]);

        $userGroupUpdate = $this->menuPermissionService->createMenuPermission([
            'name' => '进入修改个人伙伴页面',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '个人伙伴修改主页面路由',
                'code' => 'perm_group_simple_edit',
                'route' => '/backstage/group-simple/edit/{groupId}',
                'action' => 'Backstage\System\GroupController@editSimpleGroup',
                'slug' => 'group.simple.manage.edit',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 10,
            ])->id,
            'parent_id' => $userGroupManage->id,
            'sort_order' => 4,
        ]);

        $userGroupUpdateSave = $this->menuPermissionService->createMenuPermission([
            'name' => '保存修改',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '修改个人伙伴保存',
                'code' => 'perm_group_simple_save_update',
                'route' => '/backstage/api/group-simple/update',
                'action' => 'Backstage\System\GroupController@saveUpdateSimpleGroup',
                'slug' => 'group.simple.manage.saveupdate',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 11,
            ])->id,
            'parent_id' => $userGroupManage->id,
            'sort_order' => 5,
        ]);

        $userGroupDelete = $this->menuPermissionService->createMenuPermission([
            'name' => '删除个人合作伙伴',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '删除个人合作伙伴',
                'code' => 'perm_group_simple_delete',
                'route' => '/backstage/api/group-simple/delete',
                'action' => 'Backstage\System\GroupController@deleteSimpleGroup',
                'slug' => 'group.simple.manage.delete',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 12,
            ])->id,
            'parent_id' => $userGroupManage->id,
            'sort_order' => 6,
        ]);

        $userManage = $this->menuPermissionService->createMenuPermission([
            'name' => '用户管理',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id' => $this->routeService->createRoute([
                'name' => '用户管理主页面',
                'code' => 'perm_user_manage',
                'route' => '/backstage/user',
                'action' => 'Backstage\System\UserController@index',
                'slug' => 'group.user.manage',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 3,
            ])->id,
            'parent_id' => $systemManage->id,
            'sort_order' => 3,
        ]);

        $userGetDeptTree = $this->menuPermissionService->createMenuPermission([
            'name' => '获取右侧部门树',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '获取右侧部门树路由',
                'code' => 'perm_user_department_get_tree',
                'route' => '/backstage/api/user-department/gettree',
                'action' => 'Backstage\System\GroupController@getInitOrgTrees',
                'slug' => 'user.department.manage.gettree',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 4,
            ])->id,
            'parent_id' => $userManage->id,
            'sort_order' => 0,
        ]);

        $userDeptQuery = $this->menuPermissionService->createMenuPermission([
            'name' => '查询内部用户',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '内部用户查询路由',
                'code' => 'perm_user_department_query',
                'route' => '/backstage/api/user-department/query',
                'action' => 'Backstage\System\UserController@queryByDept',
                'slug' => 'user.department.api.query',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 5,
            ])->id,
            'parent_id' => $userManage->id,
            'sort_order' => 1,
        ]);

        $userGroupQuery = $this->menuPermissionService->createMenuPermission([
            'name' => '查询组织用户',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '组织用户查询路由',
                'code' => 'perm_user_group_query',
                'route' => '/backstage/api/user-group/query',
                'action' => 'Backstage\System\UserController@queryUsers',
                'slug' => 'user.group.api.query',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 5,
            ])->id,
            'parent_id' => $userManage->id,
            'sort_order' => 1,
        ]);

        $userDeptSelect = $this->menuPermissionService->createMenuPermission([
            'name' => '添加用户',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '添加用户路由',
                'code' => 'perm_user_to_group_select',
                'route' => '/backstage/user-to-group/select/{groupId}',
                'action' => 'Backstage\System\UserController@toSelectUser',
                'slug' => 'user.togroup.create.select',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 6,
            ])->id,
            'parent_id' => $userManage->id,
            'sort_order' => 1,
        ]);

        $userMobileQuery = $this->menuPermissionService->createMenuPermission([
            'name' => '根据手机查用户',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '根据手机查用户信息路由',
                'code' => 'perm_user_query_by_mobile',
                'route' => '/backstage/api/query-user-bymobile',
                'action' => 'Backstage\System\UserController@checkUserByMobile',
                'slug' => 'user.query.bymobile.manage',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 7,
            ])->id,
            'parent_id' => $userManage->id,
            'sort_order' => 3,
        ]);

        $userCreateNewPage = $this->menuPermissionService->createMenuPermission([
            'name' => '创建新用户',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '进入创建新用户路由',
                'code' => 'perm_user_create_by_mobile',
                'route' => '/backstage/user/create-user-bymobile',
                'action' => 'Backstage\System\UserController@toCreateNewUser',
                'slug' => 'user.create.bymobile.manage',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 8,
            ])->id,
            'parent_id' => $userManage->id,
            'sort_order' => 3,
        ]);

        $userRegMobile = $this->menuPermissionService->createMenuPermission([
            'name' => '发送创建新用户验证码',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '发送创建新用户验证码路由',
                'code' => 'perm_user_register_verify_code',
                'route' => '/backstage/api/user/register-user/getverifycode',
                'action' => 'Backstage\System\UserController@getRegisterVerifyCode',
                'slug' => 'user.register.getverifycode',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 9,
            ])->id,
            'parent_id' => $userManage->id,
            'sort_order' => 3,
        ]);

        $userCreateSave = $this->menuPermissionService->createMenuPermission([
            'name' => '保存新用户',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '新用户保存路由',
                'code' => 'perm_user_create_save_new',
                'route' => '/backstage/api/user/save',
                'action' => 'Backstage\System\UserController@toSaveNewUser',
                'slug' => 'user.create.savenew',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 10,
            ])->id,
            'parent_id' => $userManage->id,
            'sort_order' => 3,
        ]);

        $userModify = $this->menuPermissionService->createMenuPermission([
            'name' => '进入修改用户信息页',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '进入修改用户信息页路由',
                'code' => 'perm_user_edit',
                'route' => '/backstage/user/edit/{userId}',
                'action' => 'Backstage\System\UserController@modifyUser',
                'slug' => 'user.edit.manage',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 11,
            ])->id,
            'parent_id' => $userManage->id,
            'sort_order' => 3,
        ]);

        $userModifyMobile = $this->menuPermissionService->createMenuPermission([
            'name' => '发送修改用户信息验证码',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '发送修改用户信息验证码路由',
                'code' => 'perm_user_modify_verify_code',
                'route' => '/backstage/api/user/modify-user/getverifycode',
                'action' => 'Backstage\System\UserController@getModifyVerifyCode',
                'slug' => 'user.modify.getverifycode',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 12,
            ])->id,
            'parent_id' => $userManage->id,
            'sort_order' => 3,
        ]);

        $userModifySave = $this->menuPermissionService->createMenuPermission([
            'name' => '保存用户修改',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '用户保存修改路由',
                'code' => 'perm_user_modify_save_update',
                'route' => '/backstage/api/user/saveupdate',
                'action' => 'Backstage\System\UserController@toSaveUpdateUser',
                'slug' => 'user.edit.saveupdate',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 13,
            ])->id,
            'parent_id' => $userManage->id,
            'sort_order' => 3,
        ]);

        $userRemoveFromGroup = $this->menuPermissionService->createMenuPermission([
            'name' => '移除用户',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '移除用户路由',
                'code' => 'perm_user_remove_from_group',
                'route' => '/backstage/api/user/removefromgroup',
                'action' => 'Backstage\System\UserController@toRemoveFromGroup',
                'slug' => 'user.remove.fromgroup',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 13,
            ])->id,
            'parent_id' => $userManage->id,
            'sort_order' => 3,
        ]);

        $userAddToGroup = $this->menuPermissionService->createMenuPermission([
            'name' => '添加用户到当前组织',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '添加用户到当前组织路由',
                'code' => 'perm_user_add_to_group',
                'route' => '/backstage/api/user/addtogroup',
                'action' => 'Backstage\System\UserController@toAddUserToCurrent',
                'slug' => 'user.add.togroup',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 14,
            ])->id,
            'parent_id' => $userManage->id,
            'sort_order' => 3,
        ]);

        $roleManage = $this->menuPermissionService->createMenuPermission([
            'name' => '角色管理',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id' => $this->routeService->createRoute([
                'name' => '角色管理主页面',
                'code' => 'perm_role_manage',
                'route' => '/backstage/role',
                'action' => 'Backstage\System\RoleController@index',
                'slug' => 'role.manage',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 4,
            ])->id,
            'parent_id' => $systemManage->id,
            'sort_order' => 4,
        ]);

        $roleQuery = $this->menuPermissionService->createMenuPermission([
            'name' => '查询角色',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '角色查询路由',
                'code' => 'perm_role_query',
                'route' => '/backstage/api/role/query',
                'action' => 'Backstage\System\RoleController@query',
                'slug' => 'role.api.query',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 7,
            ])->id,
            'parent_id' => $roleManage->id,
            'sort_order' => 1,
        ]);

        $roleCreate = $this->menuPermissionService->createMenuPermission([
            'name' => '新增角色',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '新增角色路由',
                'code' => 'perm_role_create_new',
                'route' => '/backstage/role/create',
                'action' => 'Backstage\System\RoleController@create',
                'slug' => 'role.manage.createnew',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 8,
            ])->id,
            'parent_id' => $roleManage->id,
            'sort_order' => 2,
        ]);

        $roleCreateSave = $this->menuPermissionService->createMenuPermission([
            'name' => '保存新增',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '新增角色保存路由',
                'code' => 'perm_role_save_new',
                'route' => '/backstage/api/role/savenew',
                'action' => 'Backstage\System\RoleController@saveNew',
                'slug' => 'role.manage.savenew',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 9,
            ])->id,
            'parent_id' => $roleManage->id,
            'sort_order' => 3,
        ]);

        $roleUpdate = $this->menuPermissionService->createMenuPermission([
            'name' => '进入修改角色页面',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '角色修改页面路由',
                'code' => 'perm_role_edit',
                'route' => '/backstage/role/edit/{roleId}',
                'action' => 'Backstage\System\RoleController@edit',
                'slug' => 'role.manage.edit',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 10,
            ])->id,
            'parent_id' => $roleManage->id,
            'sort_order' => 4,
        ]);

        $roleUpdateSave = $this->menuPermissionService->createMenuPermission([
            'name' => '保存修改',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '角色修改保存',
                'code' => 'perm_role_save_update',
                'route' => '/backstage/api/role/update',
                'action' => 'Backstage\System\RoleController@update',
                'slug' => 'role.manage.saveupdate',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 11,
            ])->id,
            'parent_id' => $roleManage->id,
            'sort_order' => 5,
        ]);

        $roleDelete = $this->menuPermissionService->createMenuPermission([
            'name' => '删除角色',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '删除角色路由',
                'code' => 'perm_role_delete',
                'route' => '/backstage/api/role/delete',
                'action' => 'Backstage\System\RoleController@delete',
                'slug' => 'role.manage.delete',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 12,
            ])->id,
            'parent_id' => $roleManage->id,
            'sort_order' => 6,
        ]);

        $rolePermission = $this->menuPermissionService->createMenuPermission([
            'name' => '打开设置角色权限页面',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '设置角色权限页面路由',
                'code' => 'perm_role_permission',
                'route' => '/backstage/role/permission/{roleId}',
                'action' => 'Backstage\System\RoleController@getPermissionsByRole',
                'slug' => 'role.manage.permission',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 13,
            ])->id,
            'parent_id' => $roleManage->id,
            'sort_order' => 1,
        ]);

        $rolePermission = $this->menuPermissionService->createMenuPermission([
            'name' => '保存设置角色权限',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '保存设置角色权限路由',
                'code' => 'perm_role_permission_save',
                'route' => '/backstage/api/role/permission/save',
                'action' => 'Backstage\System\RoleController@saveRolePermissions',
                'slug' => 'role.manage.permission.save',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 14,
            ])->id,
            'parent_id' => $roleManage->id,
            'sort_order' => 1,
        ]);
    }
}
