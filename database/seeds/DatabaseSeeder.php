<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(DefaultSuperAdminSeeder::class);
        $this->call(DefaultModuleSeeder::class);
        $this->call(CreateOtherModuleSeeder::class);
        $this->call(CreateUtilitySeeder::class);
        $this->call(NormalGodUserSeeder::class);
        $this->call(CityTableSeeder::class);
        $this->call(CityDistrictInitSeeder::class);
        $this->call(IteratorFrontV1Seeder::class);
        $this->call(AppendBaseModuleSeeder::class);
        $this->call(IteratorBackstageV1Seeder::class);
        $this->call(IteratorBackstageV2Seeder::class);
    }
}
