<?php

use Illuminate\Database\Seeder;
use App\MenuPermission;
use App\Route;
use App\BaseDictionary;
use App\Services\MenuPermissionService;
use App\Services\RouteService;

class IteratorBackstageV1Seeder extends Seeder
{
    private $menuPermissionService = null;
    private $routeService = null;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->menuPermissionService = new MenuPermissionService();
        $this->routeService = new RouteService();

        $this->appendModule();
    }

    private function appendModule()
    {
        $topPerms = $this->menuPermissionService->getMenuPermissions([
            'permissionName' => '业务运营', 
            'permissionType' => MenuPermission::$PERMISSION_TYPE_TOP_NAVI
        ], [
            MenuPermission::$PERMISSION_TYPE_TOP_NAVI
        ], false);

        if (count($topPerms) == 0) {
            return false;
        }

        $subPerms = $this->menuPermissionService->getMenuPermissions([
            'permissionName' => '模型管理', 
            'parentId' => $topPerms[0]['id']
        ], [
            MenuPermission::$PERMISSION_TYPE_TOP_NAVI,
            MenuPermission::$PERMISSION_TYPE_MENU_GROUP
        ], false);

        if (count($subPerms) == 0) {
            return false;
        }

        $xMenus = $this->menuPermissionService->getMenuPermissions([
            'permissionName' => '模型管理', 
            'parentId' => $subPerms[0]['id']
        ], [
            MenuPermission::$PERMISSION_TYPE_TOP_NAVI,
            MenuPermission::$PERMISSION_TYPE_MENU_GROUP,
            MenuPermission::$PERMISSION_TYPE_MENU,
        ], false);

        if (count($xMenus) == 0) {
            return false;
        }

        $xMenuId = $xMenus[0]['id'];

        $xEdit = $this->menuPermissionService->createMenuPermission([
            'name' => '进入修改模型页',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '修改模型页面路由',
                'code' => 'perm_model_edit',
                'route' => '/backstage/model/edit/{modelId}',
                'action' => 'Operation\Model\ModelController@edit',
                'slug' => 'model.manage.edit',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 7,
            ])->id,
            'parent_id' => $xMenuId,
            'sort_order' => 2,
        ]);

        $xEditSave = $this->menuPermissionService->createMenuPermission([
            'name' => '保存修改',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '修改模型保存',
                'code' => 'perm_model_save_update',
                'route' => '/backstage/api/model/update',
                'action' => 'Operation\Model\ModelController@update',
                'slug' => 'model.manage.saveupdate',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 8,
            ])->id,
            'parent_id' => $xMenuId,
            'sort_order' => 3,
        ]);

        $xDelete = $this->menuPermissionService->createMenuPermission([
            'name' => '删除模型',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '删除模型',
                'code' => 'perm_model_delete',
                'route' => '/backstage/api/model/delete',
                'action' => 'Operation\Model\ModelController@delete',
                'slug' => 'model.manage.delete',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 9,
            ])->id,
            'parent_id' => $xMenuId,
            'sort_order' => 4,
        ]);
    }
}
