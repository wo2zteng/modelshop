<?php

use Illuminate\Database\Seeder;
use App\District;

class CityDistrictInitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->initShenzhen();
    }

    private function initShenzhen()
    {
        $cityId = config('constants.DEFAULT_CITY_ID');

        District::create([
            'name' => '福田',
            'full_name' => '福田区',
            'sort_order' => 0,
            'city_id' => $cityId
        ]);

        District::create([
            'name' => '罗湖',
            'full_name' => '罗湖区',
            'sort_order' => 1,
            'city_id' => $cityId
        ]);

        District::create([
            'name' => '南山',
            'full_name' => '南山区',
            'sort_order' => 2,
            'city_id' => $cityId
        ]);

        District::create([
            'name' => '宝安',
            'full_name' => '宝安区',
            'sort_order' => 3,
            'city_id' => $cityId
        ]);

        District::create([
            'name' => '龙岗',
            'full_name' => '龙岗区',
            'sort_order' => 4,
            'city_id' => $cityId
        ]);

        District::create([
            'name' => '盐田',
            'full_name' => '盐田区',
            'sort_order' => 5,
            'city_id' => $cityId
        ]);
    }
}
