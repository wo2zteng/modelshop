<?php

use Illuminate\Database\Seeder;
use App\MenuPermission;
use App\Route;
use App\BaseDictionary;
use App\Services\MenuPermissionService;
use App\Services\RouteService;

class AppendBaseModuleSeeder extends Seeder
{
    private $menuPermissionService = null;
    private $routeService = null;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->menuPermissionService = new MenuPermissionService();
        $this->routeService = new RouteService();

        $this->appendModule();
    }

    private function appendModule()
    {
        $topPerms = $this->menuPermissionService->getMenuPermissions([
            'permissionName' => '基础运营', 
            'permissionType' => MenuPermission::$PERMISSION_TYPE_TOP_NAVI
        ], [
            MenuPermission::$PERMISSION_TYPE_TOP_NAVI
        ] ,false);

        if (count($topPerms) == 0) {
            return false;
        }

        $subPerms = $this->menuPermissionService->getMenuPermissions([
            'permissionName' => '系统管理', 
            'parentId' => $topPerms[0]['id']
        ], [
            MenuPermission::$PERMISSION_TYPE_TOP_NAVI,
            MenuPermission::$PERMISSION_TYPE_MENU_GROUP
        ], false);

        if (count($subPerms) == 0) {
            return false;
        }

        $baseImgMenus = $this->menuPermissionService->getMenuPermissions([
            'permissionName' => '设置运营图片', 
            'parentId' => $subPerms[0]['id']
        ], [
            MenuPermission::$PERMISSION_TYPE_TOP_NAVI,
            MenuPermission::$PERMISSION_TYPE_MENU_GROUP,
            MenuPermission::$PERMISSION_TYPE_MENU,
        ], false);

        if (count($baseImgMenus) == 0) {
            return false;
        }

        $baseImgMenuId = $baseImgMenus[0]['id'];

        $baseImgEdit = $this->menuPermissionService->createMenuPermission([
            'name' => '进入修改运营图片页',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '修改运营图片页面路由',
                'code' => 'perm_base_image_edit',
                'route' => '/backstage/base-image/edit/{imgId}',
                'action' => 'Backstage\System\BaseImageController@edit',
                'slug' => 'base.image.manage.edit',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 7,
            ])->id,
            'parent_id' => $baseImgMenuId,
            'sort_order' => 2,
        ]);

        $baseImgEditSave = $this->menuPermissionService->createMenuPermission([
            'name' => '保存修改',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '修改运营图片保存',
                'code' => 'perm_base_image_save_update',
                'route' => '/backstage/api/base-image/update',
                'action' => 'Backstage\System\BaseImageController@update',
                'slug' => 'base.image.manage.saveupdate',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 8,
            ])->id,
            'parent_id' => $baseImgMenuId,
            'sort_order' => 3,
        ]);

        $routeDelete = $this->menuPermissionService->createMenuPermission([
            'name' => '删除运营图片',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '删除运营图片',
                'code' => 'perm_base_image_delete',
                'route' => '/backstage/api/base-image/delete',
                'action' => 'Backstage\System\BaseImageController@delete',
                'slug' => 'base.image.manage.delete',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 9,
            ])->id,
            'parent_id' => $baseImgMenuId,
            'sort_order' => 4,
        ]);
    }
}
