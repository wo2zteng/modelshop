<?php

use Illuminate\Database\Seeder;
use App\MenuPermission;
use App\Route;
use App\BaseDictionary;
use App\Services\MenuPermissionService;
use App\Services\RouteService;

class IteratorBackstageV2Seeder extends Seeder
{
    private $menuPermissionService = null;
    private $routeService = null;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->menuPermissionService = new MenuPermissionService();
        $this->routeService = new RouteService();

        $this->appendModule();
    }

    private function appendModule()
    {
        $topPerms = $this->menuPermissionService->getMenuPermissions([
            'permissionName' => '业务运营', 
            'permissionType' => MenuPermission::$PERMISSION_TYPE_TOP_NAVI
        ], [
            MenuPermission::$PERMISSION_TYPE_TOP_NAVI
        ], false);

        if (count($topPerms) == 0) {
            return false;
        }

        $subPerms = $this->menuPermissionService->getMenuPermissions([
            'permissionName' => '模型管理', 
            'parentId' => $topPerms[0]['id']
        ], [
            MenuPermission::$PERMISSION_TYPE_TOP_NAVI,
            MenuPermission::$PERMISSION_TYPE_MENU_GROUP
        ], false);

        if (count($subPerms) == 0) {
            return false;
        }

        $xMenus = $this->menuPermissionService->getMenuPermissions([
            'permissionName' => '模型品类管理', 
            'parentId' => $subPerms[0]['id']
        ], [
            MenuPermission::$PERMISSION_TYPE_TOP_NAVI,
            MenuPermission::$PERMISSION_TYPE_MENU_GROUP,
            MenuPermission::$PERMISSION_TYPE_MENU,
        ], false);

        if (count($xMenus) == 0) {
            return false;
        }

        $xMenuId = $xMenus[0]['id'];

        $classDelete = $this->menuPermissionService->createMenuPermission([
            'name' => '删除模型品类',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '删除模型品类',
                'code' => 'perm_model_class_delete',
                'route' => '/backstage/api/model-class/delete',
                'action' => 'Operation\Model\ModelClassController@delete',
                'slug' => 'model.class.manage.delete',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 8,
            ])->id,
            'parent_id' => $xMenuId,
            'sort_order' => 0,
        ]);
    }
}
