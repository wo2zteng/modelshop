<?php

use Illuminate\Database\Seeder;
use App\Services\UserService;

class DefaultSuperAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userService = new UserService();
        $userService->createSuperAdmin();
    }
}
