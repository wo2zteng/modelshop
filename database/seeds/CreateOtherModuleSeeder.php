<?php

use Illuminate\Database\Seeder;
use App\MenuPermission;
use App\Route;
use App\BaseDictionary;
use App\Services\MenuPermissionService;
use App\Services\RouteService;

class CreateOtherModuleSeeder extends Seeder
{
    private $menuPermissionService = null;
    private $routeService = null;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->menuPermissionService = new MenuPermissionService();
        $this->routeService = new RouteService();

        $operationModule = $this->menuPermissionService->createMenuPermission([
            'name' => '业务运营',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_TOP_NAVI,
            'parent_id' => null,
            'sort_order' => 1,
        ]);

        $this->createModelFuncs($operationModule);
    }

    private function createModelFuncs($operationModule)
    {
        $modelFuncs = $this->menuPermissionService->createMenuPermission([
            'name' => '模型管理',
            'icon' => '&#xe631;',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU_GROUP,
            'parent_id' => $operationModule->id,
            'sort_order' => 0,
        ]);

        $categoryManage = $this->menuPermissionService->createMenuPermission([
            'name' => '模型分类管理',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id' => $this->routeService->createRoute([
                'name' => '模型分类管理主页面',
                'code' => 'perm_model_category_manage',
                'route' => '/backstage/model-category',
                'action' => 'Operation\Model\ModelCategoryController@index',
                'slug' => 'model.category.manage',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 0,
            ])->id,
            'parent_id' => $modelFuncs->id,
            'sort_order' => 0,
        ]);

        $categoryTree = $this->menuPermissionService->createMenuPermission([
            'name' => '获取模型分类右侧树',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '获取模型分类右侧树路由',
                'code' => 'perm_model_category_get_tree',
                'route' => '/backstage/api/model-category/gettree',
                'action' => 'Operation\Model\ModelCategoryController@getInitCategoryTrees',
                'slug' => 'model.category.manage.gettree',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 1,
            ])->id,
            'parent_id' => $categoryManage->id,
            'sort_order' => 1,
        ]);

        $categoryQuery = $this->menuPermissionService->createMenuPermission([
            'name' => '查询模型分类',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '查询模型分类路由',
                'code' => 'perm_model_category_query',
                'route' => '/backstage/api/model-category/query',
                'action' => 'Operation\Model\ModelCategoryController@query',
                'slug' => 'model.category.manage.query',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 2,
            ])->id,
            'parent_id' => $categoryManage->id,
            'sort_order' => 1,
        ]);

        $categoryCreate = $this->menuPermissionService->createMenuPermission([
            'name' => '新增模型分类',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '新增模型分类路由',
                'code' => 'perm_model_category_create_new',
                'route' => '/backstage/model-category/create/{parentId}',
                'action' => 'Operation\Model\ModelCategoryController@createCategory',
                'slug' => 'model.category.manage.createnew',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 3,
            ])->id,
            'parent_id' => $categoryManage->id,
            'sort_order' => 2,
        ]);

        $categoryCreateSave = $this->menuPermissionService->createMenuPermission([
            'name' => '新增模型分类保存',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '新增模型分类保存路由',
                'code' => 'perm_model_category_save_new',
                'route' => '/backstage/api/model-category/savenew',
                'action' => 'Operation\Model\ModelCategoryController@saveNewCategory',
                'slug' => 'model.category.manage.savenew',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 4,
            ])->id,
            'parent_id' => $categoryManage->id,
            'sort_order' => 3,
        ]);

        $categoryUpdate = $this->menuPermissionService->createMenuPermission([
            'name' => '进入修改模型分类页面',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '修改模型分类页面路由',
                'code' => 'perm_model_category_edit',
                'route' => '/backstage/model-category/edit/{categoryId}',
                'action' => 'Operation\Model\ModelCategoryController@editCategory',
                'slug' => 'model.category.manage.edit',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 5,
            ])->id,
            'parent_id' => $categoryManage->id,
            'sort_order' => 4,
        ]);

        $categorySaveUpdate = $this->menuPermissionService->createMenuPermission([
            'name' => '保存修改',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '保存修改模型分类路由',
                'code' => 'perm_model_category_save_update',
                'route' => '/backstage/api/model-category/update',
                'action' => 'Operation\Model\ModelCategoryController@saveUpdateCategory',
                'slug' => 'model.category.manage.saveupdate',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 6,
            ])->id,
            'parent_id' => $categoryManage->id,
            'sort_order' => 5,
        ]);

        $categoryDelete = $this->menuPermissionService->createMenuPermission([
            'name' => '删除模型分类',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '删除模型分类路由',
                'code' => 'perm_model_category_delete',
                'route' => '/backstage/api/model-category/delete',
                'action' => 'Operation\Model\ModelCategoryController@deleteCategory',
                'slug' => 'model.category.manage.delete',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 7,
            ])->id,
            'parent_id' => $categoryManage->id,
            'sort_order' => 6,
        ]);

        $classManage = $this->menuPermissionService->createMenuPermission([
            'name' => '模型品类管理',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id' => $this->routeService->createRoute([
                'name' => '模型品类管理主页面',
                'code' => 'perm_model_class_manage',
                'route' => '/backstage/model-class',
                'action' => 'Operation\Model\ModelClassController@index',
                'slug' => 'model.class.manage',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 1,
            ])->id,
            'parent_id' => $modelFuncs->id,
            'sort_order' => 1,
        ]);

        $classQuery = $this->menuPermissionService->createMenuPermission([
            'name' => '查询模型品类',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '查询模型品类路由',
                'code' => 'perm_model_class_query',
                'route' => '/backstage/api/model-class/query',
                'action' => 'Operation\Model\ModelClassController@query',
                'slug' => 'model.class.manage.query',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 2,
            ])->id,
            'parent_id' => $classManage->id,
            'sort_order' => 1,
        ]);

        $classCreate = $this->menuPermissionService->createMenuPermission([
            'name' => '进入创建模型品类页面',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '创建模型品类页面路由',
                'code' => 'perm_model_class_create_new',
                'route' => '/backstage/api/model-class/create',
                'action' => 'Operation\Model\ModelClassController@createNew',
                'slug' => 'model.class.manage.createnew',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 2,
            ])->id,
            'parent_id' => $classManage->id,
            'sort_order' => 1,
        ]);

        $classCreateSave = $this->menuPermissionService->createMenuPermission([
            'name' => '保存新增模型品类',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '保存新增模型品类路由',
                'code' => 'perm_model_class_create_savenew',
                'route' => '/backstage/api/model-class/savenew',
                'action' => 'Operation\Model\ModelClassController@saveNewClass',
                'slug' => 'model.class.manage.savenew',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 2,
            ])->id,
            'parent_id' => $classManage->id,
            'sort_order' => 1,
        ]);

        $classEdit = $this->menuPermissionService->createMenuPermission([
            'name' => '进入修改模型品类页面',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '修改模型品类页面路由',
                'code' => 'perm_model_class_edit',
                'route' => '/backstage/model-class/edit/{classId}',
                'action' => 'Operation\Model\ModelClassController@editClass',
                'slug' => 'model.class.manage.edit',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 7,
            ])->id,
            'parent_id' => $classManage->id,
            'sort_order' => 0,
        ]);

        $classEditSave = $this->menuPermissionService->createMenuPermission([
            'name' => '保存修改',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '修改模型品类保存',
                'code' => 'perm_model_class_save_update',
                'route' => '/backstage/api/model-class/update',
                'action' => 'Operation\Model\ModelClassController@saveUpdateClass',
                'slug' => 'model.class.manage.saveupdate',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 8,
            ])->id,
            'parent_id' => $classManage->id,
            'sort_order' => 0,
        ]);

        $modelManage = $this->menuPermissionService->createMenuPermission([
            'name' => '模型管理',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id' => $this->routeService->createRoute([
                'name' => '模型管理主页面',
                'code' => 'perm_model_manage',
                'route' => '/backstage/model',
                'action' => 'Operation\Model\ModelController@index',
                'slug' => 'model.manage',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 2,
            ])->id,
            'parent_id' => $modelFuncs->id,
            'sort_order' => 0,
        ]);

        $modelCategoryTree = $this->menuPermissionService->createMenuPermission([
            'name' => '获取模型分类右侧树',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '获取模型分类右侧树路由',
                'code' => 'perm_model_mode_category_get_tree',
                'route' => '/backstage/api/model-model-category/gettree',
                'action' => 'Operation\Model\ModelController@getInitCategoryTrees',
                'slug' => 'model.model.category.manage.gettree',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 1,
            ])->id,
            'parent_id' => $modelManage->id,
            'sort_order' => 1,
        ]);

        $modelClassesQuery = $this->menuPermissionService->createMenuPermission([
            'name' => '获取模型品类列表',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '获取获取模型品类列表路由',
                'code' => 'perm_model_category_query_model',
                'route' => '/backstage/api/model_category_class/getclass/{categoryId}',
                'action' => 'Operation\Model\ModelController@getCateClasses',
                'slug' => 'model.model.category.manage.gettree',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 1,
            ])->id,
            'parent_id' => $modelManage->id,
            'sort_order' => 1,
        ]);

        $modelQuery = $this->menuPermissionService->createMenuPermission([
            'name' => '查询模型',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '查询模型路由',
                'code' => 'perm_model_query',
                'route' => '/backstage/api/model/query',
                'action' => 'Operation\Model\ModelController@query',
                'slug' => 'model.manage.query',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 2,
            ])->id,
            'parent_id' => $modelManage->id,
            'sort_order' => 1,
        ]);

        $modelQueryCategory = $this->menuPermissionService->createMenuPermission([
            'name' => '查询模型分类列表',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '查询模型分类列表路由',
                'code' => 'perm_model_category_query_list',
                'route' => '/backstage/api/model-category/querylist',
                'action' => 'Operation\Model\ModelController@getCategory',
                'slug' => 'model-category.manage.querylist',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 2,
            ])->id,
            'parent_id' => $modelManage->id,
            'sort_order' => 1,
        ]);

        $modelCreate = $this->menuPermissionService->createMenuPermission([
            'name' => '进入新增模型页面',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '新增模型品类路由',
                'code' => 'perm_model_create_new',
                'route' => '/backstage/model/create',
                'action' => 'Operation\Model\ModelController@create',
                'slug' => 'model.manage.createnew',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 2,
            ])->id,
            'parent_id' => $modelManage->id,
            'sort_order' => 1,
        ]);

        $modelPropertyQuery = $this->menuPermissionService->createMenuPermission([
            'name' => '获得模型品类属性',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '获得模型品类属性路由',
                'code' => 'perm_model_property_query_list',
                'route' => '/backstage/api/model-property/querylist',
                'action' => 'Operation\Model\ModelController@getModelProperties',
                'slug' => 'model-property.manage.querylist',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 2,
            ])->id,
            'parent_id' => $modelManage->id,
            'sort_order' => 1,
        ]);

        $modelCreateSave = $this->menuPermissionService->createMenuPermission([
            'name' => '保存新增模型',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '保存新增模型路由',
                'code' => 'perm_model_create_savenew',
                'route' => '/backstage/api/model/savenew',
                'action' => 'Operation\Model\ModelController@saveNewModel',
                'slug' => 'model.manage.savenew',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 2,
            ])->id,
            'parent_id' => $modelManage->id,
            'sort_order' => 1,
        ]);
    }
}
