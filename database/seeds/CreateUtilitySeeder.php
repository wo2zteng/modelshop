<?php

use Illuminate\Database\Seeder;
use App\MenuPermission;
use App\Route;
use App\BaseDictionary;
use App\Services\MenuPermissionService;
use App\Services\RouteService;

class CreateUtilitySeeder extends Seeder
{
    private $menuPermissionService = null;
    private $routeService = null;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->menuPermissionService = new MenuPermissionService();
        $this->routeService = new RouteService();

        $extensionModule = $this->menuPermissionService->createMenuPermission([
            'name' => '扩展组件',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_TOP_NAVI,
            'parent_id' => null,
            'sort_order' => 1,
        ]);

        $this->createUtilityFuncs($extensionModule);
    }

    private function createUtilityFuncs($extensionModule)
    {
        $projectFuncs = $this->menuPermissionService->createMenuPermission([
            'name' => '项目工具',
            'icon' => '&#xe631;',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU_GROUP,
            'parent_id' => $extensionModule->id,
            'sort_order' => 0,
        ]);

        $helpTutorialManage = $this->menuPermissionService->createMenuPermission([
            'name' => '服装素材库工具',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id' => $this->routeService->createRoute([
                'name' => '服装素材库工具主页面',
                'code' => 'perm_help_tutorial_manage',
                'route' => '/backstage/help-tutorial',
                'action' => 'Utility\HelpTutorialController@index',
                'slug' => 'helptutorial.manage',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 0,
            ])->id,
            'parent_id' => $projectFuncs->id,
            'sort_order' => 0,
        ]);

        $tutorialTree = $this->menuPermissionService->createMenuPermission([
            'name' => '获取服装素材库右侧树',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '获取服装素材库右侧树路由',
                'code' => 'perm_help_tutorial_get_tree',
                'route' => '/backstage/api/help-tutorial/gettree',
                'action' => 'Utility\HelpTutorialController@getInitTutorialTrees',
                'slug' => 'helptutorial.manage.gettree',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 1,
            ])->id,
            'parent_id' => $helpTutorialManage->id,
            'sort_order' => 1,
        ]);

        $tutorialQuery = $this->menuPermissionService->createMenuPermission([
            'name' => '查询服装素材库',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '查询服装素材库路由',
                'code' => 'perm_help_tutorial_query',
                'route' => '/backstage/api/help-tutorial/query',
                'action' => 'Utility\HelpTutorialController@query',
                'slug' => 'helptutorial.manage.query',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 2,
            ])->id,
            'parent_id' => $helpTutorialManage->id,
            'sort_order' => 1,
        ]);

        $tutorialCreate = $this->menuPermissionService->createMenuPermission([
            'name' => '新增素材文档条目',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '新增素材文档条目路由',
                'code' => 'perm_help_tutorial_create_new',
                'route' => '/backstage/help-tutorial/create/{parentId}',
                'action' => 'Utility\HelpTutorialController@createTutorial',
                'slug' => 'helptutorial.manage.createnew',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 3,
            ])->id,
            'parent_id' => $helpTutorialManage->id,
            'sort_order' => 2,
        ]);

        $tutorialCreateSave = $this->menuPermissionService->createMenuPermission([
            'name' => '新增素材文档条目保存',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '新增素材文档条目保存路由',
                'code' => 'perm_help_tutorial_save_new',
                'route' => '/backstage/api/help-tutorial/savenew',
                'action' => 'Utility\HelpTutorialController@saveNewTutorial',
                'slug' => 'helptutorial.manage.savenew',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 4,
            ])->id,
            'parent_id' => $helpTutorialManage->id,
            'sort_order' => 3,
        ]);

        $tutorialUpdate = $this->menuPermissionService->createMenuPermission([
            'name' => '进入修改素材文档条目页面',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '素材文档条目路由',
                'code' => 'perm_help_tutorial_edit',
                'route' => '/backstage/help-tutorial/edit/{tutorialId}',
                'action' => 'Utility\HelpTutorialController@editTutorial',
                'slug' => 'helptutorial.manage.edit',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 5,
            ])->id,
            'parent_id' => $helpTutorialManage->id,
            'sort_order' => 4,
        ]);

        $tutorialSaveUpdate = $this->menuPermissionService->createMenuPermission([
            'name' => '保存修改',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '保存修改素材文档条目路由',
                'code' => 'perm_help_tutorial_save_update',
                'route' => '/backstage/api/help-tutorial/update',
                'action' => 'Utility\HelpTutorialController@saveUpdateTutorial',
                'slug' => 'helptutorial.manage.saveupdate',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 6,
            ])->id,
            'parent_id' => $helpTutorialManage->id,
            'sort_order' => 5,
        ]);

        $helpTutorialDelete = $this->menuPermissionService->createMenuPermission([
            'name' => '删除素材文档条目分类',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '删除素材文档条目路由',
                'code' => 'perm_help_tutorial_delete',
                'route' => '/backstage/api/help-tutorial/delete',
                'action' => 'Utility\HelpTutorialController@deleteTutorial',
                'slug' => 'helptutorial.manage.delete',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 7,
            ])->id,
            'parent_id' => $helpTutorialManage->id,
            'sort_order' => 6,
        ]);

        $tutorialDetailUpdate = $this->menuPermissionService->createMenuPermission([
            'name' => '进入修改素材图片页面',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '素材图片修改路由',
                'code' => 'perm_help_tutorial_images_edit',
                'route' => '/backstage/help-tutorial/edit-detail/{tutorialId}',
                'action' => 'Utility\HelpTutorialController@getTutorialDetail',
                'slug' => 'helptutorial.manage.detail.edit',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 8,
            ])->id,
            'parent_id' => $helpTutorialManage->id,
            'sort_order' => 7,
        ]);

        $tutorialDetailQuery = $this->menuPermissionService->createMenuPermission([
            'name' => '查询所有素材图片',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '查询所有素材图片路由',
                'code' => 'perm_help_tutorial_images_query',
                'route' => '/backstage/api/help-tutorial/edit-detail/query/{tutorialId}',
                'action' => 'Utility\HelpTutorialController@getTutorialDetailImages',
                'slug' => 'helptutorial.manage.detail.query',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 8,
            ])->id,
            'parent_id' => $helpTutorialManage->id,
            'sort_order' => 7,
        ]);

        $tutorialImageAdd = $this->menuPermissionService->createMenuPermission([
            'name' => '新增素材图片',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '新增素材图片路由',
                'code' => 'perm_help_tutorial_images_add',
                'route' => '/backstage/api/help-tutorial/edit-detail/add',
                'action' => 'Utility\HelpTutorialController@addTutorialImage',
                'slug' => 'helptutorial.manage.detail.add',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 8,
            ])->id,
            'parent_id' => $helpTutorialManage->id,
            'sort_order' => 7,
        ]);

        $tutorialImageDelete = $this->menuPermissionService->createMenuPermission([
            'name' => '删除素材图片',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '删除素材图片路由',
                'code' => 'perm_help_tutorial_images_delete',
                'route' => '/backstage/api/help-tutorial/edit-detail/delete/{tutorialImageId}',
                'action' => 'Utility\HelpTutorialController@deleteTutorialImage',
                'slug' => 'helptutorial.manage.detail.delete',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 8,
            ])->id,
            'parent_id' => $helpTutorialManage->id,
            'sort_order' => 7,
        ]);
    }
}
