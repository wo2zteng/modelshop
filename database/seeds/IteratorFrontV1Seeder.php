<?php

use Illuminate\Database\Seeder;
use App\MenuPermission;
use App\Route;
use App\BaseDictionary;
use App\Services\MenuPermissionService;
use App\Services\RouteService;

class IteratorFrontV1Seeder extends Seeder
{
    private $menuPermissionService = null;
    private $routeService = null;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->menuPermissionService = new MenuPermissionService();
        $this->routeService = new RouteService();

        $this->makeModelManage();
    }

    private function makeModelManage()
    {
        $topPerms = $this->menuPermissionService->getMenuPermissions([
            'permissionName' => '业务运营', 
            'permissionType' => MenuPermission::$PERMISSION_TYPE_TOP_NAVI
        ], [
            MenuPermission::$PERMISSION_TYPE_TOP_NAVI
        ] ,false);

        if (count($topPerms) == 0) {
            return false;
        }

        $subPerms = $this->menuPermissionService->getMenuPermissions([
            'permissionName' => '模型管理', 
            'parentId' => $topPerms[0]['id']
        ], [
            MenuPermission::$PERMISSION_TYPE_TOP_NAVI,
            MenuPermission::$PERMISSION_TYPE_MENU_GROUP
        ], false);

        if (count($subPerms) == 0) {
            return false;
        }

        $modelFuncsId = $subPerms[0]['id'];

        $modelAllManage = $this->menuPermissionService->createMenuPermission([
            'name' => '管理运营模型',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_MENU,
            'route_id' => $this->routeService->createRoute([
                'name' => '管理运营模型主页面',
                'code' => 'perm_model_oper_manage',
                'route' => '/backstage/model-oper',
                'action' => 'Operation\Model\ModelController@indexOper',
                'slug' => 'model.oper.manage',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 2,
            ])->id,
            'parent_id' => $modelFuncsId,
            'sort_order' => 0,
        ]);

        $modelAllQuery = $this->menuPermissionService->createMenuPermission([
            'name' => '查询模型',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '查询模型路由',
                'code' => 'perm_model_oper_query',
                'route' => '/backstage/api/model-oper/query',
                'action' => 'Operation\Model\ModelController@queryAll',
                'slug' => 'model.oper.manage.query',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 2,
            ])->id,
            'parent_id' => $modelAllManage->id,
            'sort_order' => 1,
        ]);

        $modelRecommandQuery = $this->menuPermissionService->createMenuPermission([
            'name' => '首页推荐设置',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '首页推荐设置路由',
                'code' => 'perm_model_oper_settings',
                'route' => '/backstage/model-oper/settings/{modelId}',
                'action' => 'Operation\Model\ModelController@manageSetting',
                'slug' => 'model.oper.manage.settings',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_GET,
                'sort_order' => 2,
            ])->id,
            'parent_id' => $modelAllManage->id,
            'sort_order' => 2,
        ]);

        $modelSettingSave = $this->menuPermissionService->createMenuPermission([
            'name' => '保存网站首页推荐设置',
            'permission_type' => MenuPermission::$PERMISSION_TYPE_OPERATION,
            'route_id' => $this->routeService->createRoute([
                'name' => '保存网站首页推荐设置路由',
                'code' => 'perm_model_oper_settings_save',
                'route' => '/backstage/api/model-oper/settings/dosave',
                'action' => 'Operation\Model\ModelController@doSaveMainPageModelSetting',
                'slug' => 'model.oper.manage.settings.save',
                'route_type' => Route::$ROUTE_TYPE_WEB,
                'method' => Route::$ROUTE_METHOD_POST,
                'sort_order' => 2,
            ])->id,
            'parent_id' => $modelAllManage->id,
            'sort_order' => 3,
        ]);
    }
}
