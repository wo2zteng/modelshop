<?php

use Illuminate\Database\Seeder;

use App\Services\UserGroupService;
use App\Services\UserService;
use App\BaseDictionary;
use App\UserGroup;
use App\Role;
use App\UserRole;
use App\RoleMenuPermission;
use App\MenuPermission;
use App\User;
use App\GroupUser;

class NormalGodUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createUserGroup();
    }

    private function createUserGroup()
    {
        $userGroupService = new UserGroupService();
        $userService = new UserService();

        $rootOrg = $userGroupService->createUserGroup([
            'name' => config('system.default_platform_org'),
            'code' => $userGroupService->getNewGroupCode(),
            'group_type' => UserGroup::$GROUP_TYPE_INTERNAL_ORGANIZATION,
            'active_status' => UserGroup::$GROUP_STATUS_ACTIVE,
            'sort_order' => 0,
        ]);
    }
}
