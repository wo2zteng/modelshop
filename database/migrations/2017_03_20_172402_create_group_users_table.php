<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_users', function (Blueprint $table) {
            $table->bigInteger('user_id')->unsigned()->comment('用户ID');
            $table->bigInteger('group_id')->unsigned()->comment('用户组id');
            $table->tinyInteger('is_primary_group')->default(0)->nullable()->comment('是否主用户组：0=否，1=是');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('group_id')->references('id')->on('user_groups');

            $table->primary(['user_id', 'group_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('group_users', function ($table) {
            $table->dropForeign(['user_id']);
            $table->dropForeign(['group_id']);
        });
        Schema::dropIfExists('group_users');
    }
}
