<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModelClassPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('model_class_properties', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('name', 100)->nullable()->comment('属性名称');
            $table->string('value', 1000)->nullable()->comment('属性值，形如:key1:value1,key2:value2');
            $table->tinyInteger('sort_order')->unsigned()->nullable()->comment('同级排序号');
            $table->integer('class_id')->unsigned()->nullable()->comment('品类ID');
            $table->foreign('class_id')->references('id')->on('model_classes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('model_class_properties');
    }
}
