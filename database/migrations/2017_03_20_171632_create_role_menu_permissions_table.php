<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoleMenuPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role_menu_permissions', function (Blueprint $table) {
            $table->bigInteger('role_id')->unsigned()->comment('角色ID');
            $table->integer('permission_id')->unsigned()->comment('权限id');

            $table->foreign('role_id')->references('id')->on('roles');
            $table->foreign('permission_id')->references('id')->on('menu_permissions');
            $table->primary(['role_id', 'permission_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('role_menu_permissions', function ($table) {
            $table->dropForeign(['permission_id']);
            $table->dropForeign(['role_id']);
        });
        Schema::dropIfExists('role_menu_permissions');
    }
}
