<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHelpTutorialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('help_tutorials', function ($table) {
            $table->increments('id')->unsigned();
            $table->string('title', 50)->nullable()->comment('主题名称');
            $table->string('content', 8000)->nullable()->comment('主题内容');
            $table->tinyInteger('sort_order')->unsigned()->nullable()->comment('同级排序号');
            
            $table->bigInteger('parent_id')->unsigned()->nullable()->comment('父主题ID');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('help_tutorials');
    }
}
