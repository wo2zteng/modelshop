<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPartnerGroupTypeToGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_groups', function ($table) {
            $table->integer('partner_group_type_id')->unsigned()->nullable()->comment('合作伙伴类型ID');
            $table->foreign('partner_group_type_id')->references('id')->on('partner_group_types');

            $table->string('icon', 200)->nullable()->comment('用户组图标');

            $table->integer('province_id')->unsigned()->nullable()->comment('地址所在省份id');
            $table->foreign('province_id')->references('id')->on('provinces');

            $table->integer('city_id')->unsigned()->nullable()->comment('地址所在城市id');
            $table->foreign('city_id')->references('id')->on('cities');

            $table->integer('district_id')->unsigned()->nullable()->comment('地址所在城区id');
            $table->foreign('district_id')->references('id')->on('districts');

            $table->string('detailed_address', 200)->nullable()->comment('详细地址');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_groups', function ($table) {
            $table->dropForeign(['partner_group_type_id']);
            $table->dropForeign(['district_id']);
            $table->dropForeign(['city_id']);
            $table->dropForeign(['province_id']);
        });
    }
}
