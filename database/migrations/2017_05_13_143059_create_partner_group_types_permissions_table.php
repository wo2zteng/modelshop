<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnerGroupTypesPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partner_group_type_permissions', function (Blueprint $table) {
            $table->integer('group_type_id')->unsigned()->comment('合作伙伴类型ID');
            $table->foreign('group_type_id')->references('id')->on('partner_group_types');
            $table->integer('permission_id')->unsigned()->comment('权限ID');
            $table->foreign('permission_id')->references('id')->on('menu_permissions');

            $table->primary(['group_type_id', 'permission_id'], 'group_type_permission_pk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('partner_group_type_permissions', function ($table) {
            $table->dropForeign(['group_type_id']);
            $table->dropForeign(['permission_id']);
        });
        Schema::dropIfExists('partner_group_type_permissions');
    }
}
