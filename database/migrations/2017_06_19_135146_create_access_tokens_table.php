<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccessTokensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('access_tokens', function ($table) {
            $table->integer('user_id')->unsigned()->comment('用户id');
            $table->smallInteger('app_id')->unsigned()->comment('APP的代码：定义：1=默认程序');
            $table->smallInteger('client_type')->unsigned()->comment('APP的类型：定义：1=IOS，2=android，3=WEB');
            $table->string('device_ip_addr', 30)->nullable()->comment('APP的终端IP');
            $table->string('access_token', 64)->nullable()->comment('验证token，只要用户不注销，则该条记录一直存在，一旦注销，则删除该记录');
            $table->dateTime('last_updated_time')->nullable()->comment('上一次刷新时间，超过2小时需要刷新token以及时间');
            $table->timestamps();
            $table->primary(['user_id', 'app_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('access_tokens');
    }
}
