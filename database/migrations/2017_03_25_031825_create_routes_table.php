<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('routes', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('name', 100)->nullable()->comment('路由名称');
            $table->string('code', 100)->nullable()->comment('路由权限代码');
            $table->string('route', 200)->nullable()->comment('路由路径');
            $table->string('action', 200)->nullable()->comment('路由Action');
            $table->string('slug', 200)->nullable()->comment('路由别名');
            $table->tinyInteger('route_type')->default(0)->nullable()->comment('路由类型：1=web路由，2=api路由');
            $table->tinyInteger('method')->default(2)->nullable()->comment('路由方法类型：1=any，2=get，3=post，4=put，5=delete, 6=patch, 7=option');
            $table->tinyInteger('sort_order')->unsigned()->nullable()->comment('排序号');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('routes');
    }
}
