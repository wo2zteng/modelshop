<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnerGroupTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partner_group_types', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('name', 100)->nullable()->comment('合作伙伴用户组类型');
            $table->tinyInteger('group_type')->default(1)->nullable()->comment('用户组基本类型：1=外部企业单位，2=内部组织机构，3=内部部门，4=个人组');
            $table->tinyInteger('sort_order')->unsigned()->nullable()->comment('排序号');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partner_group_types');
    }
}
