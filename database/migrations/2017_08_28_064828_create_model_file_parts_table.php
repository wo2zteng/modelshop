<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModelFilePartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('model_file_parts', function (Blueprint $table) {
            $table->bigInteger('model_id')->unsigned()->nullable()->comment('模型ID');
            $table->foreign('model_id')->references('id')->on('models');
            $table->string('trunk_id', 20)->nullable()->comment('文件切片ID');
            $table->integer('sort_order')->unsigned()->nullable()->comment('排序号');
            $table->string('file_id', 255)->nullable()->comment('文件ID');

            $table->primary(['model_id', 'trunk_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('model_file_parts');
    }
}
