<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModelCategorysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('model_categories', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('name', 100)->nullable()->comment('分类名称');
            $table->tinyInteger('sort_order')->unsigned()->nullable()->comment('同级排序号');
            $table->integer('parent_id')->unsigned()->nullable()->comment('父级权限');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('model_categories');
    }
}
