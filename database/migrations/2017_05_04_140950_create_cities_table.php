<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->increments('id')->comment('城市id');
            $table->string('name', 10)->nullable()->comment('城市名称');
            $table->string('full_name', 20)->nullable()->comment('城市全名');
            $table->string('code', 30)->nullable()->unique()->comment('城市代码');
            $table->smallInteger('sort_order')->nullable()->comment('排序号');
            $table->string('region_code', 10)->nullable()->comment('区号：0755');
            $table->string('zip_code', 10)->nullable()->comment('邮政编码');
            $table->integer('province_id')->unsigned()->comment('所属省份id');
            $table->foreign('province_id')->references('id')->on('provinces');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cities', function ($table) {
            $table->dropForeign(['province_id']);
        });
        Schema::drop('cities');
    }
}
