<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHelpTutorialImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('help_tutorial_images', function ($table) {
            $table->bigIncrements('id')->unsigned();
            $table->integer('tutorial_id')->unsigned()->comment('帮助主题的ID');
            $table->foreign('tutorial_id')->references('id')->on('help_tutorials');
            $table->string('file_id', 200)->nullable()->comment('素材图片文件的ID');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('help_tutorial_images');
    }
}
