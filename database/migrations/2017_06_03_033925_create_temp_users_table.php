<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_users', function (Blueprint $table) {
            $table->string('mobile', 30)->unique()->comment('临时用户手机');
            $table->string('name', 50)->nullable()->comment('临时用户名');
            $table->string('nick_name', 50)->nullable()->comment('临时用户昵称');
            $table->string('email', 100)->nullable()->comment('临时用户邮箱');
            $table->string('password', 200)->nullable()->comment('临时用户密码');
            $table->string('mobile_verify_code', 10)->nullable()->comment('临时用户手机验证码');
            $table->dateTime('mobile_verify_begin')->nullable()->comment('临时用户手机验证的开始时间，超过60秒过期');

            $table->primary(['mobile']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_users');
    }
}
