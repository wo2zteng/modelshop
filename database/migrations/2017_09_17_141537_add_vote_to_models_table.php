<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVoteToModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('models', function ($table) {
            $table->integer('visit_count')->nullable()->comment('浏览数');
            $table->integer('vote_count')->nullable()->comment('点赞数');
            $table->tinyInteger('is_new')->nullable()->comment('是否最新新品');
            $table->tinyInteger('is_recommand')->nullable()->comment('是否推荐');
            $table->tinyInteger('is_recommand_materials')->nullable()->comment('是否首页推荐材质');
            $table->integer('is_new_order')->nullable()->comment('最新新品排序');
            $table->integer('is_recommand_order')->nullable()->comment('是否推荐排序');
            $table->integer('is_recommand_materials_order')->nullable()->comment('是否首页推荐材质排序');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('models', function ($table) {
            $table->dropColumn(['visit_count']);
            $table->dropColumn(['vote_count']);
            $table->dropColumn(['is_new']);
            $table->dropColumn(['is_recommand']);
            $table->dropColumn(['is_recommand_materials']);
            $table->dropColumn(['is_new_order']);
            $table->dropColumn(['is_recommand_order']);
            $table->dropColumn(['is_recommand_materials_order']);
        });
    }
}
