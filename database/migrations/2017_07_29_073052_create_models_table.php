<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('models', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('name', 255)->nullable()->comment('模型名称');
            $table->string('description', 1000)->nullable()->comment('模型描述');
            $table->string('main_image', 255)->nullable()->comment('模型主图');
            $table->string('broadcast_images', 1000)->nullable()->comment('模型详情轮播图');
            $table->string('line_images', 1000)->nullable()->comment('模型线框图');
            $table->bigInteger('author_id')->unsigned()->nullable()->comment('作者ID');
            $table->foreign('author_id')->references('id')->on('users');
            $table->bigInteger('owner_id')->unsigned()->nullable()->comment('所有者ID');
            $table->foreign('owner_id')->references('id')->on('user_groups');
            $table->integer('class_id')->unsigned()->nullable()->comment('品类ID');
            $table->decimal('price', 10, 2)->nullable()->comment('价格');
            $table->string('file_guid', 200)->nullable()->comment('模型文件的GUID，用于关联文件');
            $table->string('file_name', 100)->nullable()->comment('模型文件的原名');
            $table->string('md5_name', 255)->nullable()->comment('上传的文件的MD5');
            $table->foreign('class_id')->references('id')->on('model_classes');
            $table->integer('sort_order')->unsigned()->nullable()->comment('排序号');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('models');
    }
}
