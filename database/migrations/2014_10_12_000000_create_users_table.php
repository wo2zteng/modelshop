<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('name', 50)->nullable()->comment('用户名');
            $table->string('nick_name', 50)->nullable()->comment('用户昵称');
            $table->string('email', 100)->nullable()->comment('用户邮箱');
            $table->string('password', 200)->nullable()->comment('用户密码');
            $table->string('mobile', 30)->unique()->comment('用户手机');
            $table->string('mobile_verify_code', 10)->nullable()->comment('用户手机验证码');
            $table->dateTime('mobile_verify_begin')->nullable()->comment('用户手机验证的开始时间，超过60秒过期');
            $table->tinyInteger('user_active')->default(0)->comment('用户是否有效：1=未生效，2=已生效，3=已禁用');
            $table->tinyInteger('is_super')->default(0)->comment('是否是超级用户：0=否，1=是');
            $table->tinyInteger('has_login_once')->default(0)->comment('是否曾经登录过一次：0=否，1=是');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
