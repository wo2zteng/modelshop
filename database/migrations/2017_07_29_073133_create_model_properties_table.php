<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModelPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('model_properties', function (Blueprint $table) {
            $table->bigInteger('model_id')->unsigned()->nullable()->comment('模型ID');
            $table->foreign('model_id')->references('id')->on('models');
            $table->integer('sequence')->unsigned()->nullable()->comment('排序号');
            $table->integer('sort_order')->unsigned()->nullable()->comment('显示排序号');
            $table->string('name', 100)->nullable()->comment('属性名称');
            $table->string('value', 100)->nullable()->comment('属性值');

            $table->primary(['model_id', 'sequence']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('model_properties');
    }
}
