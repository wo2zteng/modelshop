<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProvincesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('provinces', function (Blueprint $table) {
            $table->increments('id')->comment('省份id');
            $table->string('name', 10)->nullable()->comment('省份名称');
            $table->string('full_name', 20)->nullable()->comment('省份全名');
            $table->string('code', 30)->nullable()->unique()->comment('省份代码');
            $table->smallInteger('sort_order')->nullable()->comment('省份排序号');
            $table->boolean('is_direct_city')->nullable()->comment('是否是直辖市：0=否，1=是');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('provinces');
    }
}
