<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModelClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('model_classes', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('code', 100)->nullable()->comment('品类代码');
            $table->string('name', 100)->nullable()->comment('品类名称');
            $table->tinyInteger('sort_order')->unsigned()->nullable()->comment('同级排序号');
            $table->integer('category_id')->unsigned()->nullable()->comment('目录ID');
            $table->foreign('category_id')->references('id')->on('model_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('model_classes', function ($table) {
            $table->dropForeign(['category_id']);
        });
        Schema::dropIfExists('model_classes');
    }
}
