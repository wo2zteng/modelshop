layui.define('upload', function(exports){
    var obj = {
        doUpload: function(options) {
            var uploadOpts = {
                url: '/backstage/api/upload',
                success: function(result){
                    if (result.status == 0) {
                        options.success.call(this, result.fileId, result.filePath, result.fileKey);
                    } else {
                        layer.msg(result.info, { icon: 5 });
                    }
                }
            };
            if (options.elem) {
                uploadOpts.elem = options.elem;
            }
            if (options.before) {
                uploadOpts.before = options.before;
            }
            if (options.type) {
                uploadOpts.type = options.type;
            }
            if (options.ext) {
                uploadOpts.ext = options.ext;
            }
            layui.upload(uploadOpts);
        }
    };
    exports('uploadUtil', obj);
});
