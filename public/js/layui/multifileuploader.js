layui.define('jquery', function(exports){
    var $ = layui.jquery;

    var obj = {
        init: function(options) {
            requirejs.config({
                baseUrl: '/vendor/webuploader/',
                paths: {
                    jquery: './jquery'
                }
            });
            require(['webuploader'], function( WebUploader ) {
                $(function() {
                    var wrapbox = $('#' + options.uploader),
                        queuebox = $('<ul class="filelist"></ul>')
                            .appendTo( wrapbox.find('.queueList')),
                        statusBarbox = wrapbox.find('.statusBar'),
                        infobox = statusBarbox.find('.info'),
                        uploadbox = wrapbox.find('.uploadBtn'),
                        placeHoldercontainer = wrapbox.find( '.placeholder' ),
                        progressInfo = statusBarbox.find( '.progress' ).hide(),
                        fileCount = 0,
                        fileSize = 0,
                        ratio = window.devicePixelRatio || 1,
                        thumbnailWidth = 110 * ratio,
                        thumbnailHeight = 110 * ratio,
                        state = 'pedding',
                        percentages = {},
                        supportTransition = (function(){
                            var s = document.createElement('p').style,
                                r = 'transition' in s ||
                                      'WebkitTransition' in s ||
                                      'MozTransition' in s ||
                                      'msTransition' in s ||
                                      'OTransition' in s;
                            s = null;
                            return r;
                        })(),
                        uploader;

                    var guid = WebUploader.Base.guid();

                    uploader = WebUploader.create({
                        pick: {
                            id: options.filePicker? ('#'+options.filePicker) : '#filePicker',
                            label: '点击选择图片'
                        },
                        dnd: options.dnd ? ('#'+options.dnd) : '#dndArea',
                        paste: options.paste ? ('#'+options.paste) : '#uploader',
                        swf: '/vendor/webuploader/Uploader.swf',
                        chunked: true,
                        sendAsBinary: true,
                        server: '/backstage/api/batchupload',
                        fileNumLimit: 300,
                        formData: {guid: guid},
                        fileSizeLimit: 200 * 1024 * 1024,
                        fileSingleSizeLimit: 50 * 1024 * 1024
					});
					
                    uploader.addButton({
                        id: '#' + options.addButton,
                        label: '继续添加'
					});
					
                    function addFile( file ) {
                        var liObj = $( '<li id="' + file.id + '">' +
                                '<p class="title">' + file.name + '</p>' +
                                '<p class="imgWrap"></p>'+
                                '<p class="progress"><span></span></p>' +
								'</li>' ),
							
                            btns = $('<div class="file-panel">' +
                                '<span class="cancel">删除</span>' +
                                '<span class="rotateRight">向右旋转</span>' +
                                '<span class="rotateLeft">向左旋转</span></div>').appendTo(liObj),
                            prgressSpan = liObj.find('p.progress span'),
                            wrapbox = liObj.find( 'p.imgWrap' ),
                            infobox = $('<p class="error"></p>'),

                            showError = function( code ) {
                                switch( code ) {
                                    case 'exceed_size':
                                        text = '文件大小超出';
                                        break;

                                    case 'interrupt':
                                        text = '上传暂停';
                                        break;

                                    default:
                                        text = '上传失败，请重试';
                                        break;
                                }

                                infobox.text( text ).appendTo( liObj );
                            };

                        if (file.getStatus() === 'invalid') {
                            showError( file.statusText );
                        } else {
                            wrapbox.text('预览中');
                            uploader.makeThumb( file, function(error, src) {
                                if (error) {
                                    wrapbox.text('不能预览');
                                    return;
                                }
                                var img = $('<img src="'+src+'">');
                                wrapbox.empty().append( img );
                            }, thumbnailWidth, thumbnailHeight );
                            percentages[ file.id ] = [ file.size, 0 ];
                            file.rotation = 0;
						}
						
                        file.on('statuschange', function( cur, prev ) {
                            if (prev === 'progress') {
                                prgressSpan.hide().width(0);
                            } else if (prev === 'queued') {
                                liObj.off('mouseenter mouseleave');
                                btns.remove();
							}
							
                            if (cur === 'error' || cur === 'invalid') {
                                console.log( file.statusText );
                                showError( file.statusText );
                                percentages[ file.id ][ 1 ] = 1;
                            } else if (cur === 'interrupt') {
                                showError('interrupt');
                            } else if (cur === 'queued') {
                                percentages[file.id][1] = 0;
                            } else if (cur === 'progress') {
                                infobox.remove();
                                prgressSpan.css('display', 'block');
                            } else if (cur === 'complete') {
                                liObj.append('<span class="success"></span>');
                            }
                            liObj.removeClass('state-' + prev).addClass('state-' + cur);
                        });
                        liObj.on('mouseenter', function() {
                            btns.stop().animate({height: 30});
                        });
                        liObj.on( 'mouseleave', function() {
                            btns.stop().animate({height: 0});
                        });
                        btns.on('click', 'span', function() {
                            var index = $(this).index(), deg;
                            switch (index) {
                                case 0:
                                    uploader.removeFile( file );
                                    return;
                                case 1:
                                    file.rotation += 90;
                                    break;
                                case 2:
                                    file.rotation -= 90;
                                    break;
                            }
                            if (supportTransition) {
                                deg = 'rotate(' + file.rotation + 'deg)';
                                wrapbox.css({
                                    '-webkit-transform': deg,
                                    '-mos-transform': deg,
                                    '-o-transform': deg,
                                    'transform': deg
                                });
                            } else {
                                wrapbox.css('filter', 'progid:DXImageTransform.Microsoft.BasicImage(rotation='+ (~~((file.rotation/90)%4 + 4)%4) +')');
                            }
                        });
                        liObj.appendTo(queuebox);
                    }
                    function removeFile(file) {
                        var liObj = $('#'+file.id);
                        delete percentages[file.id];
                        updateTotalProgress();
                        liObj.off().find('.file-panel').off().end().remove();
                    }
                    function updateTotalProgress() {
                        var loaded = 0,
                            total = 0,
                            spans = progressInfo.children(),
                            percent;
                        $.each(percentages, function(k, v) {
                            total += v[0];
                            loaded += v[0] * v[1];
                        });
                        percent = total ? loaded / total : 0;
                        spans.eq(0).text(Math.round(percent * 100) + '%');
                        spans.eq(1).css('width', Math.round(percent * 100) + '%');
                        updateStatus();
                    }

                    function updateStatus() {
                        var text = '', stats;

                        if (state === 'ready') {
                            text = '选中' + fileCount + '张图片，共' + WebUploader.formatSize(fileSize) + '。';
                        } else if (state === 'confirm') {
                            stats = uploader.getStats();
                            if (stats.uploadFailNum) {
                                text = '已成功上传' + stats.successNum+ '张照片至XX相册，'+
                                    stats.uploadFailNum + '张照片上传失败，<a class="retry" href="#">重新上传</a>失败图片或<a class="ignore" href="#">忽略</a>'
                            }
                        } else {
                            stats = uploader.getStats();
                            text = '共' + fileCount + '张（' +
                                    WebUploader.formatSize( fileSize )  +
                                    '），已上传' + stats.successNum + '张';
                            if (stats.uploadFailNum) {
                                text += '，失败' + stats.uploadFailNum + '张';
                            }
                        }
                        infobox.html(text);
                    }
                    function setState(val) {
                        var file, stats;
                        if (val === state) {
                            return;
                        }
                        uploadbox.removeClass('state-' + state);
                        uploadbox.addClass('state-' + val);
                        state = val;

                        switch (state) {
                            case 'pedding':
                                placeHoldercontainer.removeClass('element-invisible');
                                queuebox.hide();
                                statusBarbox.addClass('element-invisible');
                                uploader.refresh();
                                break;
                            case 'ready':
                                placeHoldercontainer.addClass('element-invisible');
                                $('#' + options.addButton).removeClass('element-invisible');
                                queuebox.show();
                                statusBarbox.removeClass('element-invisible');
                                uploader.refresh();
                                break;
                            case 'uploading':
                                $('#' + options.addButton).addClass('element-invisible');
                                progressInfo.show();
                                uploadbox.text('暂停上传');
                                break;
                            case 'paused':
                                progressInfo.show();
                                uploadbox.text('继续上传');
                                break;
                            case 'confirm':
                                progressInfo.hide();
                                uploadbox.text('开始上传').addClass('disabled');
                                stats = uploader.getStats();
                                if (stats.successNum && !stats.uploadFailNum) {
                                    setState('finish');
                                    return;
                                }
                                break;
                            case 'finish':
                                stats = uploader.getStats();
                                if (stats.successNum) {
                                    $('#' + options.guidField).val(guid);
                                    alert('上传成功');
                                } else {
                                    state = 'done';
                                    location.reload();
                                }
                                break;
                        }
                        updateStatus();
                    }

                    uploader.onUploadProgress = function(file, percentage) {
                        var liObj = $('#'+file.id),
                            $percent = liObj.find('.progress span');

                        $percent.css('width', percentage * 100 + '%');
                        percentages[file.id][1] = percentage;
                        updateTotalProgress();
                    };
                    uploader.onFileQueued = function(file) {
                        fileCount++;
                        fileSize += file.size;
                        if (fileCount === 1) {
                            placeHoldercontainer.addClass('element-invisible');
                            statusBarbox.show();
                        }
                        addFile(file);
                        setState('ready');
                        updateTotalProgress();
                    };
                    uploader.on('uploadSuccess', function(file) {
                        var successfiles = $('#' +options.successfiles).val();
                        if (successfiles == '') {
                            successfiles = successfiles + file.name;
                        } else {
                            successfiles = successfiles + ',' + file.name;
                        }
                        $('#'+options.successfiles).val(successfiles);
                    });
                    uploader.onFileDequeued = function(file) {
                        fileCount--;
                        fileSize -= file.size;
                        if (!fileCount) {
                            setState('pedding');
                        }
                        removeFile(file);
                        updateTotalProgress();
                    };
                    uploader.on('all', function(type) {
                        var stats;
                        switch(type) {
                            case 'uploadFinished':
                                setState('confirm');
                                break;
                            case 'startUpload':
                                setState('uploading');
                                break;
                            case 'stopUpload':
                                setState('paused');
                                break;
                        }
                    });
                    uploader.onError = function(code) {
                        alert('Eroor: ' + code);
                    };
                    uploadbox.on('click', function() {
                        if ($(this).hasClass('disabled')) {
                            return false;
                        }
                        if (state === 'ready') {
                            uploader.upload();
                        } else if (state === 'paused') {
                            uploader.upload();
                        } else if (state === 'uploading') {
                            uploader.stop();
                        }
                    });
                    infobox.on('click', '.retry', function() {
                        uploader.retry();
                    });

                    infobox.on('click', '.ignore', function() {
                        alert('todo');
                    });
                    uploadbox.addClass('state-' + state);
                    updateTotalProgress();
                });
            });

        }
    };
    exports('multifileuploader', obj);
});
