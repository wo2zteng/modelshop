layui.define('layer', function(exports){
    var obj = {
        processValidateError: function(error) {
            if (error.status == 422) {
                var errors = error.responseJSON;
                var message = '';
                for (var errorCode in errors)  {
                    var errorItem = errors[errorCode];
                    for (var i=0; i<errorItem.length; i++) {
                        message += errorItem[i] + '<br>';
                    }
                }
                layer.msg(message, { icon: 2 });
            } else {
                layer.msg("请求异常", { icon: 2 });
            }
        }
    };
    exports('validator', obj);
});
