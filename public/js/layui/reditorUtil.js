layui.define(['jquery', 'layedit'], function(exports){
    var $ = layui.jquery;
    var layedit = layui.layedit;

    var obj = {
        doInitEditor: function(options) {
            layedit.set({
                uploadImage: {
                    url: '/backstage/api/richeditor/upload',
                    type: 'post'
                }
            });
            return layedit.build(options.elemId);
        },
        getContent: function(editorIndex) {
            return layedit.getContent(editorIndex);
        }
    };
    exports('reditorUtil', obj);
});
