layui.define(['laydate','jquery'], function (exports) {
    var $ = layui.jquery;
    var layer = layui.layer;
    var laydate = layui.laydate;

    var obj = {
        makeDateRange(options) {
            var startElem = options.startElem, endElem = options.endElem;
            var startCreate = {
                min: laydate.now(),
                max: '2099-06-16 23:59:59',
                istoday: false,
                choose: function(datas){
                    endCreate.min = datas;
                    endCreate.start = datas;
                }
            };

            if (options.isDateTime) {
                startCreate.istime = true;
                if (options.format) {
                    startCreate.format = options.format;
                } else {
                    startCreate.format = 'YYYY-MM-DD hh:mm:ss';
                }
            }

            var endCreate = {
                min: laydate.now(),
                max: '2099-06-16 23:59:59',
                istoday: false,
                choose: function(datas){
                    startCreate.max = datas;
                }
            };

            if (options.isDateTime) {
                endCreate.istime = true;
                if (options.format) {
                    endCreate.format = options.format;
                } else {
                    endCreate.format = 'YYYY-MM-DD hh:mm:ss';
                }
            }

            startElem.on('click', function(){
                startCreate.elem = this;
                laydate(startCreate);
            });

            endElem.on('click', function(){
                endCreate.elem = this;
                laydate(endCreate);
            });
        }
    };
    exports('dateRangeUtil', obj);
});
