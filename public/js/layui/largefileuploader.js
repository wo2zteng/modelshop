layui.define(['jquery', 'element'], function(exports){
    var $ = layui.jquery;
    var layer = layui.layer;
    var element = layui.element();

    var obj = {
        init: function(options) {
            requirejs.config({
                baseUrl: '/vendor/webuploader/',
                paths: {
                    jquery: './jquery'
                }
            });

            require([ 'webuploader' ], function( WebUploader ) {
                $(function() {
                    var guid, uploader;
                    var uploaderId = options.uploader? ('#'+options.uploader) : '#uploader';
                    var filePicker = options.filePicker;
                    var pickerId = '#' + filePicker.id;
                    var uploaderBtnId = options.uploadBtn? ('#'+options.uploadBtn) : '#uploadBtn';
                    var progressFilter = options.pgfilter;
                    var uploadResultId = options.uploadId;

                    initFileUploader(pickerId);
                    
                    uploader.on( 'fileQueued', function( file ) {
                        $(uploaderId).find('div.t-file-info').text(file.name);
                        $(uploaderId).find('div.t-upload-progress').css('display', '');
                        element.progress(progressFilter, '0%');
                        element.init();
                    });
                    
                    $(uploaderBtnId).on('click', function(){
                        $('#' + uploadResultId).val('');
                        uploader.upload();
                    });
                    
                    uploader.on( 'uploadSuccess', function( file ) {
                        //$( '#'+file.id ).find('p.state').text('已上传');
                    });
                    
                    uploader.on( 'uploadError', function( file ) {
                        layer.msg('上传错误', { icon: 5 });
                    });
                    
                    uploader.on( 'uploadComplete', function( file ) {
                        layer.msg('上传成功', { icon: 6 });
                        $('#' + uploadResultId).val(guid);
                        //uploader.destroy();
                        initFileUploader(pickerId);
                    });
                    
                    uploader.on( 'uploadProgress', function( file, percentage ) {
                        var percentage = parseInt(percentage * 100);
                        element.progress(progressFilter, percentage + '%');
                        element.init();
                    });

                    function initFileUploader(pickerId) {
                        guid = WebUploader.Base.guid();

                        uploader = WebUploader.create({
                            swf: '/vendor/webuploader/Uploader.swf',
    
                            server: '/backstage/api/thunkupload',
    
                            pick: {
                                id: pickerId,
                                label: filePicker.text,
                                multiple : false
                            },
    
                            resize: false,
                            chunked: true,
                            chunkSize: 1 * 1024 * 1024,
                            sendAsBinary: true,
                            fileNumLimit: 1,
                            threads: 1,
                            chunkRetry: 10,
                            formData: {guid: guid},
                            fileSizeLimit: 1024 * 1024 * 1024
                        
                        });

                        $(uploaderId).find('div.t-upload-progress').css('display', 'none');

                        $(pickerId).css('float', 'left');
                        $(pickerId).css('margin-right', '15px');
                        $(pickerId).find('div.webuploader-pick').attr('class', 'layui-btn layui-btn-small layui-btn-primary');

                    }
                });
            });

        }
    };
    exports('largefileuploader', obj);
});
