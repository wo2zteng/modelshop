layui.define(['form', 'element', 'layer', 'util', 'jquery', 'popLayerUtil'], function(exports){
    var $ = layui.jquery;
    var layer = layui.layer;
    var element = layui.element();

    var obj = {
        activateNaviTab: function (url, id, title, moduleId) {
            if (url == undefined) return;

            var tabTitleDiv = $('.layui-tab[lay-filter=\'navitab\']').children('.layui-tab-title');
            var exist = tabTitleDiv.find('li[lay-id=' + id + ']');
            if (exist.length > 0) {
                element.tabChange('navitab', id);
            } else {
                var index = layer.load(1);
                $.ajax({
                    type: 'get',
                    url: url,
                    success: function (data) {
                        var naviTabUtil = layui.naviTabUtil;
                        layer.close(index);
                        element.init();
                        element.tabAdd('navitab', { title: title, content: data, id: id });
                        element.tabChange('navitab', id);
                        naviTabUtil.disableFirstTabClose();
                        $('.layui-default-tree-left').each(function(){
                            var height = $(this).parent().parent().innerHeight() - 20;
                            $(this).css('height', height + 'px');
                        });
                    },
                    error: function (e) {
                        var message = e.responseText;
                        layer.close(index);
                        layer.msg(message, { icon: 2 });
                    }
                });
            }
        },
        disableFirstTabClose: function() {
            $('.layui-tab[lay-filter=\'navitab\']').children('.layui-tab-title')
                .find('li').each(function(i){
                if ( i == 0 ) {
                    $(this).find('.layui-tab-close').css('display', 'none');
                }
            });
        }
    };
    exports('naviTabUtil', obj);
});
