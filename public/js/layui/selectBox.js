layui.define(['form','jquery'], function(exports){
    var $ = layui.jquery, form = layui.form();

    "use strict";

    var SelectBox = function (options) {
        this.defaultConfig={
            boxWidth: 100,
            spaceWidth: 50,
            bottomSpace: 10,
            enableBorder: true,
            boxBorderWidth: 1,
            boxBorderColor: '#ccc',
            backColor: '#f2f2f2',
            textColor: '#bbb',
            addButton: true
        };

        this.options = $.extend(this.defaultConfig, options);
    };

    SelectBox.prototype.init = function() {
        var t = this, opts = t.options;

        t.$element = opts.elem;
        t.$element.addClass('layui-select-box');

        this.createAddButton();
    }

    SelectBox.prototype.createAddButton = function() {
        var t = this, opt = t.options;

        if (opt.addButton) {
            var item = $('<li>').css('width', '100px');

            var addbtn = $('<a></a>').addClass('layui-btn').addClass('layui-btn-small');
            addbtn.append('<i class="layui-icon">&#xe654;</i>');
            addbtn.on('click', function() {
                opt.onBtnAddClick.call(t, t);
            });
            item.append(addbtn);

            t.$element.append(item);

            t.addButton = item;
        }
    }

    SelectBox.prototype.itemCount = function() {
        return this.$element.find('.box-label').length;
    }

    SelectBox.prototype.getAllItemIds = function() {
        var ids = [];
        this.$element.find('.box-label').each(function(){
            ids.push($(this).find('input').val());
        });
        return ids;
    }

    SelectBox.prototype.addItem = function(data) {
        var t = this, opt = t.options;
        var item = $('<li>').css('margin-right', opt.spaceWidth + 'px')
            .css('margin-bottom', opt.bottomSpace + 'px');

        var box = $('<div>').css('width', opt.boxWidth + 'px')
            .css('background-color', opt.backColor)
            .css('color', opt.textColor)
            .addClass('box-label');

        if (opt.enableBorder) {
            box.css('border-style', 'solid').css('border-width', opt.boxBorderWidth + 'px').css('border-color', opt.boxBorderColor);
        }

        item.append(box);

        box.append($("<div>").text(data.title));
        box.append($("<input>").prop('type', 'hidden').val(data.id));

        var rmButton = $("<div>").addClass('box-icon');
        rmButton.append('<i class="layui-icon">&#x1007;</i>');
        rmButton.on('click', function() {
            if (opt.onBeforeRemoveItem) {
                if (opt.onBeforeRemoveItem.call(t, t)) {
                    item.remove();
                }
            } else {
                item.remove();
            }
        });
        box.append(rmButton);

        item.insertBefore(t.addButton);

        return this;
    };

    exports('selectBox', function(options){
        var selectBox = new SelectBox(options);
        selectBox.init();
        return selectBox;
    });
});
